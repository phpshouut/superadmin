<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
error_reporting(E_ALL);
class Voucher extends CI_Controller {

    public function __construct() {
        parent::__construct();
       header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		  header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
    header('Pragma: no-cache'); // HTTP 1.0.
    header('Expires: 0');
		include APPPATH . 'libraries/config_paytm.php';
        include APPPATH . 'libraries/encdec_paytm.php';
 
     //   $this->load->library('mongo_db');
         $this->load->library('form_validation');
		  $this->load->model('voucher_model');
		  $this->load->helper('cookie');
       
     
        $this->load->library('My_PHPMailer');
		//echo "ssss"; die;

      /*  $this->session->unset_userdata('logged_in');
          $this->session->unset_userdata('vc_data');
          $this->session->sess_destroy(); */
    }

    public function index() {
		
        // $id=$this->input->get('id');
            // $session_data = $this->session->userdata('logged_in');
			// echo "<pre>"; print_R($session_data);die;
        //   $data['charity_data']=$this->charity_model->get_charity_data($id);
        // $data['chrtbhikshu_data']=$this->charity_model->get_charitybhikshu_data($id);
        //$data['chrt_recentactvty'] = $this->charity_model->get_charityrecent_activity($id,0,5);
         $this->session->unset_userdata('is_payment');
        $id = get_cookie('loc_id');
        if ($id != '') {
            $username = get_cookie('username');
            $location_name = get_cookie('location_name');
            $mobile = get_cookie('mobile');
	    $locuid=get_cookie('loc_uid');
            // $nasipaddress=get_cookie('nasipaddress');
            $sess_array = array(
                'username' => $username,
                'id' => $id,
                'location_name' => $location_name,
                'mobile' => $mobile,
		'location_uid'=>$locuid
            );

            $this->session->set_userdata('logged_in', $sess_array);
            
        }
		
		//echo "ssss"; die;
		
        $this->load->view('voucher');
    }

    public function login() {
        $postdata = $this->input->post();
        $userdata = $this->voucher_model->check_login();


        if ($userdata['message_type'] == 1 || $userdata['message_type'] == 4) {
            $sess_array = array(
                'username' => $userdata['username'],
                'id' => $userdata['id'],
                'location_name' => $userdata['location_name'],
                'mobile' => $userdata['mobile'],
		'location_uid'=>$userdata['location_uid']
            );
	    

            $this->session->set_userdata('logged_in', $sess_array);
	      $session_data = $this->session->userdata('logged_in');
	   // echo "<pre>"; print_R($session_data);//die;
	   //  echo "<pre>"; print_R($postdata);die;
            if ($postdata['loginchecked'] == 1) {
                set_cookie('username', $userdata['username'], time() + 60 * 60 * 24 * 30);
                set_cookie('loc_id', $userdata['id'], time() + 60 * 60 * 24 * 30);
                set_cookie('location_name', $userdata['location_name'], time() + 60 * 60 * 24 * 30);
                set_cookie('mobile', $userdata['mobile'], time() + 60 * 60 * 24 * 30);
                set_cookie('nasipaddress', $userdata['nasipaddress'], time() + 60 * 60 * 24 * 30);
		 set_cookie('loc_uid', $userdata['location_uid'], time() + 60 * 60 * 24 * 30);
            }

            echo $userdata['message_type'];
        } else {
            echo $userdata['message_type'];
        }
    }

    public function voucher_list() {
        $data = $this->voucher_model->get_voucher_list();
        $html = "";
        $html1 = "";
	if(!empty($data)){
        foreach ($data as $vouhers) {
            $html.=' <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                       <h5>' . $vouhers['plan_name'] . ' : <small>' . $vouhers['voucher_Available'] . ' Vouchers</small></h5>
                                       <h6>Sold For ₹ ' . $vouhers['sale_price'] . '</h6>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2  nopadding-left nopadding-right">
                                       <center>';
            if ($vouhers['voucher_Available'] != 0) {
                $html.= '<a href="javascript:void(0);" class="assgn_voucher" data-mb="' . $vouhers['data_amnt'] . '" data-costid="' . $vouhers['id'] . '" rel="' . $vouhers['plan_name'] . '" data-cost="' . $vouhers['sale_price'] . '"> <img src="' . base_url() . 'assets/imgs/ticket_icons.svg" width="30" height="30" class="img-responsive svgmargin"/></a>';
            } else {
                $html.= '<a href="javascript:void(0);" > <img src="' . base_url() . 'assets/imgs/shouut_reseller_sell_disabled.svg" width="30" height="30" class="img-responsive svgmargin"/></a>';
            }
            $html.= ' </center>
                                    </div>
                                 </div>
                              </div>';


            $html1.='<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<h5>' . $vouhers['plan_name'] . ' : <span class="page-span">₹ ' . $vouhers['purchase_price'] . '</span> <small>/ voucher</small></h5>
                                       <h6>Sold For ₹ ' . $vouhers['sale_price'] . '</h6>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="row" style="margin-top:2px;">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px 10px;">
											<div class="input-group">
												<span class="input-group-voucher input-group-addon sub_vc" rel=' . $vouhers['purchase_price'] . '>
												<i class="fa fa-minus"  aria-hidden="true"></i>
												</span>
												<span class="form-control-voucher vc_ttl_amt" rel="0" id="' . $vouhers['id'] . '">0</span>
												<span class="input-group-voucher input-group-addon add_vc" rel=' . $vouhers['purchase_price'] . '>
												<i class="fa fa-plus"  aria-hidden="true"></i>
												</span>
											</div>
										  </div>
										</div>
                                    </div>
                                 </div>
                              </div>';
        }
	}
	else
	{
	    $html.='<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                       <h5>Voucher Data Not Configured in Location</h5>
                                       
                                    </div></div></div>';
	}
        echo $html . ":::" . $html1;
    }

    public function update_pwd() {
        $postdata = $this->input->post();
        $session_data = $this->session->userdata('logged_in');
        if ((isset($session_data['id']) && $session_data['id'] != '') || $postdata['locid'] != '') {
            $locid = (isset($postdata['locid']) && $postdata['locid'] != '') ? $postdata['locid'] : $session_data['id'];
            $inserted = $this->voucher_model->update_user_pwd($locid);
            if ($inserted) {
                echo "1";
            } else {
                echo "2";
            }
        } else {
            echo "2";
        }
    }

    public function send_voucher_sms() {
        $session_data = $this->session->userdata('logged_in');
        $sms_send = $this->voucher_model->send_voucher_sms($session_data['id']);
        if ($sms_send) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function activate_voucher() {
        $session_data = $this->session->userdata('logged_in');
        echo $this->voucher_model->activate_voucher($session_data['id']);
    }

    public function voucher_sold_list() {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->voucher_model->voucher_sold_list($session_data['id']);
        $html = "";
        if (count($data) > 0) {
            foreach ($data as $val) {
                $soldon = date('M d, H:i', strtotime($val->assigned_on));
                $class = ($val->is_used == 1) ? "used" : "unused";
                $msg = ($val->is_used == 1) ? "Used" : "Unused";
                $html.=' <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"> 
                                       <h5>' . $val->plan_name . ', <small style="color:#36465f; font-size: 14px">+91 ' . $val->customer_mobile . ' </small></h5>
                                       <h6 style="font-weight:200">Sold: ' . $soldon . ' </h6>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3  nopadding-left" style="padding-top:10px; text-align: right">
										   <small class="' . $class . '">' . $msg . '</small>
                                    </div>
                                 </div>
                              </div>';
            }
        } else {
            $html = '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"> 
                                       <h5>No Voucher Sold</h5>
                                       
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3  nopadding-left" style="padding-top:10px; text-align: right">
										 
                                    </div>
                                 </div>
                              </div>';
        }
        echo $html;
        //echo "<pre>"; print_R($data);
    }

    public function purchase_voucher_list() {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->voucher_model->purchase_voucher_list($session_data['id']);
        $html = "";
        if (count($data) > 0) {
            foreach ($data as $val) {
                $purchasedon = date('M d, Y', strtotime($val['date']));
                $totalamt = (strpos($val['totalamt'], '.')) ? $val['totalamt'] . "0" : $val['totalamt'] . ".00";

                $html.=' <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"> 
                                       
                                         <h5>₹ ' . $totalamt . ', <span class="page-span-history">' . $purchasedon . '</span></h5>
                                       <h6>' . $val['planlist'] . '</h6>   
                                    </div>
                                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2  nopadding-left nopadding-right">
                                        <center>
                                          <img src="' . base_url() . 'assets/imgs/shouut_reseller_receipt.svg" width="40" height="40" class="img-responsive svgmargin"/>
                                       </center>
                                    </div>
                                 </div>
                              </div>';
            }
        } else {
            $html = '<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 col-centered" style="border-bottom: 1px solid #f4f4f5;	background-color: #fff;">
                                 <div class="row rowpadding">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10"> 
                                       <h5>No Purchase History</h5>
                                       
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3  nopadding-left" style="padding-top:10px; text-align: right">
										 
                                    </div>
                                 </div>
                              </div>';
        }
        echo $html;
        //echo "<pre>"; print_R($data);
    }

    public function voucher_payment() {
		$data=array();
        $postdata = $this->input->post();
        $sess_array = array(
            'vc_data' => $postdata['VC_DATA'],
            'total_amt' => $postdata['TXN_AMOUNT']
        );
		
		//echo "<pre>"; print_R($datapg); die;
		//echo "<pre>"; print_R($postdata);
		//echo "<pre>"; print_R($datapg);
		
        $this->session->set_userdata('vc_data', $sess_array);
        $sess_arr=array('msg'=>'','payment_type'=>"voucher");
         $this->session->set_userdata('target_msg', $sess_arr);
		
        $session_data = $this->session->userdata('logged_in');
	$datapg=$this->voucher_model->payment_gateway_type();
		if(isset($datapg['citrus']) && $datapg['citrus']['citrus_secret_key']!='')
		{
			
		$amount = $postdata['TXN_AMOUNT'];
		$formPostUrl = $datapg['citrus']['citrus_post_url'];
		$secret_key = $datapg['citrus']['citrus_secret_key'];
		$vanityUrl = $datapg['citrus']['citrus_vanity_url'];
		$merchantTxnId = uniqid();
		$orderAmount = $amount;
		$currency = "INR";
		$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
		$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
		$notifyUrl = base_url() . 'voucher/paymentnotify';
		$returnUrl = base_url() . 'voucher/paymentresponse';
		$data['citrusdetail'] = array("merchantTxnId" => $merchantTxnId,"orderAmount" => $orderAmount,
			"currency" => $currency,"returnUrl" => $returnUrl,   "notifyUrl" => $notifyUrl,
			"secSignature" => $securitySignature,'posturl'=>$formPostUrl);
			 $data['pgtype'] = "citrus";
		}
		 if(isset($datapg['payu']) && $datapg['payu']['payu_merchantid']!='')
		{
			//echo "xxxsssss"; die;
			$amount =$postdata['TXN_AMOUNT'];
		$productinfo = 'bills';
		$fname = '';
		$email = '';
		$phone = '';
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		// get payumoney credential
		
	
		$MERCHANT_KEY = $datapg['payu']['payu_merchantkey'];
		$SALT = $datapg['payu']['payu_merchantsalt'];
		$PAYU_BASE_URL = $datapg['payu']['payu_authheader'];
		$udf1='';
		$udf2='';
		$udf3='';
		$udf4='';
		$udf5='';
		$hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|'. $fname . '|' . $email .'|'.$udf1.'|' .$udf2.'|' .$udf3.'|'.$udf4.'|'.$udf5.'||||||'. $SALT;

        $hash = strtolower(hash('sha512', $hashstring));
        $data['hash'] = $hash;
	$success_url = base_url() . 'voucher/payumoney_success';
	$fail_url = base_url() . 'voucher/payumoney_fail';
	$cancle_url = base_url() . 'voucher/payumoney_cancle';
	$data['payumoneydetail'] = array('posturl'=>$PAYU_BASE_URL, 'mkey' => $MERCHANT_KEY, 'tid' => $txnid, 'amount' => $amount,
					 'name' => $fname, 'email'=>$email, 'phone' => $phone, 'productinfo' => $productinfo,
					 'success_url' => $success_url, 'fail_url' => $fail_url, 'cancle_url'=> $cancle_url);
					  $data['pgtype'] = "payu";
		}
		 if(isset($datapg['paytm']) && $datapg['paytm']['paytm_merchant_key']!='')
		{
		//	echo "rrrrrrr"; die;
			$data['userdata']['total_amt'] = $postdata['TXN_AMOUNT'];
        $data['userdata']['user_id'] = $session_data['id'];
        $data['userdata']['ORDER_ID'] = $postdata['ORDER_ID'];
        $data['userdata']['CUST_ID'] = $postdata['CUST_ID'];
        $data['userdata']['INDUSTRY_TYPE_ID'] = 'Retail';
        $data['userdata']['CHANNEL_ID'] = $postdata['CHANNEL_ID'];
        $data['userdata']['TXN_AMOUNT'] = $postdata['TXN_AMOUNT'];
        $data['userdata']['MOBILE_NO'] = $postdata['mobile_no'];
		 $data['pgtype'] = "paytm";
		 $data['paytm']=$datapg['paytm'];
		}
        


 //$this->load->view('pg');


 
        $this->load->view('pg', $data);
		
		//echo "ssssss";
    }
	
	
	public function payumoney_success(){
	    //echo "<pre>"; print_R($_POST);die;
	      $session_data1 = $this->session->userdata('is_payment');
         if(isset($session_data1['form_submit']))
         {
           redirect(base_url() . "voucher");  
         }
         else
         {
             $sessarr=array("form_submit"=>1);
              $this->session->set_userdata('is_payment', $sessarr);
         }
       
        
                
       
        $session_data = $this->session->userdata('logged_in');
        if (!isset($session_data['id']) && $session_data['id'] != "" ) {
            redirect(base_url() . "voucher");
        }
        if(empty($_POST))
        {
              redirect(base_url() . "voucher");
        }
        $sessionvc_data = $this->session->userdata('vc_data');
        $sessionmsg_type = $this->session->userdata('target_msg');
        if($sessionmsg_type['payment_type']=="voucher")
        {
            //   echo "<pre>";print_R($session_data);die;
            if (isset($_POST['status']) && $_POST['status'] == "success") {
		$transid=$this->voucher_model->payment_response(json_encode($_POST),'payu');
                $this->voucher_model->add_voucher_location($session_data['id'], $sessionvc_data['vc_data'],$transid);
                $data['voucher']['voucher_list'] = $this->voucher_model->get_voucher_list();
                $data['voucher']['is_paid'] = 1;
				//echo "<pre>"; print_R($data);
				
                 $this->load->view('voucher',$data);
				 
                 

               
            } else if (empty($_POST)) {
                $this->load->view('paymentfail');
            } else {
				$this->voucher_model->payment_response(json_encode($_POST),'payu');
              $this->load->view('paymentfail');
            }
        }
        else
        {
            $data=array();
         //   echo "<pre>"; print_R($sessionmsg_type); die;
             $session_data = $this->session->userdata('target_data');
              if (isset($_POST['status']) && $_POST['status'] == "success") {
                  $ud=$this->voucher_model->get_user_data($session_data['dataid']);
             $data['msg_type']=$sessionmsg_type['msg_type'];
             $data['message']=$sessionmsg_type['msg'];
             $data['user_data']=$ud;
               
               $this->voucher_model->send_users_msg($data);
               $dataarr['msg']['is_sent'] = 1;
               $dataarr['msg']['total_user'] = count($ud);
               $dataarr['msg']['message'] = $sessionmsg_type['msg'];
                  $dataarr['msg']['msg_type']=$sessionmsg_type['msg_type'];
               //$this->session->unset_userdata('target_data');
                 //$this->session->unset_userdata('target_msg');
				 	$this->voucher_model->payment_response(json_encode($_POST),'payu');
              $this->load->view('voucher', $dataarr);
               
            } else if (empty($_POST)) {
                 $this->load->view('paymentfail');
            } else {
					$this->voucher_model->payment_response(json_encode($_POST),'payu');
               $this->load->view('paymentfail');
            }
             
          
            
        }
	    
		//$this->bils_model->payumoney_success();
	}
	public function payumoney_fail(){
		$this->voucher_model->payment_response(json_encode($_POST),'payu');
              $this->load->view('paymentfail');
	}
	public function payumoney_cancle(){
		
		$this->voucher_model->payment_response(json_encode($_POST),'payu');
              $this->load->view('paymentfail');
	}
	public function payumoney_cancel_view($id){
		$data["transection_detail"] = $this->bils_model->payumoney_transection_detail($id);
		$this->load->view('account/payment/payumoney/payumoney_cancle',$data);
	}
	public function payumoney_success_view($id){
		$data["transection_detail"] = $this->bils_model->payumoney_transection_detail($id);
		$this->load->view('account/payment/payumoney/payumoney_success',$data);
	}
	
	
	public function paymentnotify() {

	}
	public function paymentresponse() {
		$postdata=$this->input->post();
		//echo "<pre>"; print_R($postdata);//die;
		$datarr=array();
	$datapg=$this->voucher_model->payment_gateway_type();
		if (isset($_POST['TxId'])) {
			// echo "sssssssssss"; die;
			set_include_path('../lib' . PATH_SEPARATOR . get_include_path());
			$secret_key = $datapg['citrus']['citrus_secret_key'];
			$data = "";
			$flag = "true";


			$paymentPostResponse = $_POST;
			if (isset($_POST['TxId'])) {
				$txnid = $_POST['TxId'];
				$data .= $txnid;
			}
			if (isset($_POST['TxStatus'])) {
				$txnstatus = $_POST['TxStatus'];
				$data .= $txnstatus;
			}
			if (isset($_POST['amount'])) {
				$amount = $_POST['amount'];
				$data .= $amount;
			}
			if (isset($_POST['pgTxnNo'])) {
				$pgtxnno = $_POST['pgTxnNo'];
				$data .= $pgtxnno;
			}
			if (isset($_POST['issuerRefNo'])) {
				$issuerrefno = $_POST['issuerRefNo'];
				$data .= $issuerrefno;
			}
			if (isset($_POST['authIdCode'])) {
				$authidcode = $_POST['authIdCode'];
				$data .= $authidcode;
			}
			if (isset($_POST['firstName'])) {
				$firstName = $_POST['firstName'];
				$data .= $firstName;
			}
			if (isset($_POST['lastName'])) {
				$lastName = $_POST['lastName'];
				$data .= $lastName;
			}
			if (isset($_POST['pgRespCode'])) {
				$pgrespcode = $_POST['pgRespCode'];
				$data .= $pgrespcode;
			}
			if (isset($_POST['addressZip'])) {
				$pincode = $_POST['addressZip'];
				$data .= $pincode;
			}
			if (isset($_POST['signature'])) {
				$signature = $_POST['signature'];
			}

			$respSignature = hash_hmac('sha1', $data, $secret_key);
			if ($signature != "" && strcmp($signature, $respSignature) != 0) {
				$flag = "false";
			}

			if ($flag == "true") {

				if ($txnstatus == "SUCCESS") {
					 $session_data1 = $this->session->userdata('is_payment');
         if(isset($session_data1['form_submit']))
         {
           redirect(base_url() . "voucher");  
         }
         else
         {
             $sessarr=array("form_submit"=>1);
              $this->session->set_userdata('is_payment', $sessarr);
         }
       
        
                
       
        $session_data = $this->session->userdata('logged_in');
        if (!isset($session_data['id']) && $session_data['id'] != "" ) {
            redirect(base_url() . "voucher");
        }
        if(empty($_POST))
        {
              redirect(base_url() . "voucher");
        }
        $sessionvc_data = $this->session->userdata('vc_data');
        $sessionmsg_type = $this->session->userdata('target_msg');
		// echo "<pre>";print_R($sessionmsg_type);//die;
        if($sessionmsg_type['payment_type']=="voucher")
        {
	    $data=array();	
             //  echo "<pre>";print_R($sessionvc_data);//die;
          // die;
	  $transid=$this->voucher_model->payment_response(json_encode($postdata),'citrus');
                $this->voucher_model->add_voucher_location($session_data['id'], $sessionvc_data['vc_data'],$transid);
                $data['voucher']['voucher_list'] = $this->voucher_model->get_voucher_list();
                $data['voucher']['is_paid'] = 1;
				//echo "<pre>"; print_R($data); 
				
                 $this->load->view('voucher',$data);
				 
                 

               
            
        }
        else
        {
			
            $data=array();
         //   echo "<pre>"; print_R($sessionmsg_type); die;
             $session_data = $this->session->userdata('target_data');
            
                  $ud=$this->voucher_model->get_user_data($session_data['dataid']);
             $data['msg_type']=$sessionmsg_type['msg_type'];
             $data['message']=$sessionmsg_type['msg'];
             $data['user_data']=$ud;
               
               $this->voucher_model->send_users_msg($data);
               $dataarr['msg']['is_sent'] = 1;
               $dataarr['msg']['total_user'] = count($ud);
               $dataarr['msg']['message'] = $sessionmsg_type['msg'];
                  $dataarr['msg']['msg_type']=$sessionmsg_type['msg_type'];
               //$this->session->unset_userdata('target_data');
                 //$this->session->unset_userdata('target_msg');
				 	$this->voucher_model->payment_response(json_encode($_POST),'citrus');
              $this->load->view('voucher', $dataarr);
               
          
             
          
            
        }

				} else {
					$this->voucher_model->payment_response(json_encode($_POST),'paytm');

				}
			} else {

				$this->voucher_model->payment_response(json_encode($_POST),'paytm');
				// $this->render('payment-failed', array('eventdetail' => $eventDetail, "eventStockList" => $eventStockList, 'paymentPostResponse' => $paymentPostResponse, 'amount' => $amount, 'event_id' => $event_id, 'message' => 'Citrus Response Signature and Our (Merchant) Signature Mis-Mactch'));
			}
		} else {

			redirect(base_url()."voucher");
		}
	}
    
    public function smsemail_payment()
    {
        $postdata=$this->input->post();
       
        $sess_arr=array('msg'=>$postdata['message'],'payment_type'=>"msg","msg_type"=>$postdata['message_type']);
         $this->session->set_userdata('target_msg', $sess_arr);
       
         $session_data = $this->session->userdata('logged_in');
     $datapg=$this->voucher_model->payment_gateway_type();
     
		if(isset($datapg['citrus']) && $datapg['citrus']['citrus_secret_key']!='')
		{
			
		$amount = $postdata['TXN_AMOUNT'];
		$formPostUrl = $datapg['citrus']['citrus_post_url'];
		$secret_key = $datapg['citrus']['citrus_secret_key'];
		$vanityUrl = $datapg['citrus']['citrus_vanity_url'];
		$merchantTxnId = uniqid();
		$orderAmount = $amount;
		$currency = "INR";
		$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
		$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
		$notifyUrl = base_url() . 'voucher/paymentnotify';
		$returnUrl = base_url() . 'voucher/paymentresponse';
		$data['citrusdetail'] = array("merchantTxnId" => $merchantTxnId,"orderAmount" => $orderAmount,
			"currency" => $currency,"returnUrl" => $returnUrl,   "notifyUrl" => $notifyUrl,
			"secSignature" => $securitySignature,'posturl'=>$formPostUrl);
			 $data['pgtype'] = "citrus";
		}
		 if(isset($datapg['payu']) && $datapg['payu']['payu_merchantid']!='')
		{
			
			$amount =$postdata['TXN_AMOUNT'];
		$productinfo = 'bills';
		$fname = '';
		$email = '';
		$phone = '';
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		// get payumoney credential
		
	
		$MERCHANT_KEY = $datapg['payu']['payu_merchantkey'];
		$SALT = $datapg['payu']['payu_merchantsalt'];
		$PAYU_BASE_URL = $datapg['payu']['payu_authheader'];
		$udf1='';
		$udf2='';
		$udf3='';
		$udf4='';
		$udf5='';
		$hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $productinfo . '|'. $fname . '|' . $email .'|'.$udf1.'|' .$udf2.'|' .$udf3.'|'.$udf4.'|'.$udf5.'||||||'. $SALT;

        $hash = strtolower(hash('sha512', $hashstring));
        $data['hash'] = $hash;
	$success_url = base_url() . 'voucher/payumoney_success';
	$fail_url = base_url() . 'voucher/payumoney_fail';
	$cancle_url = base_url() . 'voucher/payumoney_cancle';
	$data['payumoneydetail'] = array('posturl'=>$PAYU_BASE_URL, 'mkey' => $MERCHANT_KEY, 'tid' => $txnid, 'amount' => $amount,
					 'name' => $fname, 'email'=>$email, 'phone' => $phone, 'productinfo' => $productinfo,
					 'success_url' => $success_url, 'fail_url' => $fail_url, 'cancle_url'=> $cancle_url);
					  $data['pgtype'] = "payu";
		}
		 if(isset($datapg['paytm']) && $datapg['paytm']['paytm_merchant_key']!='')
		{
		//	echo "rrrrrrr"; die;
			$data['userdata']['total_amt'] = $postdata['TXN_AMOUNT'];
        $data['userdata']['user_id'] = $session_data['id'];
        $data['userdata']['ORDER_ID'] = $postdata['ORDER_ID'];
        $data['userdata']['CUST_ID'] = $postdata['CUST_ID'];
        $data['userdata']['INDUSTRY_TYPE_ID'] = 'Retail';
        $data['userdata']['CHANNEL_ID'] = $postdata['CHANNEL_ID'];
        $data['userdata']['TXN_AMOUNT'] = $postdata['TXN_AMOUNT'];
        $data['userdata']['MOBILE_NO'] = $postdata['mobile_no'];
		 $data['pgtype'] = "paytm";
		 $data['paytm']=$datapg['paytm'];
		}
        

         
       // echo "<pre>"; print_R($data);die;



        $this->load->view('pg', $data);
       
        
           
        //die;
    }
    
    

    public function pg_success() {
        //echo "<pre>"; print_R($_POST); die;
         $session_data1 = $this->session->userdata('is_payment');
         if(isset($session_data1['form_submit']))
         {
           redirect(base_url() . "voucher");  
         }
         else
         {
             $sessarr=array("form_submit"=>1);
              $this->session->set_userdata('is_payment', $sessarr);
         }
       
        
                
       
        $session_data = $this->session->userdata('logged_in');
        if (!isset($session_data['id']) && $session_data['id'] != "" ) {
            redirect(base_url() . "voucher");
        }
        if(empty($_POST))
        {
              redirect(base_url() . "voucher");
        }
        $sessionvc_data = $this->session->userdata('vc_data');
        $sessionmsg_type = $this->session->userdata('target_msg');
        if($sessionmsg_type['payment_type']=="voucher")
        {
            //   echo "<pre>";print_R($session_data);die;
            if (isset($_POST['STATUS']) && $_POST['STATUS'] == "TXN_SUCCESS") {
		$transid=$this->voucher_model->payment_response(json_encode($_POST),'paytm');
                $this->voucher_model->add_voucher_location($session_data['id'], $sessionvc_data['vc_data'],$transid);
                $data['voucher']['voucher_list'] = $this->voucher_model->get_voucher_list();
                $data['voucher']['is_paid'] = 1;
				//echo "<pre>"; print_R($data);
				
                 $this->load->view('voucher',$data);
				 
                 

               
            } else if (empty($_POST)) {
                $this->load->view('paymentfail');
            } else {
				$this->voucher_model->payment_response(json_encode($_POST),'paytm');
              $this->load->view('paymentfail');
            }
        }
        else
        {
            $data=array();
         //   echo "<pre>"; print_R($sessionmsg_type); die;
             $session_data = $this->session->userdata('target_data');
              if (isset($_POST['STATUS']) && $_POST['STATUS'] == "TXN_SUCCESS") {
                  $ud=$this->voucher_model->get_user_data($session_data['dataid']);
             $data['msg_type']=$sessionmsg_type['msg_type'];
             $data['message']=$sessionmsg_type['msg'];
             $data['user_data']=$ud;
               
               $this->voucher_model->send_users_msg($data);
               $dataarr['msg']['is_sent'] = 1;
               $dataarr['msg']['total_user'] = count($ud);
               $dataarr['msg']['message'] = $sessionmsg_type['msg'];
                  $dataarr['msg']['msg_type']=$sessionmsg_type['msg_type'];
               //$this->session->unset_userdata('target_data');
                 //$this->session->unset_userdata('target_msg');
				 	$this->voucher_model->payment_response(json_encode($_POST),'paytm');
              $this->load->view('voucher', $dataarr);
               
            } else if (empty($_POST)) {
                 $this->load->view('paymentfail');
            } else {
					$this->voucher_model->payment_response(json_encode($_POST),'paytm');
               $this->load->view('paymentfail');
            }
             
          
            
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('vc_data');
        $this->session->sess_destroy();
        $this->load->view('voucher');
    }

    public function traffic_report() {
        $trafficdata = $this->voucher_model->traffic_report();
        $datarr = array();
        if ($trafficdata->resultCode == 1) {
            $i = 0;
            foreach ($trafficdata->datetimedata as $key => $val) {
                $datarr['data'][$i]['date'] = $key;
                $datarr['data'][$i]['uniqueuser'] = $val->uniqueuser;
                $datarr['data'][$i]['repeateduser'] = $val->repeateduser;
                $datarr['data'][$i]['dataused'] = $val->dataused;
                $i++;
            }
            $datarr['online_user'] = $trafficdata->online_user;
            $datarr['lifetime_user'] = $trafficdata->lifetime_user;
            $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            $datarr['totaldata'] = $trafficdata->totaldata;
            //  echo "<pre>"; print_R($datarr); die;
            echo json_encode($datarr);
        } else {
			$datarr['online_user'] = $trafficdata->online_user;
            $datarr['lifetime_user'] = $trafficdata->lifetime_user;
            $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            $datarr['totaldata'] = $trafficdata->totaldata;

            echo json_encode($datarr);
        }
    }
    
    public function check_sms_setup()
    {
     $data = $this->voucher_model->check_sms_setup();
      echo json_encode($data);
    }
    
     public function check_email_setup()
    {
      $data = $this->voucher_model->check_email_setup();
      echo json_encode($data);
    }

    public function retarget_user_count() {
        $targetdata = $this->voucher_model->retarget_user_count();
		//echo "<pre>"; print_R($targetdata); die;
        $datarr = array();
        if ($targetdata->resultCode == 1) {
            $datarr['target_user'] = $targetdata->targetusercount;
            echo json_encode($datarr);
        } else {
            $datarr['target_user'] = 0;
            echo json_encode($datarr);
        }
    }
	
	
	
    
    
      public function retarget_filter_count() {
        $targetdata = $this->voucher_model->retarget_filter_count();
        $this->session->unset_userdata('target_data');
        $datarr = array();
    //    echo "<pre>"; print_R($targetdata->userdata); die;
      
        //echo "<pre>"; print_R($userarr);
        if ($targetdata->resultcode == 1) {
            $datarr['target_user'] = $targetdata->total_user;
            
      
        
            $costdata=$this->voucher_model->voucher_cost_email($datarr['target_user']);
            //echo "<pre>"; print_R($costdata); die;
           
              $datarr['smstotal_cost']= round($costdata['smstotal_cost']);
              $datarr['emailtotalcost']= $costdata['emailtotalcost'];
              $datarr['smscost']= $costdata['smscost'];
              $datarr['emailcost']= $costdata['emailcost'];
                 $dataid=$this->voucher_model->send_sms_user($targetdata->userdata);
                $sess_array = array(
           
            'smstotal_cost' => round($costdata['smstotal_cost']),
             'emailtotalcost' => round($costdata['emailtotalcost']),
            'smscost' => round($costdata['smscost']),
            'emailcost' => round($costdata['emailcost']),     
            'total_user'=> $targetdata->total_user,
                   
                    'msg_type'=>1,
                    'dataid'=>$dataid
                 
        );
               
                  $this->session->set_userdata('target_data', $sess_array);
                
  
            echo json_encode($datarr);
        } else {
            $datarr['target_user'] = 0;
            echo json_encode($datarr);
        }
    }
    
    public function send_via_email()
    {
    $this->voucher_model->send_via_email('sandeep@shouut.com','sandeep','test');
    }
    
    public function payment_gateway_check()
    {
	$data=$this->voucher_model->payment_gateway_check();
	echo json_encode($data);
    }
    
   public function user_report() {
        $trafficdata = $this->voucher_model->user_report();
        $datarr = array();
        if ($trafficdata->resultCode == 1) {
            $i = 0;
            foreach ($trafficdata->datetimedata as $key => $val) {
                $datarr['data'][$i]['date'] = $key;
                $datarr['data'][$i]['uniqueuser'] = $val->uniqueuser;
                $datarr['data'][$i]['repeateduser'] = $val->repeateduser;
               // $datarr['data'][$i]['dataused'] = $val->dataused;
                $i++;
            }
            $datarr['online_user'] = $trafficdata->online_user;
            $datarr['lifetime_user'] = $trafficdata->lifetime_user;
            $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            //$datarr['totaldata'] = $trafficdata->totaldata;
            //  echo "<pre>"; print_R($datarr); die;
            echo json_encode($datarr);
        } else {
	    $datarr['online_user'] = $trafficdata->online_user;
	    $datarr['lifetime_user'] = $trafficdata->lifetime_user;
	    $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            echo json_encode($datarr);
        }
    }
    
     public function session_report() {
        $trafficdata = $this->voucher_model->session_report();
        $datarr = array();
        if ($trafficdata->resultCode == 1) {
            $i = 0;
            foreach ($trafficdata->datetimedata as $key => $val) {
                $datarr['data'][$i]['date'] = $key;
                $datarr['data'][$i]['uniqueuser'] = $val->uniqueuser;
                $datarr['data'][$i]['repeateduser'] = $val->repeateduser;
               // $datarr['data'][$i]['dataused'] = $val->dataused;
                $i++;
            }
            $datarr['online_user'] = $trafficdata->online_user;
            $datarr['lifetime_user'] = $trafficdata->lifetime_user;
            $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            //$datarr['totaldata'] = $trafficdata->totaldata;
            //  echo "<pre>"; print_R($datarr); die;
            echo json_encode($datarr);
        } else {
	    $datarr['online_user'] = $trafficdata->online_user;
	    $datarr['lifetime_user'] = $trafficdata->lifetime_user;
	    $datarr['total_user'] = $trafficdata->total_user;
            $datarr['unique_user'] = $trafficdata->unique_user;
            $datarr['repeated_user'] = $trafficdata->repeated_user;
            $datarr['calenderdate'] = $trafficdata->calenderdate;
            echo json_encode($datarr);
        }
    }

}
