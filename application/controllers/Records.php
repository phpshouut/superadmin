<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Records extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('records_model');
		if(!$this->session->has_userdata('isp_superadmin')){
			redirect(base_url().'login'); exit;
		}
	}

	public function index(){
		//echo str_pad('2', 3, "0", STR_PAD_RIGHT);
		$this->load->view('isp_users_records');
	}
	
	public function filterrecords(){
		$this->records_model->activeusers_permonth();
	}
	
}
