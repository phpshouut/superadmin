<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
    
    public function __construct(){
        parent :: __construct();
        $this->load->model('Settings_model', 'settings_model');
    }
    
    public function index(){
        $this->load->view('settings/isp_country');
    }
    
    public function country_listing(){
        $this->settings_model->country_listing();
    }
    
    public function configure_country(){
        $this->settings_model->configure_country();
    }
    
    public function countrydetails(){
        $this->settings_model->countrydetails();
    }
    
}


?>