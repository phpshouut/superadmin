<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class ManageISP extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		//$this->load->library('sftp');
		$this->load->model('ISP_model', 'isp_model');
		$this->load->model('Emailer_model', 'emailer_model');
		if(!$this->session->has_userdata('isp_superadmin')){
			redirect(base_url().'login'); exit;
		}
	}

	public function index(){
		$data = array();
		$data = $this->isp_model->isp_dashboard();
		$this->load->view('isp/isp_dashboard', $data);
	}
	public function getcitylist(){
		$stateid = $this->input->post('stateid');
		$this->isp_model->getcitylist($stateid);
	}
	public function getstatelist(){
		$this->isp_model->state_list();
	}
	public function recurse_copy(){
		$src = $_SERVER['DOCUMENT_ROOT']."/isp_portal";
		$dst = $_SERVER['DOCUMENT_ROOT']."/test.shouut.com";
		$ispurl = "test.shouut.com";
		$editorpath = $_SERVER['DOCUMENT_ROOT']."/test.shouut.com";
		
		$iscopy = $this->isp_model->recurse_copy($src,$dst);
		if($iscopy == '1'){
			//$this->isp_model->edit_configuration('test',$ispurl,$editorpath);
		}
	}
	
	public function shootactive_emailer(){
		//$this->emailer_model->activate_user_emailer('109');
	}
/*******************************************************************/
	
	public function status($type = NULL){
		$data = array();
		if(($type == 'act') || ($type == 'inact')|| ($type == 'demo')){
			$data = $this->isp_model->isp_dashboard();
			$data['isplisting'] = $this->isp_model->ispusers_listing($type);
			$data['user_type'] = $type;
			$this->load->view('isp/isp_userlisting', $data);
		}else{
			redirect(base_url().'manageISP');
		}
	}
	
/*******************************************************************/
	public function add_isp(){
		$this->load->view('isp/add_isp');
	}
	public function addtodb_ispuser(){
		$this->isp_model->addtodb_ispuser();
	}
	public function ispuser_details(){
		$this->isp_model->ispuser_details();
	}
	public function edit_isp($ispid=''){
		$data = array();
		if($ispid == ''){ $ispid = 0; }
		$data = $this->isp_model->loadispuser_onedit($ispid);
		$this->load->view('isp/edit_isp', $data);
	}
	public function portalurl_exists(){
		$this->isp_model->portalurl_exists();
	}
	
	
/********************************************************************/
	public function addtodb_ispmodules(){
		$this->isp_model->addtodb_ispmodules();
	}
	public function ispmodules_details(){
		$this->isp_model->ispmodules_details();
	}

	public function edittodb_ispmodules(){
		$this->isp_model->edittodb_ispmodules();
	}

/********************************************************************/
	public function checkuserstaus(){
		$this->isp_model->checkuserstaus();
	}
	public function activate_ispuser(){
		$this->isp_model->activate_ispuser();
	}
	
	public function softwareupdate(){
		$confirmupdate = $this->input->post('confirmupdate');
		if(isset($confirmupdate) && $confirmupdate == '1'){
			$this->isp_model->softwareupdate();
		}else{
			$this->load->view('software_update');
		}
	}
	
	public function ispbilling_details(){
		$check_country = $this->isp_model->ispbilling_details_check_country();
		if($check_country == '1'){
			$this->isp_model->ispbilling_details_other_country();
		}else{
			$this->isp_model->ispbilling_details();
		}
	}
	
	public function addlicense_toisp(){
		//echo '<pre>'; print_r($_POST); die;
		$this->isp_model->addlicense_toisp();
	}
	
	public function delete_inactive_isp(){
		$ispidsArr = $this->input->post('ispids');
		$explodeArr = explode(',', $ispidsArr);
		
		$this->isp_model->delete_inactive_isp($explodeArr);
	}
	
	public function add_publicpricing(){
		$this->isp_model->add_publicpricing();
	}
	
	public function publicpricing_details(){
		$this->isp_model->publicpricing_details();
	}
	
	public function copystates(){
		$this->isp_model->copystates();
	}
}
