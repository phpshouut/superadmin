<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ExceltoDB extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('Excel');
		$this->load->model('ExceltoDB_model', 'excel_model');
	}
	public function index(){
		$this->load->view("exceltodb");
		//$this->userexceltodb();
	}
	
	public function updatecreditlimit(){
		//$this->excel_model->update_creditlimit();
	}
	
	public function userexceltodb(){
		$isp_uid = $this->input->post('isp_uid');
		$file = './excelfiles/'.$isp_uid.'_users.xlsx';
		
		if(file_exists($file)){
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
			    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			    if ($row == 1) {
				$header[$row][$column] = $data_value;
			    } else {
				$arr_data[$row][$column] = $data_value;
			    }
			}
			 
			//send the data in an array format
			$data['header'] = $header;
			$data['values'] = $arr_data;
			echo '<pre>'; print_r($data); echo '</pre>'; die;
			//$this->excel_model->addUsertoDB($header,$arr_data);
		}else{
			echo "No such excel is associated with this user.";
		}
		
		//die;
	}
	
	
	    public function planexceltodb(){
		$isp_uid = $this->input->post('isp_uid');
		//$file = './excelfiles/'.$isp_uid.'_plan.xlsx';
               $file = './excelfiles/194_plan.xlsx';
		//echo "===>>".$file;
		if(file_exists($file)){
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			foreach ($cell_collection as $cell) {
			    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			    if ($row == 1) {
				$header[$row][$column] = $data_value;
			    } else {
				$arr_data[$row][$column] = $data_value;
			    }
			}
			 
			//send the data in an array format
			$data['header'] = $header;
			$data['values'] = $arr_data;
	//	echo '<pre>'; print_r($data);
			$this->excel_model->add_plan_data($header,$arr_data);
		}else{
			echo "No such excel is associated with this user.";
		}
		
		//die;
	}
	
	
	public function update_hotspot()
	{
		$getdata=$this->input->get();
		$cptype=(isset($getdata['cptype'])&&$getdata['cptype']!='')?$getdata['cptype']:'';
		$isp_uid=(isset($getdata['isp_uid'])&&$getdata['isp_uid']!='')?$getdata['isp_uid']:'';
		$router_type=(isset($getdata['routertype'])&&$getdata['routertype']!='')?$getdata['routertype']:'';
		$this->excel_model->update_hotspot($isp_uid,$cptype,$router_type);
		
	}
	
	public function update_planpricing(){
		$this->excel_model->update_planpricing();
	}
}

?>