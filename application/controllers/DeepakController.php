<?php
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');
class DeepakController extends CI_Controller {

    public function __construct(){
        parent :: __construct();
        $this->load->library('sftp');
         //$this->load->library('ftp');
        $this->load->model('deepak_model');
    }

    public function index(){
        phpinfo();
    }
    
    public function copy_server_files(){
        $this->deepak_model->copy_servertoserver();
    }
    
    public function test(){
        $this->deepak_model->test();
    }
	
	public function copy_newzealand_portal_code()
	{
		$this->deepak_model->copy_newzealand_portal_code();
	}
}
?>