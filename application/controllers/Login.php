<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('login_model');
		if($this->session->has_userdata('isp_superadmin')){
			redirect(base_url().'manageISP'); exit;
		}
	}

	public function index(){
		//echo str_pad('2', 3, "0", STR_PAD_RIGHT);
		$this->load->view('Login_view');
	}
	
	public function validate_user(){
		//echo '<pre>'; print_r($_POST);
		if(empty($_POST)){
		    redirect(base_url().'login'); exit;
		}
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim');
		$this->form_validation->set_rules('password', 'Password', 'trim|callback_check_database');
		
		if ($this->form_validation->run() == FALSE){
		    $this->load->view('Login_view');
		}else{
		    redirect(base_url().'manageISP');
		}
	}
	
	public function check_database($password){
	    $username = $this->input->post('username');
	    $result = $this->login_model->check_credentials($username, $password);
	 
	    if($result){
		//echo '<pre>'; print_r($result); die;
		$sess_array = array();
		foreach($result as $row){
		  $username = $row->username;
		  $sess_array = array(
		    'userid' => $row->id,
		    'username' => $username
		  );
		  $this->session->set_userdata('isp_superadmin', $sess_array);
		}
		return TRUE;
	    }else{
	      $this->form_validation->set_message('check_database', 'Username or Password is incorrect.');
	     return false;
	    }
	}
}
