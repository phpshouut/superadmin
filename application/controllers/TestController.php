<?php
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');
class TestController extends CI_Controller {

    public function __construct(){
        parent :: __construct();
        $this->load->library('sftp');
        $this->load->model('test_model');
    }

    public function index(){
        phpinfo();
    }
    
    //https://github.com/ipolar/CodeIgniter-sFTP-Library/blob/master/application/controllers/sftp_test.php
    public function copy_server_files(){
        $this->test_model->copy_servertoserver();
    }
}
?>