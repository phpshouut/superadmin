
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading hide">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">ISP ACTIVE USERS RECORDS</a>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="row" id="onefilter">
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="mui-select">
                                       <?php
                                       $monthArr = array('01' => 'Jan','02' => 'Feb','03' => 'Mar','04' => 'April','05' => 'May','06' => 'June','07' => 'July','08' => 'Aug','09' => 'Sep','10' => 'Oct','11' => 'Nov','12' => 'Dec');
                                       echo '
                                       <select name="filter_month" onchange="filterrecords(\'month\', this.value)">';
                                          $currm = date('m');
                                          foreach($monthArr as $mid => $month){
                                             $sel = '';
                                             if($mid == $currm){
                                                $sel = 'selected="selected"';
                                             }
                                             echo '<option value='.$mid.' '.$sel.'>'.$month.'</option>';
                                          }
                                       echo'   
                                       </select>
                                       <label>Filter By Month</label>';
                                       ?>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="mui-select">
                                       <?php
                                       $yearArr = array('2016','2017','2018');
                                       echo '
                                       <select name="filter_year" onchange="filterrecords(\'year\', this.value)">';
                                          $curry = date('Y');
                                          foreach($yearArr as $year){
                                             $sel = '';
                                             if($year == $curry){
                                                $sel = 'selected="selected"';
                                             }
                                             echo '<option value='.$year.' '.$sel.'>'.$year.'</option>';
                                          }
                                       echo'   
                                       </select>
                                       <label>Filter By Year</label>';
                                       ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="table-responsive">
                                 <table class="table table-striped">
                                    <thead>
                                       <tr class="active">
                                          <th>ISP NAME</th>
                                          <th>LEGAL NAME</th>
                                          <th>PORTAL URL</th>
                                          <th>EMAIL</th>
                                          <th>MOBILE</th>
                                          <th>MODULES</th>
                                          <th>MODULES PRICE</th>
                                          <th>ACTIVE USERS</th>
                                          <th>TOTAL PRICE</th>
                                       </tr>
                                    </thead>
                                    <tbody id="search_gridview">
                                       <?php $this->records_model->activeusers_permonth(); ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript">
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            var herader_height = $("#header").height();
            height = height - herader_height;
            //alert(height);
            $('#right-container-fluid').css('height', height);
         });
      </script>
      <script type="text/javascript">
         function filterrecords(filterby, filterval) {
            $('.loading').removeClass('hide');
            if (filterby == 'month') {
               var secfilter = $('select[name="filter_year"] option:selected').val();
            }else{
               var secfilter = $('select[name="filter_month"] option:selected').val();
            }
            $.ajax({
               url : base_url+"records/filterrecords",
               type: 'POST',
               data: 'filterby='+filterby+'&filterval='+filterval+'&secfilter='+secfilter,
               success: function(result){
                  $('#search_gridview').html(result);
                  $('.loading').addClass('hide');
               }
            });
         }
      </script>
   </body>
</html>

