

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">EDIT ISP</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <?php $onactivate = ($is_activated == 1) ? "disabled" : ''; ?>
                              <li style="margin: 5px" class="active_addc">
                                 <input data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger" type="checkbox" id="userstatus_changealert" value="<?php echo $is_activated ?>" style="height: 30px" <?php echo $onactivate; ?>>
                              </li>
                              <!--<li>
                                 <a href="<?php //echo base_url().'manageISP' ?>">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 SAVE & EXIT 
                                 </button>
                                 </a>
                              </li>-->
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="mui--appbar-height"></div>
                     <div class="add_user" style="padding-top:0px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                       <li class="mui--is-active">
                                          <a data-mui-toggle="tab" data-mui-controls="info" onclick="infotab_click()">INFO</a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="modules" onclick="moduletab_click()">
                                          MODULES
                                          </a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="billing" onclick="billingtab_click()">
                                          BILLING
                                          </a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="complaints">
                                          COMPLAINTS
                                          </a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="mui--appbar-height"></div>
                                    <div class="mui-tabs__pane mui--is-active" id="info">
                                       <form action="" method="post" id="isp_userform" autocomplete="off" onsubmit="add_ispuser(); return false;">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_name" value="<?php echo $isp_name ?>" required>
                                                      <label>ISP Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_legal_name" value="<?php echo $legal_name ?>" required>
                                                      <label>Legal Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="email" name="isp_email" value="<?php echo $email ?>" required>
                                                      <label>Email <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="row">
                                                      <div class="col-lg-8 col-md-8 col-sm-8 nopadding-left">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                            <?php $onactivate = ($is_activated == 1) ? "readonly" : ''; ?>
                                                           <!--pattern="[A-Za-z]*$"-->
                                                           <input type="text" name="isp_portal_url" value="<?php echo $isp_domain_name ?>" required <?php echo $onactivate; ?>>
                                                           <label>ISP Portal url <sup>*</sup></label>
                                                         </div>
                                                         <label style="color:#f00" class="portal_err hide">This Portal is already associated with us.</label>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 nopadding-left" style="padding-top:25px">
                                                         <!--<span class="urltype">.shouut.com</span>-->
                                                      </div>
                                                   </div> 
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_mobile" value="<?php echo $phone ?>" required>
                                                      <label>Mobile <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ispuuid" >
                                                   <div class="mui-textfield">
                                                      <input type="text" name="ispadmin_uid" value="<?php echo $isp_uid ?>" required readonly>
                                                      <label>ISP UID <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_gstin_number" value="<?php echo $gst_no ?>" required>
                                                      <label>GSTIN NUMBER <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <?php if($is_activated == 1){
                                                if($domain_exists == '0'){
                                                   if($country_id == 38){
                                                      $isp_weburl = 'http://a3.shouut.com/ispadmins/'.$isp_domain_name;
                                                   }else{
                                                      $isp_weburl = 'https://www.shouut.com/ispadmins/'.$isp_domain_name;
                                                   }
                                                   
                                                }else{
                                                   $isp_weburl = 'https://'.$isp_domain_name;
                                                }
                                             ?>
                                             <div class="row">
                                                <span>Key Password: </span><i class='fa fa-key' aria-hidden='true'></i>&nbsp; <?php echo $orig_pwd; ?> <br/>
                                                <?php echo '<a href="'.$isp_weburl.'" alt="" target="_blank">https://'.$isp_domain_name.'</a>'; ?> &nbsp;&nbsp;
                                                <?php echo '<a href="'.$isp_weburl.'/consumer" alt="" target="_blank">https://'.$isp_domain_name.'/consumer</a>'; ?>
                                             </div>
                                             <?php } ?>
                                             <?php
                                             if(isset($isp_states) && count($isp_states) > 0){
                                                echo '
                                                <div>
                                                   <div class="row">
                                                      <h4>REGION</h4>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                         <div class="mui-select">
                                                            <select name="isp_country" onchange="getstatelist_forisp(this.value)" disabled required>
                                                               <option value="">Select Country *</option>';
                                                               $this->isp_model->country_list($country_id);
                                                      echo '</select>
                                                            <label>Select Country</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                ';
                                                $total_states = $this->isp_model->count_country_states($country_id);
                                                $selected_states = count($isp_states);
                                                if($total_states != $selected_states){
                                                   echo '<div class="morestate">';
                                                   for($sc=0; $sc < count($isp_states); $sc++){
                                                      $stateid = $isp_states[$sc];
                                                      if($sc == 0){
                                                         echo '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" class="lic_cbsts" onchange="license_states(this.value)" required><option value="">Select State *</option>';
                                                         $this->isp_model->state_list($stateid,$country_id);
                                                         echo '</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 addmore_states"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="addmorestate_toisp()">ADD MORE</span></div></div>';
                                                      }else{
                                                         echo '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" class="lic_cbsts" required>
                                                         <option value="">Select State *</option>';
                                                         $this->isp_model->state_list($stateid,$country_id);
                                                         echo '</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="remove_stateoption(this)"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>';
                                                      }
                                                   }
                                                   echo '</div>';
                                                }else{
                                                   echo '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" class="lic_cbsts" onchange="license_states(this.value)" required><option value="">Select State *</option><option value="all" selected>All States</option>';
                                                         $this->isp_model->state_list($stateid,$country_id);
                                                         echo '</select><label>Select STATE</label></div></div></div>';
                                                }
                                          echo '</div>';
                                             }else{
                                             ?>
                                             
                                             <div class="row">
                                                <h4>REGION</h4>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="isp_state[]" required>
                                                         <option value="">Select State *</option>
                                                         <?php //$this->isp_model->state_list(); ?>
                                                      </select>
                                                      <label>Select STATE</label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <span class="mui-btn mui-btn--small mui-btn--accent" onclick="addmorestate_toisp()">ADD MORE</span>
                                                </div>
                                             </div>
                                             
                                             <?php } ?>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                                   <input type="submit" class="mui-btn mui-btn--accent" value="SAVE" />
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="mui-tabs__pane" id="modules">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <!--
                                          <form action="" method="post" id="isp_modulesform" autocomplete="off" onsubmit="edit_ispmodules(); return false;">
                                             <input type="hidden" name="ispadmin_uid" value="<?php echo $isp_uid ?>" class="ispadmin_uid">
                                             <div class="row">
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <h4>HOME WIFI</h4>
                                                      <div class="row">
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                               <div class="row left_module"></div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                               <div class="row right_module"></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <h4>PUBLIC WIFI</h4>
                                                         <div class="row pwifi_module"></div>
                                                      </div>
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <h4>SHOUUT CHANNEL</h4>
                                                         <div class="row shchannel_module"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <h4>RESELLER PORTAL</h4>
                                                         <div class="row reseller_module"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                                                      <input type="submit" class="mui-btn mui-btn--accent" value="SAVE" id="savemodulesbtn" />
                                                   </div>
                                                </div>
                                             </div>
                                          </form>-->
                                          
                                          <form action="" method="post" id="isp_modulesform" autocomplete="off" onsubmit="edit_ispmodules(); return false;">
                                             <input type="hidden" name="ispadmin_uid" value="<?php echo $isp_uid ?>" class="ispadmin_uid">
                                             <div class="row">
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="row">
                                                         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <div class="checkbox"><label><input name="homewifi_module" type="checkbox" value="1,2,3,4"><strong style="font-weight:700">HOME WIFI</strong></label><div></div></div>
                                                         </div>
                                                         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <div class="checkbox"><label><input name="publicwifi_module" type="checkbox" value="7,8,9"><strong style="font-weight:700">PUBLIC WIFI</strong></label><div></div></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                                                   <input type="submit" class="mui-btn mui-btn--accent" value="SAVE" id="savemodulesbtn" />
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>

                                    <div class="mui-tabs__pane" id="billing">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
                                          <div class="row">
                                             <div class="col-sm-12 col-xs-12">
                                                <h2>Currently Active :  <span id="acctype"></span></h2>
                                                <div class="billing_list">
                                                   <ul>
                                                      <li style="margin-bottom:15px"><a href="javascript:void(0)" style="margin-left:0px" onclick="show_planModalBox()">Recharge License</a></li>
                                                      <li style="margin-bottom:15px"><a href="#">Assign Reseller</a></li>
                                                      <li style="margin-bottom:15px">
                                                         <a href="javascript:void(0)" onclick="public_pricingModal()">Public Wifi Pricing</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>License Balance</strong></label>
                                                      <h2 id="license_balance"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Home License Used</strong></label>
                                                      <h2 id="home_license_used"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Public WiFi Lisence Used</strong></label>
                                                      <h2 id="public_license_used"></h2>
                                                   </div>
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                      <label><strong>Total Lisence Used</strong></label>
                                                      <h2 id="total_license_used"></h2>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-12 col-xs-12">
                                                <div class="row">
                                                   <div class="col-sm-12 col-xs-12">
                                                      <ul class="mui-tabs__bar" style="border-bottom:none">
                                                         <li class="mui--is-active">
                                                            <a data-mui-toggle="tab" data-mui-controls="pane-default-1">Usage Report</a>
                                                         </li>
                                                         <li>
                                                            <a data-mui-toggle="tab" data-mui-controls="pane-default-2">Billing report</a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                   <div class="col-sm-12 col-xs-12">
                                                      <div class="mui-tabs__pane mui--is-active" id="pane-default-1">
                                                         <div class="col-sm-12 col-xs-12">
                                                            <div class="row" style="margin-top: 15px">
                                                               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                                  <div class="mui-select" tabindex="0">
                                                                     <select name="year_filter" tabindex="-1">
                                                                        <option value="">Select Year</option>
                                                                        <?php
                                                                        for($i=2017; $i<= date('Y'); $i++){
                                                                           echo '<option value="'.$i.'">'.$i.'</option>';
                                                                        }
                                                                        ?>
                                                                     </select>
                                                                     <label>Select Year</label>
                                                                     <div class="mui-event-trigger"></div>
                                                                  </div>
                                                               </div>
                                                               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                  <div class="mui-select" tabindex="0">
                                                                     <select name="month_filter" tabindex="-1">
                                                                        <option value="">Select Month</option>
                                                                        <?php
                                                                        for($i=1; $i<= 12; $i++){
                                                                           echo '<option value="'.$i.'">'.$i.'</option>';
                                                                        }
                                                                        ?>
                                                                     </select>
                                                                     <label>Select Month</label>
                                                                     <div class="mui-event-trigger"></div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="row">
                                                                     <div class="table-responsive">
                                                                        <table class="table table-striped">
                                                                           <thead>
                                                                              <tr class="active">
                                                                                 <th>&nbsp;</th>
                                                                                 <th>MONTH</th>
                                                                                 <th>HOME LISENCES USED</th>
                                                                                 <th>PUBLIC LISENCES USED</th>
                                                                                 <th>TOTAL LISENCES VALUE (Rs.)</th>
                                                                              </tr>
                                                                           </thead>
                                                                           <tbody id="usuage_logs">
                                                                              <tr><td colspan="5">No Records Found</td></tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="mui-tabs__pane" id="pane-default-2">
                                                         <div class="row" style="margin-top:15px">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                               <div class="row">
                                                                  <div class="table-responsive">
                                                                     <table class="table table-striped">
                                                                        <thead>
                                                                           <tr class="active">
                                                                              <th>&nbsp;</th>
                                                                              <th>BILL NUMBER </th>
                                                                              <th>AMOUNT</th>
                                                                              <th>TAXES</th>
                                                                              <th>DATE</th>
                                                                              <th>MODE OF PAYMENT</th>
                                                                           </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                           <!--<tr>
                                                                              <td>1.</td>
                                                                              <td>123123123 </td>
                                                                              <td>25000</td>
                                                                              <td>12300</td>
                                                                              <td>10 Jan 2017 </td>
                                                                              <td><span class="Closed" style="font-weight: 400">Online</span></td>
                                                                           </tr>-->
                                                                           <tr><td colspan="6">No Records Found</td></tr>
                                                                        </tbody>
                                                                     </table>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
</div>
                                 <div class="mui-tabs__pane" id="complaints">complaints</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div id="planModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" role="document">
            <form action="" method="post" id="isp_planform" autocomplete="off" onsubmit="add_planbudget(); return false;">
               <div class="modal-content">
                  <div class="modal-header" style="padding:5px 15px">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body" >
                        <div class="col-sm-12 col-xs-12">
                              <div class="form-group" style="margin-bottom:0px">
                                 <div class="row">
                                    <div class="col-sm-6 col-xs-6" style="padding:0px">
                                       <h2 style="margin:0 0 10px">Choose the License</h2>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group" style="margin-bottom:0px">
                                 <div class="row">
                                    <div class="table-responsive">
                                       <table class="table table-striped" style="margin-bottom:0px">
                                          <tr align="center">
                                             <th rowspan="2" width="30%" align="center">
                                                <center>Prepaid License<br/> Fees (INR)</center>
                                             </th>
                                             <th colspan="2">
                                                <center>Per License Rate (INR)</center>
                                             </th>
                                          </tr>
                                          <tr align="center">
                                             <th>
                                                <center>Home WiFi(Per Active)</center>
                                             </th>
                                             <th>
                                                <center>Public WiFi Per AP</center>
                                             </th>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="25000" data-homewifi="10" data-publicwifi="150" required /></span> 25,000</td>
                                             <td>10</td>
                                             <td>150</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="50000" data-homewifi="10" data-publicwifi="140" required /></span> 50,000</td>
                                             <td>10</td>
                                             <td>140</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="100000" data-homewifi="9" data-publicwifi="125" required /></span> 1,00,000</td>
                                             <td>9</td>
                                             <td>125</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="250000" data-homewifi="8" data-publicwifi="110" required /></span> 2,50,000</td>
                                             <td>8</td>
                                             <td>110</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="500000" data-homewifi="7" data-publicwifi="100" required /></span> 5,00,000</td>
                                             <td>7</td>
                                             <td>100</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="1000000" data-homewifi="6" data-publicwifi="100" required /></span> 10,00,000</td>
                                             <td>6</td>
                                             <td>100</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="2500000" data-homewifi="5" data-publicwifi="100" required /></span> 25,00,000</td>
                                             <td>5</td>
                                             <td>100</td>
                                          </tr>
                                          <tr align="center">
                                             <td style="text-align:left; padding-left:20px"><span style="display:inline-block; width:20px">
                                             <input type="radio" name="select_module" value="custom" required /></span> Custom License</td>
                                             <td>&nbsp;</td>
                                             <td>&nbsp;</td>
                                          </tr>
                                          <tr align="center">
                                             <td>
                                                <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                                                   <input type="text" class="customlicense" name="custom_license_fee" id="custom_license_fee" disabled="disabled">
                                                   <label>Enter Prepaid License Fees</label>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                                                   <input type="text" class="customlicense" name="custom_license_homewifi" id="custom_license_homewifi" disabled="disabled">
                                                   <label>Enter Home WiFi(Per Active)</label>
                                                </div>
                                             </td>
                                             <td>
                                                <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                                                   <input type="text" class="customlicense" name="custom_license_publicwifi" id="custom_license_publicwifi" disabled="disabled">
                                                   <label>Enter Public WiFi (Per AP)</label>
                                                </div>
                                             </td>
                                          </tr>
                                          
                                       </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group" style="margin-bottom:20px;">
                                 <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                       <div class="row">
                                          <div class="col-sm-4 col-xs-4">
                                             <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                                                <input type="text" name="license_cheque_dd_neft" id="license_cheque_dd_neft">
                                                <label>Enter Cheque/DD/NEFT No.</label>
                                             </div>
                                          </div>
                                          <div class="col-sm-4 col-xs-4">
                                             <div class="mui-select" style="margin-bottom:0px">
                                                <select name="license_discount">
                                                   <option value="">Select Discount</option>
                                                   <?php for($i=1; $i<=100; $i++){
                                                         echo '<option value="'.$i.'">'.$i.' %</option>';
                                                      } ?>
                                                   
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                        </div>
                  </div>
                  <div class="modal-footer" style="margin-bottom:10px;">
                     <div class="col-lg-9">
                        <input type="submit" class="paybtn btn mui-btn--accent" style="width: 130px ; line-height: 20px; height:40px;" value="SAVE"/>
                        <span class="btn btn-default" onclick="reset_planmodule()">Reset</span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>


      <div class="modal fade" id="user_activatedModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ACTIVATE USER?</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
		  <input type="hidden" id="otp_to_activate_user" value="" />
		  <input type="hidden" id="phone_to_activate_user" value="" />
                  <p>To Activate <span id="activate_username"></span>, please enter the OTP sent to your regestiered mobile no. <span id="activate_userphone"></span>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="enter_otp_to_activate_user" id="enter_otp_to_activate_user">
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="activate_otp_err"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="active_user_confirmation()">ACTIVATE</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="pendingAlert" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm" style="width:500px">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ISP CANNOT BE MADE ACTIVE</strong>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Please check these details to make ISP Active</p>
		  <ul style="margin:15px;list-style-type: none">
		     <li><span id="infoactive"></span> &nbsp; Complete ISP Details</li>
		     <li><span id="modactive"></span> &nbsp; Select atleast one Module</li>
		  </ul>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
               </div>
            </div>
         </div>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="ppModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="margin-top:4%">
            <form action="" method="post" id="publicpricing_form" autocomplete="off" onsubmit="add_publicpricing(); return false;">
               <input type="hidden" name="isp_uid" id="isp_uid" value="<?php echo $isp_uid; ?>" />
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title">
                        <strong>Public WiFi Pricing Details</strong>
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-select">
                              <select name="public_currency" class="form-control" required>
                                 <option value="">Select Currency <sup>*</sup></option>
                                 <?php echo $this->isp_model->getcurrencylists(); ?>
                              </select>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="public_license_fee"  required>
                              <label>Set License Fee<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-select">
                              <select name="public_billing_frequency" class="form-control" required>
                                 <option value="">Select Billing Frequency <sup>*</sup></option>
                                 <option value="M">Monthly</option>
                                 <option value="Y">Yearly</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SAVE" >
                  </div>
               </div>
            </form>
         </div>
      </div>
      
      
      
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js?version=3.1"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script>
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            $('#right-container-fluid').css('height', height);
            window.scrollTo(0,0);
            setTimeout(function (){ $('.loading').addClass('hide') },1000);
            var active_user = "<?php echo $is_activated; ?>";
	    if (active_user == '1') {
	       $('#userstatus_changealert').parent().removeClass('btn-danger off');
               $('#userstatus_changealert').parent().addClass('btn-success on');
	    }else{
	       $('#userstatus_changealert').val(0);
	       $('#userstatus_changealert').parent().removeClass('btn-success');
	       $('#userstatus_changealert').parent().addClass('btn-danger off');
	    }
            
            $('input[type=radio][name=select_module]').change(function() {
               if (this.value == 'custom') {
                  $('.customlicense').removeAttr('disabled');
               }else{
                  $('.customlicense').val('');
                  $('.customlicense').attr('disabled', 'disabled'); 
               }
            });

            $('.customlicense').change(function() {
               var e = 0;
               $('.customlicense').each(function(i, obj) {
                  if (this.value == '') {
                     $('.customlicense').attr('required', 'required');
                     e++;
                  }
               });
               /*if (e > 0) {
                  $('input[type=radio][name=select_module]').attr('disabled', 'disabled');   
               }else{
                  $('.customlicense').removeAttr('required');
                  $('input[type=radio][name=select_module]').removeAttr('disabled');      
               }*/
            });
         });
         function reset_planmodule() {
            $('.customlicense').val('');
            $('.customlicense').attr('disabled', 'disabled'); 
            $('#isp_planform')[0].reset();
         }
         function show_planModalBox() {
            $('#planModal').modal('show');
            $('.paybtn').removeClass('hide');
            $('#isp_planform')[0].reset();
         }
         
         function getstatelist_forisp(countryid) {
            $.ajax({
                url: base_url+'manageISP/getstatelist',
                type: 'POST',
                data: 'countryid='+countryid,
                async: false,
                success: function(data){
                  $('.morestate').html('');
                  $('.lic_cbsts').empty();
                  $('.lic_cbsts').append('<option value="">Select State *</option><option value="all">All States</option>');
                  $('.lic_cbsts').append(data);
                }
            });
         }
         
         function license_states(licsid) {
            if (licsid == 'all') {
               $('.addmore_states').addClass('hide');
               $('.morestate').html('');
            }else{
               $('.addmore_states').removeClass('hide');
            }
         }
         
         function public_pricingModal() {
            var isp_uid = "<?php echo $isp_uid ?>";
            $.ajax({
               url: base_url+'manageISP/publicpricing_details',
               type: 'POST',
               data: 'isp_uid='+isp_uid,
               dataType: 'json',
               success: function(data){
                  $('select[name="public_currency"]').val(data.currency_id);
                  $('select[name="public_billing_frequency"]').val(data.billing_frequency);
                  $('input[name="public_license_fee"]').val(data.cost_per_location);
                  $('#ppModal').modal('show');
               }
            });
         }
         
         function add_publicpricing() {
            var formdata = $('#publicpricing_form').serialize();
            $.ajax({
               url: base_url+'manageISP/add_publicpricing',
               type: 'POST',
               data: formdata,
               async: false,
               success: function(data){
                 $('#ppModal').modal('hide');
               }
            });
         }
      </script>
   </body>
</html>

