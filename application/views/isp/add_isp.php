

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">ADD ISP</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li style="margin: 5px" class="active_addc hide">
                                 <input data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger" type="checkbox" id="userstatus_changealert" value="" style="height: 30px">
                              </li>
                              <li>
                                 <a href="<?php echo base_url().'manageISP' ?>">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 SAVE & EXIT 
                                 </button>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="mui--appbar-height"></div>
                     <div class="add_user" style="padding-top:0px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                       <li class="mui--is-active">
                                          <a data-mui-toggle="tab" data-mui-controls="info" onclick="infotab_click()">INFO</a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="modules" onclick="moduletab_click()">
                                          MODULES
                                          </a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="billing">
                                          BILLING
                                          </a>
                                       </li>
                                       <li>
                                          <a data-mui-toggle="tab" data-mui-controls="complaints">
                                          COMPLAINTS
                                          </a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="mui--appbar-height"></div>
                                    <div class="mui-tabs__pane mui--is-active" id="info">
                                       <form action="" method="post" id="isp_userform" autocomplete="off" onsubmit="add_ispuser(); return false;">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_name" required>
                                                      <label>ISP Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_legal_name" required>
                                                      <label>Legal Name <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="email" name="isp_email" required>
                                                      <label>Email <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="row">
                                                      <div class="col-lg-8 col-md-8 col-sm-8 nopadding-left">
                                                         <div class="mui-textfield mui-textfield--float-label">
                                                           <input type="text" name="isp_portal_url" pattern="[A-Za-z]*$" required>
                                                           <label>ISP Portal url <sup>*</sup></label>
                                                         </div>
                                                         <label style="color:#f00" class="portal_err hide">This Portal is already associated with us.</label>
                                                      </div>
                                                      <div class="col-lg-4 col-md-4 col-sm-4 nopadding-left" style="padding-top:25px">
                                                         <span class="urltype">.shouut.com</span>
                                                      </div>
                                                   </div> 
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <div class="mui-textfield mui-textfield--float-label">
                                                      <input type="text" name="isp_mobile" pattern="[789][0-9]{9}" required>
                                                      <label>Mobile <sup>*</sup></label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ispuuid hide" >
                                                   <div class="mui-textfield">
                                                      <input type="text" name="ispadmin_uid" required readonly>
                                                      <label>ISP UID <sup>*</sup></label>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                             <?php
                                             if(isset($isp_states) && count($isp_states) > 0){
                                                echo '
                                                <div class="morestate">
                                                   <div class="row">
                                                      <h4>REGION</h4>
                                                   </div>
                                                ';
                                                for($sc=0; $sc < count($isp_states); $sc++){
                                                   $stateid = $isp_states[$sc];
                                                   if($sc == 0){
                                                      echo '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" required>
                                                      <option value="">Select State *</option>';
                                                      $this->isp_model->state_list($stateid);
                                                      echo '</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="addmorestate_toisp()">ADD MORE</span></div></div>';
                                                   }else{
                                                      echo '<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" required>
                                                      <option value="">Select State *</option>';
                                                      $this->isp_model->state_list($stateid);
                                                      echo '</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="remove_stateoption(this)"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>';
                                                   }
                                                }
                                          echo '</div>';
                                             }else{
                                             ?>
                                             
                                             <div class="row">
                                                <h4>REGION</h4>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;">
                                                   <div class="mui-select">
                                                      <select name="isp_state[]" required>
                                                         <option value="">Select State *</option>
                                                         <?php $this->isp_model->state_list(); ?>
                                                      </select>
                                                      <label>Select STATE</label>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                   <span class="mui-btn mui-btn--small mui-btn--accent" onclick="addmorestate_toisp()">ADD MORE</span>
                                                </div>
                                             </div>
                                             <div class="morestate"></div>
                                             
                                             <?php } ?>
                                             
                                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nomargin-left">
                                                   <input type="submit" class="mui-btn mui-btn--accent" value="NEXT" />
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="mui-tabs__pane" id="modules">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <form action="" method="post" id="isp_modulesform" autocomplete="off" onsubmit="add_ispmodules(); return false;">
                                             <input type="hidden" name="ispadmin_uid" class="ispadmin_uid">
                                             <div class="row">
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <h4>HOME WIFI</h4>
                                                      <div class="row">
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                               <div class="row left_module"></div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                               <div class="row right_module"></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <h4>PUBLIC WIFI</h4>
                                                         <div class="row pwifi_module"></div>
                                                      </div>
                                                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                         <h4>SHOUUT CHANNEL</h4>
                                                         <div class="row shchannel_module"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
                                                      <input type="submit" class="mui-btn mui-btn--accent" value="SAVE" id="savemodulesbtn" />
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>

                                    <div class="mui-tabs__pane" id="billing">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px">
                                          <div class="row">
                                             <div class="col-sm-4 col-xs-4">
                                                <h2> WALLET : <span>00</span></h2>
                                             </div>
                                             <div class="col-sm-4 col-xs-4">
                                                <h2>CREDIT LIMIT : <span>00</span></h2>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="row">
                                             <div class="table-responsive">
                                                <table class="table table-striped">
                                                   <thead>
                                                      <tr class="active">
                                                         <th>&nbsp;</th>
                                                         <th>MONTH</th>
                                                         <th>ACTIVE</th>
                                                         <th>USERS</th>
                                                         <th>BILL.NO.</th>
                                                         <th>RECEIPT.NO.</th>
                                                         <th>STATUS</th>
                                                         <th>AMOUNT</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      <tr>
                                                         <td>1.</td>
                                                         <td>May</td>
                                                         <td>dd.mm.yyyy</td>
                                                         <td>shouut</td>
                                                         <td>999</td>
                                                         <td>999</td>
                                                         <td><span class="Closed">Paid</span></td>
                                                         <td>₹ 999 / mo</td>
                                                      </tr>
                                                      <tr>
                                                         <td>2.</td>
                                                         <td>May</td>
                                                         <td>dd.mm.yyyy</td>
                                                         <td>shouut</td>
                                                         <td>999</td>
                                                         <td>999</td>
                                                         <td><span class="open">Pending</span></td>
                                                         <td>₹ 999 / mo</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="mui-tabs__pane" id="complaints">complaints</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="user_activatedModal" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>ACTIVATE USER?</strong></h4>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
		  <input type="hidden" id="otp_to_activate_user" value="" />
		  <input type="hidden" id="phone_to_activate_user" value="" />
                  <p>To Activate <span id="activate_username"></span>, please enter the OTP sent to your regestiered mobile no. <span id="activate_userphone"></span>
                  </p>
                  <div class="mui-textfield mui-textfield--float-label" style="margin-bottom:0px">
                     <input type="text" name="enter_otp_to_activate_user" id="enter_otp_to_activate_user">
                     <label>Enter 4-Digit Mobile No.</label>
                  </div>
		  <label id="activate_otp_err"></label>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                  <button type="button" class="mui-btn mui-btn--large mui-btn--accent" onclick="active_user_confirmation()">ACTIVATE</button>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade" id="pendingAlert" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm" style="width:500px">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>ISP CANNOT BE MADE ACTIVE</strong>
               </div>
               <div class="modal-body" style="padding-bottom:5px">
                  <p>Please check these details to make ISP Active</p>
		  <ul style="margin:15px;list-style-type: none">
		     <li><span id="infoactive"></span> &nbsp; Complete ISP Details</li>
		     <li><span id="modactive"></span> &nbsp; Select atleast one Module</li>
		  </ul>
               </div>
               <div class="modal-footer" style="text-align: right">
                  <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
               </div>
            </div>
         </div>
      </div>
      
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script>
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            $('#right-container-fluid').css('height', height);
             window.scrollTo(0,0);
            setTimeout(function (){ $('.loading').addClass('hide') },1000);
         
            // on user type change change page(form)
            $('input[type=radio][name=user_type_radio]').change(function() {
               if (this.value == 'lead' || this.value == 'enquiry') {
            	  $("#user_type_lead_enquiry").show();
                  $("#user_type_customer").hide();
               }else if (this.value == 'customer') {
            	  $("#user_type_customer").show();
                  $("#user_type_lead_enquiry").hide();
               }
            }); 
         });
      </script>
   </body>
</html>

