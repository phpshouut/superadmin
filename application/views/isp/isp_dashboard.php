

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading" style="display:none">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">ISP</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li>
                                 <!--<a href="<?php echo base_url().'manageISP/add_isp' ?>">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD ISP
                                 </button>
                                 </a>-->
                                 <a href="<?php echo 'https://www.shouut.com/wifi/shop.html' ?>" target="_blank" alt="">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD ISP
                                 </button>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-right">
                                 <div class="row">
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="activeisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#ffbc00;">
                                          <div class="caption">
                                             <h5>LIVE ISP'S </h5>
                                             <h4><?php echo $active ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="demoisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#29ABE2;">
                                          <div class="caption">
                                             <h5>DEMO ISP'S</h5>
                                             <h4><?php echo $demoacct ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="inactiveisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#F4474A;">
                                          <div class="caption">
                                             <h5>INACTIVE ISP'S</h5>
                                             <h4><?php echo $inactive ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="serch_box">
                                 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-md-offset-3">
                                    <div class="input-group" id="adv-search">
                                       <input type="text" class="isp-serch-form-control form-control" placeholder="Search ISP" onFocus="this.placeholder=''" onBlur="this.placeholder=' Search ISP'" style="box-shadow:none" id="search_input"/>
                                       <div class="input-group-btn">
                                          <div class="btn-group" role="group">
                                             <div class="dropdown dropdown-lg">
                                                <button type="button" class="btn btn-primary" id="search_filter" style="border-bottom-right-radius :4px; border-top-right-radius :4px;">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right"  id="search_filter_areas" style="border-radius:0px; margin-top: 2px; min-width: 480px;">
                                                   <div class="row">
                                                      <div class="search-dropdown">
                                                         <h5>SHOUUT</h5>
                                                      </div>
                                                      <div class="search-dropdown">
                                                         <h5>SHOUUT-1</h5>
                                                      </div>
                                                      <div class="search-dropdown">
                                                         <h5>SHOUUT-2</h5>
                                                      </div>
                                                      <div class="search-dropdown">
                                                         <h5>SHOUUT-3</h5>
                                                      </div>
                                                      <div class="search-dropdown">
                                                         <h5>SHOUUT-4</h5>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript">
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            var herader_height = $("#header").height();
            height = height - herader_height;
            //alert(height);
            $('#right-container-fluid').css('height', height);
         });
      </script>
      <script type="text/javascript">
         $('body').on('click', '#activeisplist', function(){
            window.location.href = base_url+"manageISP/status/act";
         });
         $('body').on('click', '#demoisplist', function(){
            window.location.href = base_url+"manageISP/status/demo";
         });
         $('body').on('click', '#inactiveisplist', function(){
            window.location.href = base_url+"manageISP/status/inact";
         });
      </script>
   </body>
</html>

