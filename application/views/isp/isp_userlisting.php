
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading" style="display:none">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">ISP</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                           <ul class="nav navbar-nav navbar-right">
                              <li>
                                 <!--<a href="<?php echo base_url().'manageISP/add_isp' ?>">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD ISP
                                 </button>
                                 </a>-->
                                 <a href="<?php echo 'https://www.shouut.com/wifi/shop.html' ?>" target="_blank" alt="">
                                 <button class="mui-btn mui-btn--small mui-btn--accent">
                                 + ADD ISP
                                 </button>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="right_side">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding-right">
                                 <div class="row">
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="activeisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#ffbc00;">
                                          <div class="caption">
                                             <h5>LIVE ISP'S </h5>
                                             <h4><?php echo $active ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i></h4>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="demoisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#29ABE2;">
                                          <div class="caption">
                                             <h5>DEMO ISP'S</h5>
                                             <h4><?php echo $demoacct ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 col-sm-3 col-md-3 plans-padding" id="inactiveisplist" style="cursor:pointer">
                                       <div class="plans-thumbnail" style="background-color:#F4474A;">
                                          <div class="caption">
                                             <h5>INACTIVE ISP'S</h5>
                                             <h4><?php echo $inactive ?>
                                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="row">
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="mui-select">
                                       <select name="filter_state" onchange="search_filter(this.value, 'state')">
                                          <option value="">All States</option>
                                          <?php $this->isp_model->state_list(); ?>
                                       </select>
                                       <label>State</label>
                                    </div>
                                 </div>
                                 <?php if($user_type == 'inact'){ ?>
                                 <div class="col-lg-2 col-md-2 col-sm-2 hide" id="delete_records">
                                    <a href="javascript:void(0)" onclick="delete_inactive_isp()" alt="">
                                       <button class="mui-btn mui-btn--small mui-btn--accent">DELETE</button>
                                    </a>
                                 </div>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <div class="table-responsive">
                                 <table class="table table-striped">
                                    <thead>
                                       <tr class="active">
                                          <th>&nbsp;</th>
                                          <th>
                                             <div class="checkbox" style="margin-top:0px; margin-bottom:0px;">
                                                <label>
                                                <input type="checkbox" class="collapse_allcheckbox"> 
                                                </label>
                                             </div>
                                          </th>
                                          <th>ISP NAME</th>
                                          <th>LEGAL NAME</th>
                                          <th>PORTAL URL</th>
                                          <th>EMAIL</th>
                                          <th>MOBILE</th>
                                          <th>STATE</th>
                                          <th>ACTIVE FROM</th>
                                          <th>PAID TILL</th>
                                          <th>BALANCE</th>
                                          <th>TICKETS</th>
                                       </tr>
                                    </thead>
                                    <tbody id="search_gridview">
                                       <?php
                                       if(count($isplisting) > 0){
                                          foreach($isplisting as $lobj){
                                             $statename = $this->isp_model->getisp_statename($lobj->isp_uid);
                                             $is_activated = $lobj->is_activated;
                                             if($lobj->activated_on != '0000-00-00 00:00:00'){
                                                $activated_on = date('d-m-Y', strtotime($lobj->activated_on));
                                             }else{
                                                $activated_on = '00-00-0000';
                                             }
                                             echo "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' value='".$lobj->isp_uid."'></label></div></td><td><a href='".base_url()."manageISP/edit_isp/".$lobj->id."'>".$lobj->isp_name." (".$lobj->isp_uid.")</a></td><td>".$lobj->legal_name."</td><td>".$lobj->portal_url."</td><td>".$lobj->email."</td><td>".$lobj->phone."</td><td>".$statename."</td><td>".$activated_on."</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td>";
                                          }
                                       }else{
                                          echo "<tr><td style='text-align:center' colspan='13'> No Result Found !!</td></tr>";
                                       }
                                       ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /Start your project here-->
      <!-- SCRIPTS -->
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript">
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            var herader_height = $("#header").height();
            height = height - herader_height;
            //alert(height);
            $('#right-container-fluid').css('height', height);
         });
      </script>
      <script type="text/javascript">
         $('body').on('click', '#activeisplist', function(){
            window.location.href = base_url+"manageISP/status/act";
         });
         $('body').on('click', '#inactiveisplist', function(){
            window.location.href = base_url+"manageISP/status/inact";
         });
         $('body').on('click', '#demoisplist', function(){
            window.location.href = base_url+"manageISP/status/demo";
         });
         $('body').on('change', '.collapse_allcheckbox', function(){
            var status = this.checked;
            $('.collapse_checkbox').each(function(){ 
               this.checked = status;
            });
            if (status) {
               $('#delete_records').removeClass('hide');
            }else{
               $('#delete_records').addClass('hide');
            }
         });
         
          $('body').on('change', '.collapse_checkbox', function(){
            if(this.checked == false){ 
               $(".collapse_allcheckbox")[0].checked = false; 
            }
            if ($('.collapse_checkbox:checked').length == $('.collapse_checkbox').length ){
               $(".collapse_allcheckbox")[0].checked = true; 
            }
            
            if ($('.collapse_checkbox:checked').length > 0) {
               $('#delete_records').removeClass('hide');
            }else{
               $('#delete_records').addClass('hide');
            }
         });
         
         function delete_inactive_isp() {
            var matches = [];
            $(".collapse_checkbox:checked").each(function() {
               var ispid = this.value;
               matches.push(ispid);
            });

            var c = confirm('Are you sure to delete these ISPs');
            if (c) {
               $.ajax({
                  url: base_url+'manageISP/delete_inactive_isp',
                  type: 'POST',
                  data: 'ispids='+matches,
                  success: function(data){
                     window.location.href = base_url+"manageISP/status/inact";
                  }
               });
            }
         }
      </script>
   </body>
</html>

