<?php
$isplist = ''; $alerts=''; $records=''; $billing=''; $softwareupdate=''; $settings = '';
$controller = $this->router->fetch_class();
$method=$this->router->fetch_method();

if($controller == 'manageISP'){
    if($method == 'index'){
        $isplist = 'class="active"';
    }elseif($method == 'softwareupdate'){
        $softwareupdate = 'class="active"';
    }
}elseif($controller == 'records'){
    $records = 'class="active"';
}elseif($controller == 'settings'){
    $settings = 'class="active"';
}
?>
<div id="sidedrawer" class="mui--no-user-select">
    <div id="sidedrawer-brand" class="mui--appbar-line-height">
       <span class="mui--text-title">
       <img src="<?php echo base_url() ?>assets/images/isp_dashlogo.png" class="img-responsive" width="80%"/>
       </span>
    </div>
    <div class="menu-container">
       <div class="crbnMenu">
          <ul class="menu">
             <li>
                <a href="<?php echo base_url().'manageISP' ?>" <?php echo $isplist; ?>><i class="fa fa-list" aria-hidden="true"></i>
                ISP List </a>
             </li>
             <li>
                <a href="#"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Alerts</a>
             </li>
             <li>
                <a href="<?php echo base_url().'settings' ?>" <?php echo $settings; ?>><i class="fa fa-wrench" aria-hidden="true"></i> Global Settings</a>
             </li>
             <li>
                <a href="<?php echo base_url().'records' ?>"  <?php echo $records; ?>><i class="fa fa-cubes" aria-hidden="true"></i> Records</a>
             </li>
             <li>
                <a href="<?php echo base_url().'manageISP/softwareupdate' ?>"  <?php echo $softwareupdate; ?>><i class="fa fa-futbol-o" aria-hidden="true"></i> Software Update</a>
             </li>
             <li class="hide">
                <a href="<?php echo base_url().'exceltoDB' ?>"><i class="fa fa-futbol-o" aria-hidden="true"></i>
Excel-DB</a>
             </li>
             <li><a href="<?php echo base_url().'logout' ?>" style="background-color:#cccccc; color:#36465f; box-shadow: none; font-weight:600; text-transform:inherit; padding: 10px 22px"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
          </ul>
       </div>
    </div>
 </div>