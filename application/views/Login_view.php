

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="container-fluid">
         <div class="login_container" id="main_div">
            <div class="login_logo">
               <img src="<?php echo base_url() ?>assets/images/isp_logo.png" class="img-responsive"/>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="input_container">
		  <?php echo validation_errors('<div style="color:#f00; font-size:15px; text-align:center">', '</div>'); ?>
		  
		  <form method="post" action="<?php echo base_url(); ?>login/validate_user" autocomplete="off" class="mui-form">
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="text" class="form-control" id="username" name="username" required="required">
                        <label>Username or Email</label>
                     </div>
                     <div class="mui-textfield mui-textfield--float-label">
                        <input type="password" class="form-control" id="password" name="password" required="required">
                        <label>Password</label>
                     </div>
                     <div class="mui-row">
                        <div class="col-sm-2 col-xs-2">&nbsp;</div>
                        <div class="col-sm-8 col-xs-8">
			   <input type="submit" class="mui-btn mui-btn--accent btn-lg btn-block" name="submitlogin" value="LOGIN" />
                        </div>
                        <div class="col-sm-2 col-xs-2">&nbsp;</div>
                     </div>
                     <!--<div class="mui-row">
                        <div class="mui--text-center">
                           <h5>Want to create an account? <a href="#">Contact Us</a></h5>
                        </div>
                     </div>-->
                  </form>
               </div>
            </div>
         </div>
      </div>

      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script>
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
         });
      </script>
   </body>
</html>

