

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>SHOUUT | ISP</title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- Start your project here-->
      <div class="loading">
         <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="wapper">
               <?php $this->view('left_nav'); ?>
               <header id="header">
                  <nav class="navbar navbar-default">
                     <div class="container-fluid">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="#">GLOBAL SETTINGS</a>
                        </div>
                     </div>
                  </nav>
               </header>
               <div id="content-wrapper">
                  <div class="mui--appbar-height"></div>
                  <div class="mui-container-fluid" id="right-container-fluid">
                     <div class="mui--appbar-height"></div>
                     <div class="add_user" style="padding-top:0px;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <ul class="mui-tabs__bar plan_mui-tabs__bar">
                                       <li class="mui--is-active">
                                          <a data-mui-toggle="tab" data-mui-controls="countryinfo" onclick="infotab_click()">
                                             Country Details
                                          </a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="mui--appbar-height"></div>
                                    <div class="mui-tabs__pane mui--is-active" id="countryinfo">
                                       <div class="row">
                                          <div class="col-sm-3 col-xs-3 pull-right">
                                             <button class="mui-btn mui-btn--accent pull-right" style="height:30px; line-height: 30px; margin-bottom:15px; font-size:12px;" id="_addcountryModal">Add Country<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="row">
                                                <div class="table-responsive">
                                                   <table class="table table-striped">
                                                      <thead>
                                                         <tr class="active">
                                                            <th>&nbsp;</th>
                                                            <th>Country </th>
                                                            <th>ShortName</th>
                                                            <th>Currency</th>
                                                            <th>Demo Cost</th>
                                                            <th>Cost / User</th>
                                                            <th>Cost / Location</th>
                                                            <th>&nbsp;</th>
                                                         </tr>
                                                      </thead>
                                                      <tbody id="countrylisting">
                                                         <tr><td colspan="6">No Records Found</td></tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" tabindex="-1" role="dialog" id="countryRequestModal" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog" style="margin-top:4%">
            <form action="" method="post" id="countrydata_form" autocomplete="off" onsubmit="add_country_data(); return false;">
               <input type="hidden" name="countryid" id="countryid" value="" />
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title">
                        <strong>Country Details</strong>
                     </h4>
                  </div>
                  <div class="modal-body">
                     <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="country_name"  required>
                              <label>Country Name <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="mui-textfield">
                              <input type="text" name="country_shortname"  required>
                              <label>Country ShortName <sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <div class="">
                              <select name="country_currency" class="form-control country_currency" required>
                                 <option value="">Select Currency <sup>*</sup></option>
                                 <?php echo $this->settings_model->getcurrencylists(); ?>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <input type="text" name="country_democost"  required>
                              <label>Demo Account Cost (in <span class="curr_symbol"></span>)<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <input type="text" name="country_phonecode"  required>
                              <label>Phone Code<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <input type="text" name="country_cost_user"  required>
                              <label>Cost Per User (in <span class="curr_symbol"></span>)<sup>*</sup></label>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield">
                              <input type="text" name="country_cost_location"  required>
                              <label>Cost Per Location (in <span class="curr_symbol"></span>)<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="mui-btn mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                     <input type="submit" class="mui-btn mui-btn--primary mui-btn--flat" style="background-color:#f00f64; color:#fff" value="SAVE" >
                  </div>
               </div>
            </form>
         </div>
      </div>
      
      
      
      <!-- JQuery -->
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
      <script type="text/javascript"> var base_url = "<?php echo base_url() ?>"; </script>
      <script type="text/javascript" src="<?php echo base_url() ?>assets/js/misc.js"></script>
      <link href="<?php echo base_url() ?>assets/css/bootstrap-toggle.min.css" rel="stylesheet">
      <script src="<?php echo base_url() ?>assets/js/bootstrap-toggle.min.js"></script>
      <script type="text/javascript">
         $(document).ready(function() {
            var height = $(window).height();
            $('#main_div').css('height', height);
            $('#right-container-fluid').css('height', height);
            window.scrollTo(0,0);
            //setTimeout(function (){ $('.loading').addClass('hide') },1000);
            
            $.ajax({
               url: base_url+'settings/country_listing',
               type: 'POST',
               dataType: 'json',
               success: function(response){
                  $('#countrylisting').html(response.countrylisting);
                  $('.loading').addClass('hide');
               }
            });
            
         });
         
         $('body').on('click', '#_addcountryModal', function(){
            $('select[name="country_currency"]').val('');
            $('#countryid').val('');
            $('#countrydata_form')[0].reset();
            $('#countryRequestModal').modal('show');
         });
         $('body').on('change', '.country_currency', function(){
            var currencyval = $(this).find("option:selected").text();
            $('.curr_symbol').html(currencyval);
         });
         
         function add_country_data() {
            $('.loading').removeClass('hide');
            $('#countryRequestModal').modal('hide');
            var formdata = $('#countrydata_form').serialize();
            $.ajax({
               url: base_url+'settings/configure_country',
               type: 'POST',
               data: formdata,
               dataType: 'json',
               async: false,
               success: function(response){
                  $('#countrydata_form')[0].reset();
                  window.location.href = base_url+'settings';
               }
            });
         }
         
         function edit_countrydetails(countryid) {
            $('#countrydata_form')[0].reset();
            $('.loading').removeClass('hide');
            $('#countryid').val(countryid);
            $.ajax({
               url: base_url+'settings/countrydetails',
               type: 'POST',
               data: 'countryid='+countryid,
               dataType: 'json',
               success: function(response){
                  $('input[name="country_name"]').val(response.country_name);
                  $('input[name="country_shortname"]').val(response.country_shortname);
                  $('input[name="country_democost"]').val(response.country_democost);
                  $('input[name="country_phonecode"]').val(response.country_phonecode);
                  $('input[name="country_cost_user"]').val(response.country_cost_user);
                  $('input[name="country_cost_location"]').val(response.country_cost_location);
                  $('select[name="country_currency"] option[value="'+response.country_currencyid+'"]').attr('selected', 'selected');
                  
                  $('#countryRequestModal').modal('show');
                  
                  $('.loading').addClass('hide');
               }
            });
         }
      </script>
   </body>
</html>

