<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>SHOUUT | ISP</title>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/mui.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mui.min.js"></script>
        <script type="text/javascript">var base_url = "<?php echo base_url(); ?>";</script>
        <script type="text/javascript">
            $(document).ready(function() {
                var height = $(window).height();
                $('#right-container-fluid').css('height', height);
                window.scrollTo(0,0);
                setTimeout(function (){
                   $('.loading').addClass('hide');
                },1000);
             });
            function showmodal() {
                $('#softwareupdate').modal('show');
            }
            function updateallISP() {
                $('#softwareupdate').modal('hide');
                $('.loading').removeClass('hide');
                $.ajax({
                    url: base_url+'manageISP/softwareupdate',
                    type: 'POST',
                    data: 'confirmupdate=1',
                    success: function(data){
                        //alert(data);
                        window.location.href = base_url+'manageISP/softwareupdate'; 
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="loading">
            <img src="<?php echo base_url() ?>assets/images/loader.svg"/>
        </div>
        <div class="container-fluid">
            <div class="row">
               <div class="wapper">
                  <?php $this->view('left_nav'); ?>
                    <header id="header">
                        <nav class="navbar navbar-default">
                           <div class="container-fluid">
                              <div class="navbar-header">
                                 <a class="navbar-brand" href="#">APPLICATION UPDATE</a>
                              </div>
                              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                 <ul class="nav navbar-nav navbar-right">
                                    <li>
                                       <a href="javascript:void(0)" onclick="showmodal()">
                                       <button class="mui-btn mui-btn--small mui-btn--accent">
                                       UPDATE APPLICATION
                                       </button>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </nav>
                    </header>
                    <div id="content-wrapper">
                        <div class="mui--appbar-height"></div>
                        <div class="mui-container-fluid" id="right-container-fluid">
                            <div class="mui--appbar-height"></div>
                            <h2>UPDATES LOGS</h2>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left:20px">
                                <div class="row">
                                    <?php echo $this->isp_model->application_update_summary(); ?>
                                </div>
                            </div>
                        </div>
               </div>
            </div>
        </div>
        <div class="modal fade" id="softwareupdate" role="dialog"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>CONFIRMATION ALERT</strong>
                       </h4>
                    </div>
                    <div class="modal-body" style="padding-bottom:5px">
                       <p>Are you sure you want to update once for all ?</p>
                    </div>
                    <div class="modal-footer" style="text-align: right">
                       <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" style="background-color:#4D4D4D" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="mui-btn  mui-btn--small mui-btn--accent" onclick="updateallISP()">YES</button>
                    </div>
                  </div>
            </div>
        </div>
    </body>
</html>