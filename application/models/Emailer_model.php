<?php
class Emailer_model extends CI_Model{
    public function __construct(){
        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);    */
    }
    
    public function getispdemo_account($isp_uid, $countryid){
        $demobalance = 0; $currsymbol = '&#8377;';
        $walletQ = $this->db->query("SELECT wallet_amount FROM sht_isp_wallet WHERE isp_uid='".$isp_uid."' ORDER BY id DESC");
        if($walletQ->num_rows() > 0){
            $wrowdata = $walletQ->row();
            $demobalance = $wrowdata->wallet_amount;
        }
        
        $currQ = $this->db->query("SELECT tb2.currency_symbol FROM sht_countries as tb1 INNER JOIN sht_currency as tb2 ON(tb1.currency_id=tb2.currency_id) WHERE tb1.id='".$countryid."'");
        if($currQ->num_rows() > 0){
            $crowdata = $currQ->row();
            $currsymbol = $crowdata->currency_symbol;
        }
        
        return $currsymbol.' '.$demobalance;
    }
    
    public function activate_user_emailer($ispadmin_uid){
        $query = $this->db->query("SELECT * FROM sht_isp_admin WHERE isp_uid='".$ispadmin_uid."'");
        $rowdata = $query->row();
        $ispname = $rowdata->isp_name;
        $password = $rowdata->orig_pwd;
        $email = $rowdata->email;
        $country_id = $rowdata->country_id;
        $decibel_account = $rowdata->decibel_account;
        if($decibel_account == 'paid'){
            $portal_url = 'https://'.$rowdata->portal_url;
        }else{
            $portal_url = 'https://www.shouut.com/ispadmins/'.$rowdata->portal_url;
        }
        
        $demobalance = $this->getispdemo_account($ispadmin_uid, $country_id);
        
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" /><meta name="apple-mobile-web-app-capable" content="yes" /></head><body style="margin: 0; padding: 0;">
        <table style="border:0px; border-spacing:0; margin:0px; padding:0px; width: 100% !important;height: 100%;background: #ffffff;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none; ">
             <tr>
                <td style="display: block !important;clear: both !important;margin: 0 auto !important;max-width: 580px !important;">
                   <table style="padding:0px; margin:0px;border:0px; border-spacing:0;">
                      <tr>
                         <td valign="top">
                            <img src="'.base_url().'assets/images/decibeladvt.jpg"/>
                         </td>
                      </tr>
                      <tr>
                         <td height="5px"></td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:10px 0px">Hi '.$ispname.',</p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:600; font-family:Open Sans; margin:10px 0px">Welcome to ShouutOS!. Your account is now LIVE.</p>
                         </td>
                      </tr>
                      <tr>
                         <td height="5px"></td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px 10px 0px; text-align: justify">We have activated the demo plan and credited your account with '.$demobalance.' balance to get you started right away.</p>
                         </td>
                      </tr>
                      <tr>
                         <td height="5px"></td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px 10px 0px; text-align: justify"> You can start using ShouutOS and configure Users/NAS/Plans/Top ups etc. Once we receive your subscription payments or you make the subscription payment from your account panel, you will be able to activate more users.</p>
                         </td>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               Please note that the following modules have been Enabled in your service :
                            </p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#f00f64; font-size:13px;font-size:13px; font-weight:600; font-family:Open Sans; margin:5px 0px">Decibel Home WiFi Modules : </p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px"><strong>1. ISP Admin Panel :</strong> Backend for ISP to manage plans, topups, users, billings tickets etc.</p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               <strong>2. ISP Dynamic Website :</strong> Entire ISP dynamic website with dynamic plans and top ups listing, lead generation and 2 custom tabs for ISP content.
                            </p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               <strong>3. Subscriber Access & Management</strong>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:20px">
                               <strong>i.  Portal :</strong> Subscriber Management portal for ISP customers to login and manage their accounts, make payments, track usage status etc.
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:20px">
                               <strong>ii. ISP App :</strong> ISP Android App for subscribers to manage their account, track usage with PG integration (Available 1st July).
                            </p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               <strong>4. Public Hotspot Admin Panel :</strong>Backend management platform for public WiFi hotspots.
                            </p>
                         </td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               <strong>Admin Login :</strong>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <a href="'.$portal_url.'.shouut.com" target="_blank" style="color: #25a9e0;text-decoration:none;">'.$portal_url.'.shouut.com/</a>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <strong>Username :</strong> <a href="mailto:abdullanka@gmail.com" style="color: #25a9e0;text-decoration:none;">'.$email.'</a>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <strong>Password :</strong> '.$password.'
                            </p>
                         </td>
                      </tr>
                      <tr>
                         <td height="5px"></td>
                      </tr>
                      <tr>
                         <td>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px">
                               <strong>Consumer Portal + ISP Website :</strong>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <a href="'.$portal_url.'.shouut.com/consumer/" target="_blank" style="color: #25a9e0;text-decoration:none;">'.$portal_url.'.shouut.com/consumer/</a>
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <strong>Consumer username:</strong>  Email/User ID/Phone Number
                            </p>
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:5px 0px;padding-left:10px">
                               <strong>Password :</strong> Sent over email for every consumer
                            </p>
                         </td>
                      </tr>
                   </table>
                </td>
             </tr>
             <tr>
                <td style="display: block !important;
                   clear: both !important;
                   margin: 0 auto !important;
                   max-width: 580px !important;border-top:1px solid #e6e7e8;">
                   <!-- Message start -->
                   <table>
                      <tr>
                         <td align="left">
                            <p style="color:#404041; font-size:13px;font-size:13px; font-weight:400; font-family:Open Sans; margin:10px 0px">For partnerships and reseller programs, email us at <a href="mailto:someone@example.com" style="color: #25a9e0;text-decoration:none;">talktous@shouut.com</a></p>
                         </td>
                         <td align="right">
                            <p style="margin:10px 0px"><img src="'.base_url().'assets/images/isp_dashlogo.png" width="50%"/></p>
                         </td>
                      </tr>
                   </table>
                </td>
             </tr>
          </table>
        </body>
        </html>
        ';
        
        //echo $message;
        
        $from = ''; $fromname = '';
        $this->load->library('email');
        $from = 'talktous@shouut.com';
        $fromname = 'DECIBEL';
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => FALSE,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        );
        
        //Initialise CI mail
        $this->email->initialize($config);
        $this->email->from($from, $fromname);
        $this->email->to($email);
        $this->email->cc('praveer@shouut.com');
        $this->email->subject('Decibel: ACTIVATION EMAIL');
        $this->email->message($message);	
	$this->email->send();
    }
}
?>