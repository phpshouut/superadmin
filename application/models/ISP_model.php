<?php
class ISP_model extends CI_Model{
public $dynamicDB;


/*------------------ PAYTM OTP APIS BEGINS ----------------------------------------------------------------*/	
    private  $baseUrl = "http://sendotp.msg91.com/api";
    public function OTP_MSG($phone) {
        $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
        $data_string = json_encode($data);
        $ch = curl_init($this->baseUrl.'/generateOTP');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        //echo '<pre>'; print_r($result);
        $response = json_decode($result,true);
        if($response["status"] == "error"){
           // return $response["response"]["code"];
            return 0;
        }else{
            return $response["response"]["oneTimePassword"];
        }
    }
    public function verifyBySendOtp($phone,$otp){
        $data = array("countryCode" => "91", "mobileNumber" => "$phone", "oneTimePassword" => "$otp");
        $data_string = json_encode($data);
        $ch = curl_init($this->baseUrl . '/verifyOTP');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($data_string),
          'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if ($response["status"] == "error") {
         //customize this as per your framework
         //$resp['message'] =  $response["response"]["code"];
          return 0;
        }else {
          //$resp['message'] =  "NUMBER VERIFIED SUCCESSFULLY";
          return 1;
        }
    }

    public function checkuserstaus(){
        $data = array();
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        $query = $this->db->query("SELECT isp_name, email,phone,portal_url,infotab_details,moduletab_details,is_activated FROM sht_isp_admin WHERE is_deleted='0' AND is_activated='0' AND isp_uid='".$ispadmin_uid."' ");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            $ispname = $rowdata->isp_name;
            $mobile = $rowdata->phone;
            $infotab_details = $rowdata->infotab_details;
            $moduletab_details = $rowdata->moduletab_details;
            if(($infotab_details == '0') || ($moduletab_details == '0') ){
                $data['tab_incomplete'] = 0;
                $data['infotab_details'] = $infotab_details;
                $data['moduletab_details'] = $moduletab_details;
            }else{
                $data['tab_incomplete'] = 1;
                $data['infotab_details'] = $infotab_details;
                $data['moduletab_details'] = $moduletab_details;
                $data['mobile'] = $mobile;
                $data['ispname'] = $ispname;
                //$data['useract_otp'] = $this->OTP_MSG($mobile);
		$data['useract_otp'] = 0;
                
            }
        }
        echo json_encode($data);
    }
    
    public function activate_ispuser_othercountry($ispadmin_uid, $radchk_pwd, $password){
	$password_isp = md5($radchk_pwd);
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$row = $query->row_array();
		$servername  = $row['db_host'];
		$username = $row['db_user'];
		$password = $row['db_password'];
		$dbname = $row['db_name'];
		$this->dynamicDB = array(
		    'dsn'	=> '',
		    'hostname' => $servername,
		    'username' => $username,
		    'password' => $password,
		    'database' => $dbname,
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($this->dynamicDB, TRUE);
		$dynamicDB->update("sht_isp_admin", array('is_activated' => '1', 'orig_pwd' => $radchk_pwd, 'password' => $password_isp, 'activated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $ispadmin_uid));
	    }
	}
    }
    
    public function activate_ispuser(){
        $data = array();
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        $radchk_pwd = random_string('alnum', 7);
	$password = md5($radchk_pwd);
        $this->db->update("sht_isp_admin", array('is_activated' => '1', 'orig_pwd' => $radchk_pwd, 'password' => $password, 'activated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $ispadmin_uid));
        
        $this->emailer_model->activate_user_emailer($ispadmin_uid);
        
	$this->activate_ispuser_othercountry($ispadmin_uid, $radchk_pwd, $password);
        $this->copy_servertoserver($ispadmin_uid);
        
        $query = $this->db->query("SELECT id FROM sht_isp_admin WHERE isp_uid='".$ispadmin_uid."'");
        $ispid = $query->row()->id;
        $data['ispid'] = $ispid;
        echo json_encode($data);
    }
    
    
/*******************************************************************************************/
    public function copy_servertoserver($ispadmin_uid){
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $ftpquery = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    
	    if($ftpquery->num_rows() > 0){
		// call server api to copy the folder
		$frow = $ftpquery->row_array();
		$api_url = $frow['api_path'];
		if($api_url != ''){
		    $requestData = array(
			'isp_uid' => $ispadmin_uid,
		    );
		    $service_url = $api_url;
		    $curl = curl_init($service_url);
		    $requestData = $requestData;
		    $data_request = json_encode($requestData);
		    $curl_post_data = array("requestData" => $data_request);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_POST, true);
		    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		    $curl_response = curl_exec($curl);
		    curl_close($curl);
		}
		
		
		// Connect FTP SERVER
		/*$this->load->library('sftp');
		$config['hostname'] = $frow['ftp_host'];
		$config['username'] = $frow['ftp_user'];
		$config['password'] = $frow['ftp_password'];
		//$config['debug']  = TRUE;
		$this->sftp->connect($config);
		
		$modules = $this->db->query("SELECT tb2.module, tb1.portal_url, tb1.decibel_account FROM sht_isp_admin as tb1 INNER JOIN sht_isp_admin_modules as tb2 ON(tb1.isp_uid=tb2.isp_uid) WHERE tb1.isp_uid='".$ispadmin_uid."' AND tb2.status='1'");
		//echo $this->db->last_query(); die;
		if($modules->num_rows() > 0){
		    ini_set('max_execution_time', 0);
		    foreach($modules->result() as $mobj){
			$moduleid = $mobj->module;
			$portal_url = strtolower($mobj->portal_url);
			$decibel_account = $mobj->decibel_account;
			$ispfolder = $portal_url.'.shouut.com';
			
			$dir_path = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder";
			$this->sftp->mkdir($dir_path);
			
			
			$type = 'isp_portal';
			$dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/";
			$src = $_SERVER['DOCUMENT_ROOT']."isp_portal/";
			$this->sftp->custom_mirror($src, $dst);
			    */
			/*if(($moduleid == '3') || ($moduleid == '4')){
			    $type = 'isp_consumer';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/consumer";
			    $this->sftp->mkdir($dirname);
			    
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/consumer/";
			    $src = $_SERVER['DOCUMENT_ROOT']."isp_consumer/";
			    $this->sftp->custom_mirror($src, $dst);
			}
			elseif($moduleid == '7'){
			    $type = 'isp_publicwifi';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/publicwifi";
			    $this->sftp->mkdir($dirname);    
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/publicwifi/";
			    $src = $_SERVER['DOCUMENT_ROOT']."isp_publicwifi/";
			    $this->sftp->custom_mirror($src, $dst);		

			    //hotelportal
			    $type = 'hotelportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/hotelportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/hotelportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."hotelportal/";
			    $this->sftp->custom_mirror($src, $dst);

			    //hospitalportal
			    $type = 'hospitalportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/hospitalportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/hospitalportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."hospitalportal/";
			    $this->sftp->custom_mirror($src, $dst);

			    //instituteportal
			    $type = 'instituteportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/instituteportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/instituteportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."instituteportal/";
			    $this->sftp->custom_mirror($src, $dst);

			    //cafeportal
			    $type = 'cafeportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/cafeportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/cafeportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."cafeportal/";
			    $this->sftp->custom_mirror($src, $dst);

			    //retailportal
			    $type = 'retailportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/retailportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/retailportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."retailportal/";
			    $this->sftp->custom_mirror($src, $dst);

			    //enterpriseportal
			    $type = 'enterpriseportal';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/enterpriseportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/enterpriseportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."enterpriseportal/";
			    $this->sftp->custom_mirror($src, $dst);

			}
			elseif($moduleid == '8'){
			    $type = 'isp_channel';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/channel";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/channel/";
			    $src = $_SERVER['DOCUMENT_ROOT']."channel_publicwifi/";
			    $this->sftp->custom_mirror($src, $dst);
			}
			elseif($moduleid == '9'){
			    $type = 'isp_reseller';
			    $dirname = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/locationportal";
			    $this->sftp->mkdir($dirname);
			    $dst = $_SERVER['DOCUMENT_ROOT']."ispadmins/$ispfolder/locationportal/";
			    $src = $_SERVER['DOCUMENT_ROOT']."locationportal/";
			    $this->sftp->custom_mirror($src, $dst);
			}*/
		  //  }
		//}
		//$this->sftp->close();
	    }
	    else{
		$this->createuserpanelfolder($ispadmin_uid);
	    }
	}
    }


/******************************************************************************************
 *******************************************************************************************/
    
    public function createuserpanelfolder($isp_uid){
        //https://github.com/chriskacerguis/codeigniter-sftp/blob/master/README.md
        $modules = $this->db->query("SELECT tb2.module, tb1.isp_domain_name, tb1.portal_url, tb1.decibel_account, tb1.isp_domain_protocol, tb1.domain_exists FROM sht_isp_admin as tb1 INNER JOIN sht_isp_admin_modules as tb2 ON(tb1.isp_uid=tb2.isp_uid) WHERE tb1.isp_uid='".$isp_uid."' AND tb2.status='1'");
        if($modules->num_rows() > 0){
            ini_set('max_execution_time', 0);
            foreach($modules->result() as $mobj){
                $moduleid = $mobj->module;
                $portal_name = strtolower($mobj->portal_url);
		$portal_url = strtolower($mobj->isp_domain_name);
		$decibel_account = $mobj->decibel_account;
		$protocol = $mobj->isp_domain_protocol;
		$domain_exists = $mobj->domain_exists;
		$ispurl = '';
		if(($decibel_account == 'paid') && ($domain_exists == '1')){
		    $ispurl = $protocol.$portal_url.'/';
		}else{
		    $ispurl = ISPURL.'ispadmins/'.$portal_url.'/';
		}
		
		//$ispurl = 'http://'.$portal_url.'/';
                //$ispurl = 'http://localhost/ispadmins/'.$portal_url.'/';
                //$ispurl = ISPURL.'ispadmins/'.$portal_url.'/';
                $type= '';
                if($moduleid == '1'){
                    $type = 'isp_portal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.'';
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.'';
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_portal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
                    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
                }elseif(($moduleid == '3') || ($moduleid == '4')){
                    $type = 'isp_consumer';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/consumer";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/consumer";
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_consumer";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
                    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
                }elseif($moduleid == '7'){
                    $type = 'isp_publicwifi';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/publicwifi";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/publicwifi";
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_publicwifi";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
                    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					
		    //hotelportal
		   /* $type = 'hotelportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/hotelportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/hotelportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/hotelportal";
		    
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
		    */
		    //voucher_redeem folder
		    $type = 'voucherRedeem';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/voucherRedeem";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/voucherRedeem";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    		 
		    //hospitalportal
		 /*   $type = 'hospitalportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/hospitalportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/hospitalportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/hospitalportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					 
		    //instituteportal 
		    $type = 'instituteportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/instituteportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/instituteportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/instituteportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					 
		    //cafeportal
		    $type = 'cafeportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/cafeportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/cafeportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/cafeportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					 
		    //retailportal
		    $type = 'retailportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/retailportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/retailportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/retailportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					 
		    //enterpriseportal 
		    $type = 'enterpriseportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/enterpriseportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/enterpriseportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/enterpriseportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
		    */
		    //groupportal 
		    $type = 'groupportal';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/groupportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/groupportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/groupportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
		    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
					 
                }elseif($moduleid == '8'){
                    $type = 'isp_channel';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/channel";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/channel";
                    $src = $_SERVER['DOCUMENT_ROOT']."/channel_publicwifi";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
                    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
                }elseif($moduleid == '9'){
                    $type = 'isp_reseller';
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/locationportal";
                    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url."/locationportal";
                    $src = $_SERVER['DOCUMENT_ROOT']."/locationportal";
		    
		    $srcperm = exec("sudo chmod -R 0755 \"$src\"");
		    $destperm = exec("sudo chmod -R 0777 \"$dst\"");
		    
                    $this->recurse_copy($src,$dst);
                    $this->edit_configuration($portal_name,$portal_url,$ispurl,$editorpath,$isp_uid,$type);
                }
                
            }
            
            return 1;
        }
    }
    
    public function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) {
                    if(($file == 'source') || ($file == 'user_guide')){
                        continue;
                    }elseif($file == 'ispmedia'){
			if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
		    }elseif($file == 'documents'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }elseif($file == 'marketing_promotion'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }elseif($file == 'slider_promotion'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }else{
                        $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                    }
                } 
                else {
		    if(file_exists($dst . '/' . $file) && ($file == 'database.php')){
			continue;
		    }else{
			copy($src . '/' . $file,$dst . '/' . $file);
			chmod($dst . '/' . $file, 0777); 			
		    }
                } 
            } 
        } 
        closedir($dir);
        return 1;
    }
    
    public function edit_configuration($portal_name,$portal_url,$ispurl='',$editorpath='',$isp_uid='',$type=''){
        //https://stackoverflow.com/questions/17190810/update-code-igniter-config-file-during-installation
        /*$basepath = '';
        $pattern = '$config[\'base_url\']';
        $path_to_file = $editorpath."/application/config/config.php";
        $cconfig = $pattern . " = '".base_url()."isp_consumer';";
        $basepath = $pattern . " = 'http://". $ispurl . "/';";
        $file_contents = file_get_contents($path_to_file);
        $file_contents = str_replace($cconfig,$basepath,$file_contents);
        file_put_contents($path_to_file,$file_contents);*/
        
	if($type == 'isp_channel'){
            $chreading = fopen($editorpath."/js/angular/service/loginService.js", 'r');
            $chwriting = fopen($editorpath."/js/angular/service/loginService.tmp", 'w');
            $chreplaced = false;
            while (!feof($chreading)) {
                $line = fgets($chreading);
                if(preg_match("/\b(var isp_uid)\b/", $line)){
                    $line = 'var isp_uid = "'.$isp_uid.'"; ';
                    $chreplaced = true;
                }
                fputs($chwriting, $line);
            }
            fclose($chreading); fclose($chwriting);
            if ($chreplaced) {
                rename($editorpath."/js/angular/service/loginService.tmp", $editorpath."/js/angular/service/loginService.js");
            } else {
                unlink($editorpath."/js/angular/service/loginService.tmp");
            }
        }else{
	    $configreading = fopen($editorpath."/application/config/config.php", 'r');
	    $configwriting = fopen($editorpath."/application/config/config.tmp", 'w');
	    $replaced = false;
	    $pattern = '$config[\'base_url\']';
	    if($type == 'isp_portal'){
		$basepath = $pattern . " = '". $ispurl."';";
	    }elseif($type == 'isp_consumer'){
		$basepath = $pattern . " = '". $ispurl."consumer/';";
	    }elseif($type == 'isp_publicwifi'){
		$basepath = $pattern . " = '". $ispurl."publicwifi/';";
	    }elseif($type == 'isp_reseller'){
		$basepath = $pattern . " = '". $ispurl."locationportal/';";
	    }
		/*elseif($type == 'hotelportal'){
		$basepath = $pattern . " = '". $ispurl."hotelportal/';";
	    }
		elseif($type == 'hospitalportal'){
		$basepath = $pattern . " = '". $ispurl."hospitalportal/';";
	    }
		elseif($type == 'instituteportal'){
		$basepath = $pattern . " = '". $ispurl."instituteportal/';";
	    }
		elseif($type == 'enterpriseportal'){
		$basepath = $pattern . " = '". $ispurl."enterpriseportal/';";
	    }*/
	    elseif($type == 'groupportal'){
		$basepath = $pattern . " = '". $ispurl."groupportal/';";
	    }
	  /*  elseif($type == 'cafeportal'){
		$basepath = $pattern . " = '". $ispurl."cafeportal/';";
	    }elseif($type == 'retailportal'){
		$basepath = $pattern . " = '". $ispurl."retailportal/';";
	    }*/
	    if(($type != 'isp_portal') && ($type != 'isp_publicwifi')){
		$cisess = '$config[\'sess_cookie_name\'] = \''.$portal_name.'_'.$type.'_ci_session\';';
	    }else{
		$cisess = '$config[\'sess_cookie_name\'] = \''.$portal_name.'_ci_session\';';
	    }
	    while (!feof($configreading)) {
		$line = fgets($configreading);
		if (stristr($line,'base_url')) {
		  $line = $basepath;
		  $replaced = true;
		}else if (stristr($line,'sess_cookie_name')) {
		  $line = $cisess;
		  $replaced = true;
		}
		fputs($configwriting, $line);
	    }
	    fclose($configreading); fclose($configwriting);
	    if ($replaced) {
		//unlink($editorpath."/application/config/config.php");
		rename($editorpath."/application/config/config.tmp", $editorpath."/application/config/config.php");
	    } else {
		unlink($editorpath."/application/config/config.tmp");
	    }
	    
	    /***************************************************************************************/
	    $reading = fopen($editorpath."/application/config/constants.php", 'r');
	    $writing = fopen($editorpath."/application/config/constants.tmp", 'w');
	    $replaced = false;
	    while (!feof($reading)) {
		$line = fgets($reading);
		if($type == 'isp_consumer'){
		    if(preg_match("/\b(IMAGEPATH)\b/", $line)){
			$line = 'define("IMAGEPATH", "'. $ispurl.'assets/media/");';
		    }
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		    }
		}elseif($type == 'isp_publicwifi'){
		    if(preg_match("/\b(IMAGEPATH)\b/", $line)){
			$line = 'define("IMAGEPATH", "'. $ispurl.'assets/media/");';
		    }
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		    }
		    if(preg_match("/\b(APIPATH)\b/", $line)){
			$line = 'define("APIPATH", "'.ISPURL.'isp_consumer_api/");';
		    }
		    if(preg_match("/\b(ROUTERFILEPATH)\b/", $line)){
			$line = 'define("ROUTERFILEPATH", "'.$_SERVER['DOCUMENT_ROOT'].'/ispadmins/'.$portal_url.'/publicwifi/");';
		    }
		    if(preg_match("/\b(HOMEWIFIPORTAL)\b/", $line)){
			$line = 'define("HOMEWIFIPORTAL", "'. $ispurl.'");';
		    }
		    if(preg_match("/\b(HOMEWIFICHANNEL)\b/", $line)){
			$line = 'define("HOMEWIFICHANNEL", "'. ISPURL.'ispadmins/'.$portal_url.'/channel/index.html");';
		    }
		}elseif($type == 'isp_reseller'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		    }
		}
		
		/*elseif($type == 'hotelportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}
		elseif($type == 'hospitalportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}elseif($type == 'instituteportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}elseif($type == 'enterpriseportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}*/
		elseif($type == 'groupportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}
		/*elseif($type == 'cafeportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}
		elseif($type == 'retailportal'){
		    if(preg_match("/\b(IMAGEPATHISP)\b/", $line)){
			$line = 'define("IMAGEPATHISP", "'. $ispurl.'ispmedia/");';
		}
		}*/
		if (stristr($line,'ISPID')) {
		  $line = 'define("ISPID", "'.$isp_uid.'");';
		  $replaced = true;
		}
		if(feof($reading) && $replaced !== true){
		  $line = 'define("ISPID", "'.$isp_uid.'");';
		  $replaced = true;  
		}
		fputs($writing, $line);
	    }
	    fclose($reading); fclose($writing);
	    if ($replaced) {
		rename($editorpath."/application/config/constants.tmp", $editorpath."/application/config/constants.php");
	    } else {
		unlink($editorpath."/application/config/constants.tmp");
	    }
	}
	/*****************************************************************************************/

        return 1;
    }
 
/********************************************************************************************/    
    public function getisp_countryid($isp_uid){
	$query = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	if($query->num_rows() > 0){
	    $rowdata = $query->row();
	    return $rowdata->country_id;
	}else{
	    return '';
	}
    }

    public function getisp_statename($isp_uid){
        $sdata = array(); $name = '';
        $stateQ = $this->db->query("SELECT tb2.id, tb2.state FROM sht_isp_admin_region as tb1 INNER JOIN sht_states as tb2 ON(tb1.state=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.status='1'");
        if($stateQ->num_rows() > 0){
            foreach($stateQ->result() as $sobj){
                $sdata[] = ucwords($sobj->state);
            }
	    
	    $isp_countryid = $this->getisp_countryid($isp_uid);
	    $count_country_states = $this->count_country_states($isp_countryid);
	    if(count($sdata) == $count_country_states){
		$name = "- ALL STATES -";
	    }else{
		$name = implode(', ', $sdata);
	    }
        }
        return $name;
    }
    public function getstatename($stateid){
        $name = '';
        $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
                $data = $stateQ->row();
                $name = ucwords($data->state);
        }
        return $name;
    }
    
    public function count_country_states($countryid){
	$stateQ = $this->db->get_where('sht_states', array('country_id' => $countryid));
        $num_rows = $stateQ->num_rows();
	return $num_rows;
    }
    
    public function country_list($countryid=''){
	$countryQ = $this->db->get('sht_countries');
        $num_rows = $countryQ->num_rows();
        if($num_rows > 0){
            $gen = '';
            foreach($countryQ->result() as $stobj){
                    $sel = '';
                    if($countryid == $stobj->id){
                            $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="'.$stobj->id.'" '.$sel.'>'.$stobj->name.'</option>';
            }
        }
        echo $gen;
    }
    
    public function state_list($stateid='', $countryid=''){
	$post_countryid = $this->input->post('countryid');
	if(isset($post_countryid) && $post_countryid != ''){
	    $countryid = $post_countryid;
	}
        $stateQ = $this->db->get_where('sht_states', array('country_id' => $countryid));
        $num_rows = $stateQ->num_rows();
        if($num_rows > 0){
            $gen = '';
            foreach($stateQ->result() as $stobj){
                    $sel = '';
                    if($stateid == $stobj->id){
                            $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="'.$stobj->id.'" '.$sel.'>'.$stobj->state.'</option>';
            }
        }
        echo $gen;
    }
    
    public function getcitylist($stateid, $cityid=''){
        $gen = '';
        $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid));
        $num_rows = $cityQ->num_rows();
        if($num_rows > 0){
            foreach($cityQ->result() as $ctobj){
                $sel = '';
                if($cityid == $ctobj->city_id){
                        $sel = 'selected="selected"';
                }
                $gen .= '<option value="'.$ctobj->city_id.'" '.$sel.'>'.$ctobj->city_name.'</option>';
            }
        }
        echo $gen;
    }
    
    public function is_ispactivated($isp_uid){
        $query = $this->db->query("SELECT is_activated FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
        $isactivated = $query->row()->is_activated;
        return $isactivated;
    }
    public function ispuser_details(){
        $datArr = '';
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        $query = $this->db->query("SELECT * FROM sht_isp_admin WHERE isp_uid='".$ispadmin_uid."'");
        if($query->num_rows() > 0){
            $datArr = $query->row_array();
        }
        
        $statequery = $this->db->query("SELECT state FROM sht_isp_admin_region WHERE isp_uid='".$ispadmin_uid."' AND status='1'");
        if($statequery->num_rows() > 0){
            foreach($statequery->result() as $sobj){
                $datArr['isp_states'][] = $sobj->state;
            }
        }else{
            $datArr['isp_states'] = array();
        }
        
	$detquery = $this->db->query("SELECT gst_no,GSTIN_Registration FROM sht_isp_detail WHERE isp_uid='".$ispadmin_uid."'");
        if($detquery->num_rows() > 0){
	    $rowdata = $detquery->row();
	    $datArr['gst_no'] = $rowdata->gst_no;
	}else{
	    $datArr['gst_no'] = '';
	}
	
        echo json_encode($datArr); die;
    }
    
    public function getlast_ispuser(){
        $query = $this->db->query("SELECT isp_uid FROM sht_isp_admin ORDER BY id DESC LIMIT 1");
        if($query->num_rows() > 0){
            return $query->row()->isp_uid;
        }else{
            return $query->num_rows();
        }
    }
    public function addtodb_ispuser_othercountry($ispArr,$ispdetailArr,$ispadmin_uid,$portal_url,$uid,$countryid,$stateArr){
	$data = array();
	$query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $servername  = $row['db_host'];
	    $username = $row['db_user'];
	    $password = $row['db_password'];
	    $dbname = $row['db_name'];
	    $this->dynamicDB = array(
		'dsn'	=> '',
		'hostname' => $servername,
		'username' => $username,
		'password' => $password,
		'database' => $dbname,
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	    );
	    $dynamicDB = $this->load->database($this->dynamicDB, TRUE);
	    if(isset($ispadmin_uid) && $ispadmin_uid != ''){
		$query = $dynamicDB->query("SELECT id FROM sht_isp_admin WHERE portal_url='".$portal_url."' AND isp_uid != '".$ispadmin_uid."'");
		if($query->num_rows() > 0){
		    $data['portal_exists'] = 1;
		}else{        
		    $dynamicDB->update('sht_isp_admin', $ispArr, array('isp_uid' => $ispadmin_uid));
		}
	    }
	    else{   
		$query = $dynamicDB->query("SELECT id FROM sht_isp_admin WHERE portal_url='".$portal_url."'");
		if($query->num_rows() > 0){
		}
		else{        
		    $dynamicDB->insert('sht_isp_admin',$ispArr);
		    $lastid = $dynamicDB->insert_id();
		    $dynamicDB->update('sht_isp_admin', array('isp_uid' => $uid), array('id' => $lastid));
		    $ispadmin_uid = $uid;
                }
	    }
	    $dquery = $dynamicDB->query("SELECT id FROM sht_isp_detail WHERE isp_uid='".$ispadmin_uid."'");
	    if($dquery->num_rows() > 0){
		$dynamicDB->update('sht_isp_detail', $ispdetailArr, array('isp_uid' => $ispadmin_uid));
	    }else{
		$ispdetailArr['isp_uid'] = $ispadmin_uid;
		$dynamicDB->insert('sht_isp_detail',$ispdetailArr);
	    }
	    $allstateoptn = $stateArr[0];
	    if($allstateoptn != 'all'){
		$dynamicDB->delete('sht_isp_admin_region', array('isp_uid' => $ispadmin_uid));
		foreach($stateArr as $stateid){
		    $regionQ = $dynamicDB->query("SELECT state FROM sht_isp_admin_region WHERE isp_uid='".$ispadmin_uid."' AND status='1' AND state='".$stateid."'");
		    if($regionQ->num_rows() == 0){
			$dynamicDB->insert("sht_isp_admin_region", array('isp_uid' => $ispadmin_uid, 'state' => $stateid, 'added_on' => date('Y-m-d H:i:s')));
		    }
		}
		//$selstate = implode(',',$stateArr);
		//$this->db->query("UPDATE sht_isp_admin_region SET status='0', updated_on='".date('Y-m-d H:i:s')."' WHERE isp_uid='".$ispadmin_uid."' AND state NOT IN($selstate)");
	    }else{
		$dynamicDB->delete('sht_isp_admin_region', array('isp_uid' => $ispadmin_uid));
		$stateQ = $dynamicDB->get_where('sht_states', array('country_id' => $countryid));
		$stnum_rows = $stateQ->num_rows();
		if($stnum_rows > 0){
		    foreach($stateQ->result() as $stobj){
			$stateidd = $stobj->id;
			$dynamicDB->insert("sht_isp_admin_region", array('isp_uid' => $ispadmin_uid, 'state' => $stateidd, 'added_on' => date('Y-m-d H:i:s')));
		    }
		}
	    }
	}//
    }
    public function addtodb_ispuser(){
        $data = array();
	$isp_domain_name = strtolower($this->input->post('isp_portal_url'));
        $ispArr = array(
          'isp_name' => $this->input->post('isp_name'),
          'legal_name' => $this->input->post('isp_legal_name'),
          'email' => $this->input->post('isp_email'),
          'portal_url' => substr($isp_domain_name , 0 , strpos($isp_domain_name ,'.' ,0)),
	  'isp_domain_name' => $isp_domain_name,
          'added_on' => date('Y-m-d H:i:s'),
          'infotab_details' => '1',
          'phone' => $this->input->post('isp_mobile'),
	  'country_id' => $this->input->post('isp_country')
        );
	
	$ispdetailArr = array(
	  'isp_name' => $this->input->post('isp_name'),
          'company_name' => $this->input->post('isp_legal_name'),
          'support_email' => $this->input->post('isp_email'),
          'website' => $this->input->post('isp_portal_url') . '.shouut.com',
          'status' => '1',
          'help_number1' => $this->input->post('isp_mobile'),
	  'gst_no' => $this->input->post('isp_gstin_number'),
	);
	
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        $portal_url = $this->input->post('isp_portal_url');
        $last_ispuser = $this->getlast_ispuser();
        
        if(isset($ispadmin_uid) && $ispadmin_uid != ''){
            $query = $this->db->query("SELECT id FROM sht_isp_admin WHERE portal_url='".$portal_url."' AND isp_uid != '".$ispadmin_uid."'");
            if($query->num_rows() > 0){
                $data['portal_exists'] = 1;
            }else{        
                $this->db->update('sht_isp_admin', $ispArr, array('isp_uid' => $ispadmin_uid));
                $data['portal_exists'] = 0;
                $data['ispuid'] = $ispadmin_uid;
            }
            
        }
	else{   
            $query = $this->db->query("SELECT id FROM sht_isp_admin WHERE portal_url='".$portal_url."'");
            if($query->num_rows() > 0){
                $data['portal_exists'] = 1;
            }else{        
                $this->db->insert('sht_isp_admin',$ispArr);
                $lastid = $this->db->insert_id();
                //$uid = sprintf('%03d',$lastid);
                if(($last_ispuser == '0') || ($last_ispuser == '')){
                    $uid = str_pad($lastid, 3, "0", STR_PAD_RIGHT);
                }else{
                    $uid = $last_ispuser + 1;
                }
                $this->db->update('sht_isp_admin', array('isp_uid' => $uid), array('id' => $lastid));
                
                $ispadmin_uid = $uid;
                
                $data['lastispuid'] = $last_ispuser;
                $data['portal_exists'] = 0;
                $data['ispuid'] = $uid;
            }
        }
	
	$dquery = $this->db->query("SELECT id FROM sht_isp_detail WHERE isp_uid='".$ispadmin_uid."'");
	if($dquery->num_rows() > 0){
	    $this->db->update('sht_isp_detail', $ispdetailArr, array('isp_uid' => $ispadmin_uid));
	}else{
	    $ispdetailArr['isp_uid'] = $ispadmin_uid;
	    $this->db->insert('sht_isp_detail',$ispdetailArr);
	}
	
        
	$countryid = $this->input->post('isp_country');
	$stateArr = $this->input->post('isp_state');
	$allstateoptn = $stateArr[0];
	if($allstateoptn != 'all'){
	    $this->db->delete('sht_isp_admin_region', array('isp_uid' => $ispadmin_uid));
	    foreach($stateArr as $stateid){
		$regionQ = $this->db->query("SELECT state FROM sht_isp_admin_region WHERE isp_uid='".$ispadmin_uid."' AND status='1' AND state='".$stateid."'");
		if($regionQ->num_rows() == 0){
		    $this->db->insert("sht_isp_admin_region", array('isp_uid' => $ispadmin_uid, 'state' => $stateid, 'added_on' => date('Y-m-d H:i:s')));
		}
	    }
	    //$selstate = implode(',',$stateArr);
	    //$this->db->query("UPDATE sht_isp_admin_region SET status='0', updated_on='".date('Y-m-d H:i:s')."' WHERE isp_uid='".$ispadmin_uid."' AND state NOT IN($selstate)");
	}else{
	    $this->db->delete('sht_isp_admin_region', array('isp_uid' => $ispadmin_uid));
	    $stateQ = $this->db->get_where('sht_states', array('country_id' => $countryid));
	    $stnum_rows = $stateQ->num_rows();
	    if($stnum_rows > 0){
		foreach($stateQ->result() as $stobj){
		    $stateidd = $stobj->id;
		    $this->db->insert("sht_isp_admin_region", array('isp_uid' => $ispadmin_uid, 'state' => $stateidd, 'added_on' => date('Y-m-d H:i:s')));
		}
	    }
	}
	
        $this->addtodb_ispuser_othercountry($ispArr,$ispdetailArr,$ispadmin_uid,$portal_url,$uid,$countryid,$stateArr);
        echo json_encode($data);
    }
    
    public function ispmodules_details(){
        $datArr = array();
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        
        /*
	$moduleListQ = $this->db->get("sht_modules");
        if($moduleListQ->num_rows() > 0){
            foreach($moduleListQ->result() as $mobj){
                $module_id = $mobj->id;
                $module_name = $mobj->module_name;
                $module_keyname = $mobj->key_name;
                $module_price = $mobj->price;
                $datArr["$module_keyname"] = array('id' => $module_id, 'name' => $module_name, 'price' => $module_price);
                $modulesecQ = $this->db->get_where("sht_modules_sections", array('module_id' => $module_id));
                if($modulesecQ->num_rows() > 0){
                    foreach($modulesecQ->result() as $msecobj){
                        $datArr["$module_keyname"]['sections'][] = $msecobj->section;
                    }
                }
            }
        }
        */
        
        if($ispadmin_uid != ''){
            $query = $this->db->query("SELECT module FROM sht_isp_admin_modules WHERE isp_uid='".$ispadmin_uid."' AND status='1'");
            if($query->num_rows() > 0){
                foreach($query->result() as $mobj){
                    $datArr['selected_module'][] = $mobj->module;
                }
            }
        }
        $datArry["modules"] = $datArr;
        echo json_encode($datArry); die;
    }
    public function addtodb_ispmodules(){
        $isp_modulesArr = $this->input->post('isp_modules');
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        $this->db->delete('sht_isp_admin_modules', array('isp_uid' => $ispadmin_uid));
        $moduleArr = explode(',',$isp_modulesArr);
        foreach($moduleArr as $moduleid){
            $this->db->insert("sht_isp_admin_modules", array('isp_uid' => $ispadmin_uid, 'module' => $moduleid, 'added_on' => date('Y-m-d H:i:s')));
            $this->db->update('sht_isp_admin', array('moduletab_details' => '1'), array('isp_uid' => $ispadmin_uid));
        }
        echo json_encode(true);
        
    }
    
    public function portalurl_exists(){
	$ispid = $this->input->post('ispadmin_uid');
	$query = $this->db->query("SELECT portal_url FROM sht_isp_admin WHERE isp_uid='".$ispid."' AND is_deleted='0' AND is_franchise='0' AND portal_url != ''");
	$isexists = $query->num_rows();	
	echo $isexists;
    }

/*******************************************************************************************/
    public function loadispuser_onedit($ispid){
        $datArr = array();
        $query = $this->db->query("SELECT * FROM sht_isp_admin WHERE id='".$ispid."'");
        if($query->num_rows() > 0){
            $datArr = $query->row_array();
        }
        $ispadmin_uid = $datArr['isp_uid'];
        $statequery = $this->db->query("SELECT state FROM sht_isp_admin_region WHERE isp_uid='".$ispadmin_uid."' AND status='1'");
        if($statequery->num_rows() > 0){
            foreach($statequery->result() as $sobj){
                $datArr['isp_states'][] = $sobj->state;
            }
        }else{
            $datArr['isp_states'] = array();
        }
        
	$detquery = $this->db->query("SELECT gst_no,GSTIN_Registration FROM sht_isp_detail WHERE isp_uid='".$ispadmin_uid."'");
        if($detquery->num_rows() > 0){
	    $rowdata = $detquery->row();
	    $datArr['gst_no'] = $rowdata->gst_no;
	}else{
	    $datArr['gst_no'] = '';
	}
	
        return $datArr;
    }
    public function edittodb_ispmodules_othercountry($isp_modulesArr,$moduleArr,$ispadmin_uid){
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$row = $query->row_array();
		$servername  = $row['db_host'];
		$username = $row['db_user'];
		$password = $row['db_password'];
		$dbname = $row['db_name'];
		$this->dynamicDB = array(
		    'dsn'	=> '',
		    'hostname' => $servername,
		    'username' => $username,
		    'password' => $password,
		    'database' => $dbname,
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($this->dynamicDB, TRUE);
		foreach($moduleArr as $moduleid){
		    $prevmodQ = $dynamicDB->get_where("sht_isp_admin_modules", array('isp_uid' => $ispadmin_uid, 'module' => $moduleid, 'status' => '1'));
		    if($prevmodQ->num_rows() == 0){
			$dynamicDB->insert("sht_isp_admin_modules", array('isp_uid' => $ispadmin_uid, 'module' => $moduleid, 'added_on' => date('Y-m-d H:i:s')));
		    }
		}
		$dynamicDB->query("DELETE FROM sht_isp_admin_modules WHERE isp_uid='".$ispadmin_uid."' AND module NOT IN($isp_modulesArr)");
		$dynamicDB->update('sht_isp_admin', array('moduletab_details' => '1'), array('isp_uid' => $ispadmin_uid));
        
	    }
	}
    }
    public function edittodb_ispmodules(){
        $isp_modulesArr = $this->input->post('isp_modules');
        $moduleArr = explode(',',$isp_modulesArr);
        $ispadmin_uid = $this->input->post('ispadmin_uid');
        foreach($moduleArr as $moduleid){
            $prevmodQ = $this->db->get_where("sht_isp_admin_modules", array('isp_uid' => $ispadmin_uid, 'module' => $moduleid, 'status' => '1'));
            if($prevmodQ->num_rows() == 0){
                $this->db->insert("sht_isp_admin_modules", array('isp_uid' => $ispadmin_uid, 'module' => $moduleid, 'added_on' => date('Y-m-d H:i:s')));
            }
        }
        $this->db->query("DELETE FROM sht_isp_admin_modules WHERE isp_uid='".$ispadmin_uid."' AND module NOT IN($isp_modulesArr)");
        $this->db->update('sht_isp_admin', array('moduletab_details' => '1'), array('isp_uid' => $ispadmin_uid));
        
	$this->edittodb_ispmodules_othercountry($isp_modulesArr,$moduleArr,$ispadmin_uid);
	$this->copy_servertoserver($ispadmin_uid);
	
	echo json_encode(true);
        
    }
/******************************************************************************************/
    public function isp_dashboard(){
        $data = array();
        $inactive = "SELECT count(*) as active FROM sht_isp_admin WHERE is_activated='0' AND is_deleted='0'";
	$demoacct = "SELECT count(*) as demoacct FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0' AND is_franchise='0' AND decibel_account='demo'";
        $query = $this->db->query("SELECT count(*) as active, ($demoacct) as demoacct, ($inactive) as inactive FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0' AND is_franchise='0' AND decibel_account='paid'");
        if($query->num_rows() > 0){
            foreach($query->result() as $lobj){
                $data['active'] = $lobj->active;
                $data['inactive'] = $lobj->inactive;
                $data['demoacct'] = $lobj->demoacct;
            }
        }else{
            $data['active'] = 0;
            $data['inactive'] = 0;
            $data['demoacct'] = 0;
        }
        return $data;
    }
    public function ispusers_listing($status){
        $datArr = array();
        if($status == 'act'){ $isactivated = '1'; $decibel_account = 'paid'; }
        elseif($status == 'inact'){ $isactivated = '0'; $decibel_account = 'demo'; }
	elseif($status == 'demo'){ $isactivated = '1'; $decibel_account = 'demo'; }
        $query = $this->db->query("SELECT * FROM sht_isp_admin WHERE is_activated='".$isactivated."' AND decibel_account='".$decibel_account."' AND is_deleted='0' AND is_franchise='0'");
        if($query->num_rows() > 0){
            $datArr = $query->result();
        }
        return $datArr;
    }
/*********************************************************************************************/
    public function softwareupdate(){
        $query = $this->db->query("SELECT isp_uid,country_id FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0'");
        if($query->num_rows() > 0){
            foreach($query->result() as $iobj){
                $isp_uid = $iobj->isp_uid;
		$countryid = $iobj->country_id;
		$ftpquery = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
		if($ftpquery->num_rows() > 0){
		    
		    $frow = $ftpquery->row_array();
		    $api_url = $frow['api_path'];
		   // echo $isp_uid.'----'.$api_url."<br />";
		    if($api_url != ''){
			$requestData = array(
			    'isp_uid' => $isp_uid,
			);
			$service_url = $api_url;
			$curl = curl_init($service_url);
			$requestData = $requestData;
			$data_request = json_encode($requestData);
			$curl_post_data = array("requestData" => $data_request);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			$curl_response = curl_exec($curl);
			curl_close($curl);
		    }
		}else{
		    $this->createuserpanelfolder($isp_uid);
		}
                //$this->copy_servertoserver($isp_uid);
		
            }
            
            $this->db->insert('sht_application_updates_logs', array('updated_on' => date('Y-m-d H:i:s')));
            return 1;
            //redirect(base_url().'manageISP');
        }
    }
    
    public function application_update_summary(){
        $gen = '';
        $query = $this->db->query("SELECT * FROM sht_application_updates_logs ORDER BY id DESC");
        if($query->num_rows() > 0){
            $gen .= '<ul>';
            foreach($query->result() as $logs){
                $gen .= '<li>'.date('d-m-Y H:i:s', strtotime($logs->updated_on)).'</li>';
            }
            $gen .= '</ul>';
        }else{
            $gen .= 'No Updates to Application Yet!!';
        }
        
        echo $gen;
    }


    public function countrydetails($isp_uid){
	$data = array();
	$ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	if($ispcountryQ->num_rows() > 0){
	    $crowdata = $ispcountryQ->row();
	    $countryid = $crowdata->country_id;
    
	    $countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
	    if($countryQ->num_rows() > 0){
		$rowdata = $countryQ->row();
		$currid = $rowdata->currency_id;
		
		$currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
		if($currencyQ->num_rows() > 0){
		    $currobj = $currencyQ->row();
		    $currsymbol = $currobj->currency_symbol;
		    $data['currency'] = $currsymbol;
		}
		$data['demo_cost'] = $rowdata->demo_cost;
		$data['cost_per_user'] = $rowdata->cost_per_user;
		$data['cost_per_location'] = $rowdata->cost_per_location;
	    }
	}
	return $data;
    }

    public function ispbilling_details_check_country(){
	$is_other_country = 0;
	$ispadmin_uid = $this->input->post('ispadmin_uid');
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$is_other_country = 1;
	    }
	}
	return $is_other_country;
    }
    public function ispbilling_details_other_country(){
	$data = array();
	$ispadmin_uid = $this->input->post('ispadmin_uid');
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$row = $query->row_array();
		$servername  = $row['db_host'];
		$username = $row['db_user'];
		$password = $row['db_password'];
		$dbname = $row['db_name'];
		$this->dynamicDB = array(
		    'dsn'	=> '',
		    'hostname' => $servername,
		    'username' => $username,
		    'password' => $password,
		    'database' => $dbname,
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($this->dynamicDB, TRUE);
		$ispadmin_uid = $this->input->post('ispadmin_uid');
		$ispcodet = $this->countrydetails($ispadmin_uid);
		$data['currency'] = $ispcodet['currency'];
		
		$wallet_amt = 0; $decibel_account = ''; $is_paid = 0; $passbook_amt = 0; $home_license_used = 0; $public_license_used = 0;
		$homewifi_budget = $ispcodet['cost_per_user']; $publicwifi_budget = $ispcodet['cost_per_location'];
		
		$walletinfoQ = $dynamicDB->query("SELECT decibel_account,is_paid,homewifi_budget,publicwifi_budget FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."' AND is_paid='1' ORDER BY id DESC LIMIT 1");
		if($walletinfoQ->num_rows() > 0){
		    $rowdata = $walletinfoQ->row();
		    $decibel_account = $rowdata->decibel_account;
		    $is_paid = $rowdata->is_paid;
		    $homewifi_budget = $rowdata->homewifi_budget;
		    $publicwifi_budget = $rowdata->publicwifi_budget;
		}
		
		$walletQ = $dynamicDB->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."'");
		if($walletQ->num_rows() > 0){
		    $rowdata = $walletQ->row();
		    $wallet_amt = $rowdata->wallet_amt;
		}
		$data['wallet_amt'] = $ispcodet['currency'].' '.$wallet_amt;
		
		
		$passbookQ = $dynamicDB->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost, COALESCE(SUM(total_active_users),0) as home_license_used, COALESCE(SUM(total_active_ap),0) as public_license_used FROM sht_isp_passbook WHERE isp_uid='".$ispadmin_uid."'");
		if($passbookQ->num_rows() > 0){
		    $passrowdata = $passbookQ->row();
		    $passbook_amt = $passrowdata->activeusers_cost;
		    $home_license_used = $passrowdata->home_license_used;
		    $public_license_used = $passrowdata->public_license_used;
		}
		$balanceamt = $wallet_amt - $passbook_amt;
		$data['balance_amt'] = $ispcodet['currency'].' '.number_format($balanceamt, 2);
		$data['total_license_used'] = $passbook_amt;
		$data['home_license_used'] = $home_license_used;
		$data['public_license_used'] = $public_license_used;
		$data['decibel_account'] = $decibel_account;
		
		//Usuage Reports
		$gen = '';
		$usuageQ = $dynamicDB->query("SELECT YEAR(added_on) AS y, MONTH(added_on) AS m, SUM(`total_active_users`) as homelicense_used, SUM(`total_active_ap`) as publiclicense_used FROM sht_isp_passbook WHERE `isp_uid`='".$ispadmin_uid."'  GROUP BY y, m ORDER BY m DESC");
		if($usuageQ->num_rows() > 0){
		    $i = 1;
		    foreach($usuageQ->result() as $uobj){
			$monthNum = $uobj->m;
			$year = $uobj->y;
			$total_homelic = $uobj->homelicense_used;
			$total_publiclic = $uobj->publiclicense_used;
			$total_amount = ($total_homelic * $homewifi_budget) + ($total_publiclic * $publicwifi_budget);
			$monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
			$gen .= '<tr><td>'.$i.'.</td><td>'.$monthName.', '.$year.'</td><td>'.$total_homelic.'</td><td>'.$total_publiclic.'</td><td>'.$ispcodet['currency'].' '.number_format($total_amount, 2).'</td></tr>';
		    }
		}
		$data['usuage_logs'] = $gen;
	    }
	}
	echo json_encode($data);
    }
    public function ispbilling_details(){
	$data = array();
        $ispadmin_uid = $this->input->post('ispadmin_uid');
	$ispcodet = $this->countrydetails($ispadmin_uid);
	$data['currency'] = $ispcodet['currency'];
	
	$wallet_amt = 0; $decibel_account = ''; $is_paid = 0; $passbook_amt = 0; $home_license_used = 0; $public_license_used = 0;
	$homewifi_budget = $ispcodet['cost_per_user']; $publicwifi_budget = $ispcodet['cost_per_location'];
	
	$walletinfoQ = $this->db->query("SELECT decibel_account,is_paid,homewifi_budget,publicwifi_budget FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."' AND is_paid='1' ORDER BY id DESC LIMIT 1");
	if($walletinfoQ->num_rows() > 0){
	    $rowdata = $walletinfoQ->row();
	    $decibel_account = $rowdata->decibel_account;
	    $is_paid = $rowdata->is_paid;
	    $homewifi_budget = $rowdata->homewifi_budget;
	    $publicwifi_budget = $rowdata->publicwifi_budget;
	}
	
	$walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='".$ispadmin_uid."'");
	if($walletQ->num_rows() > 0){
	    $rowdata = $walletQ->row();
	    $wallet_amt = $rowdata->wallet_amt;
	}
	$data['wallet_amt'] = $ispcodet['currency'].' '.$wallet_amt;
	
	
	$passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost, COALESCE(SUM(total_active_users),0) as home_license_used, COALESCE(SUM(total_active_ap),0) as public_license_used FROM sht_isp_passbook WHERE isp_uid='".$ispadmin_uid."'");
	if($passbookQ->num_rows() > 0){
	    $passrowdata = $passbookQ->row();
	    $passbook_amt = $passrowdata->activeusers_cost;
	    $home_license_used = $passrowdata->home_license_used;
	    $public_license_used = $passrowdata->public_license_used;
	}
	$balanceamt = $wallet_amt - $passbook_amt;
	$data['balance_amt'] = $ispcodet['currency'].' '.number_format($balanceamt, 2);
	$data['total_license_used'] = $passbook_amt;
	$data['home_license_used'] = $home_license_used;
	$data['public_license_used'] = $public_license_used;
	$data['decibel_account'] = $decibel_account;
	
	//Usuage Reports
	$gen = '';
	$usuageQ = $this->db->query("SELECT YEAR(added_on) AS y, MONTH(added_on) AS m, SUM(`total_active_users`) as homelicense_used, SUM(`total_active_ap`) as publiclicense_used FROM sht_isp_passbook WHERE `isp_uid`='".$ispadmin_uid."'  GROUP BY y, m Order by y DESC,m DESC");
	if($usuageQ->num_rows() > 0){
	    $i = 1;
	    foreach($usuageQ->result() as $uobj){
		$monthNum = $uobj->m;
		$year = $uobj->y;
		$total_homelic = $uobj->homelicense_used;
		$total_publiclic = $uobj->publiclicense_used;
		$total_amount = ($total_homelic * $homewifi_budget) + ($total_publiclic * $publicwifi_budget);
		$monthName = date("M", mktime(0, 0, 0, $monthNum, 10));
		$gen .= '<tr><td>'.$i.'.</td><td>'.$monthName.', '.$year.'</td><td>'.$total_homelic.'</td><td>'.$total_publiclic.'</td><td>'.$ispcodet['currency'].' '.number_format($total_amount, 2).'</td></tr>';
		$i++;
	    }
	}
	$data['usuage_logs'] = $gen;
	
	echo json_encode($data);
	
    }
    public function addlicense_toisp_othercountry($licenseArr,$ispadmin_uid){
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$ispadmin_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$row = $query->row_array();
		$servername  = $row['db_host'];
		$username = $row['db_user'];
		$password = $row['db_password'];
		$dbname = $row['db_name'];
		$this->dynamicDB = array(
		    'dsn'	=> '',
		    'hostname' => $servername,
		    'username' => $username,
		    'password' => $password,
		    'database' => $dbname,
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($this->dynamicDB, TRUE);
		$dynamicDB->insert('sht_isp_wallet', $licenseArr);
		$dynamicDB->update('sht_isp_admin', array('decibel_account' => 'paid'), array('isp_uid' => $ispadmin_uid));
	    }
	}
    }
    public function addlicense_toisp(){
	$data = array();
	$postdata = $this->input->post();
	$ispadmin_uid = $postdata['ispadmin_uid'];
	$extracredit = $postdata['discount'];
	$wallet_amount = $postdata['slabamt'];
	if(isset($extracredit) && ($extracredit != '')){
	    $wallet_amount =  $wallet_amount + (($wallet_amount * $extracredit) / 100);
	}
	$licenseArr = array(
	    'decibel_account' => 'paid',
	    'wallet_amount' => $wallet_amount,
	    'homewifi_budget' => $postdata['hbudget'],
	    'publicwifi_budget' => $postdata['pbudget'],
	    'isp_uid' => $postdata['ispadmin_uid'],
	    'transaction_payment' => '1',
	    'transaction_response' => 'superadmin',
	    'cheque_dd_neft' => $postdata['cheque_dd_neft'],
	    'discount' => $extracredit,
	    'is_paid' => '1',
	    'added_on' => date('Y-m-d H:i:s')
	);
	$this->db->insert('sht_isp_wallet', $licenseArr);
	$this->db->update('sht_isp_admin', array('decibel_account' => 'paid'), array('isp_uid' => $ispadmin_uid));
	
	$this->addlicense_toisp_othercountry($licenseArr,$ispadmin_uid);
	//$this->createuserpanelfolder($ispadmin_uid);
	echo json_encode(true);
	
    }
    
    public function delete_inactive_isp($ispidsArr){
	if(count($ispidsArr) > 0){
	    foreach($ispidsArr as $ispid){
		$this->db->delete('sht_isp_admin', array('isp_uid' => $ispid));
		$this->db->delete('sht_isp_detail', array('isp_uid' => $ispid));
		$this->db->delete('sht_isp_admin_modules', array('isp_uid' => $ispid));
		$this->db->delete('sht_isp_admin_region', array('isp_uid' => $ispid));
		$this->db->delete('sht_isp_wallet', array('isp_uid' => $ispid));
	    }
	}
	
    }
    
    public function getcurrencylists(){
        $gen = '';
        $currencyQ = $this->db->query("SELECT currency_id, currency_title, currency_symbol FROM sht_currency WHERE status='1'");
        if($currencyQ->num_rows() > 0){
            foreach($currencyQ->result() as $currobj){
                $gen .= '<option value="'.$currobj->currency_id.'">'.$currobj->currency_title.' ('.$currobj->currency_symbol.') </option>';
            }
        }
        return $gen;
    }
    
        public function add_publicpricing_othercountry($isp_uid, $public_currency,$public_license_fee,$public_billing_frequency){
	$query = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$isp_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $countryid = $row['country_id'];
	    $query = $this->db->query("select * from sht_country_credential where country_id = '$countryid'");
	    if($query->num_rows() > 0){
		$row = $query->row_array();
		$servername  = $row['db_host'];
		$username = $row['db_user'];
		$password = $row['db_password'];
		$dbname = $row['db_name'];
		$this->dynamicDB = array(
		    'dsn'	=> '',
		    'hostname' => $servername,
		    'username' => $username,
		    'password' => $password,
		    'database' => $dbname,
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($this->dynamicDB, TRUE);
		$query = $dynamicDB->query("SELECT id FROM sht_isp_publicwifi_pricing WHERE isp_uid='".$isp_uid."'");
		if($query->num_rows() > 0){
		    $dynamicDB->update("sht_isp_publicwifi_pricing", array('currency_id' => $public_currency, 'billing_frequency' => $public_billing_frequency, 'cost_per_location' => $public_license_fee, 'added_on' => date('Y-m-d H:i:s')),  array('isp_uid' => $isp_uid));
		}else{
		    $dynamicDB->insert("sht_isp_publicwifi_pricing", array('currency_id' => $public_currency, 'billing_frequency' => $public_billing_frequency, 'cost_per_location' => $public_license_fee, 'added_on' => date('Y-m-d H:i:s'), 'isp_uid' => $isp_uid));
		}
	    }
	}
    }

    public function add_publicpricing(){
	$isp_uid = $this->input->post('isp_uid');
	$public_currency = $this->input->post('public_currency');
	$public_license_fee = $this->input->post('public_license_fee');
	$public_billing_frequency = $this->input->post('public_billing_frequency');
	
	$query = $this->db->query("SELECT id FROM sht_isp_publicwifi_pricing WHERE isp_uid='".$isp_uid."'");
	if($query->num_rows() > 0){
	    $this->db->update("sht_isp_publicwifi_pricing", array('currency_id' => $public_currency, 'billing_frequency' => $public_billing_frequency, 'cost_per_location' => $public_license_fee, 'added_on' => date('Y-m-d H:i:s')),  array('isp_uid' => $isp_uid));
	}else{
	    $this->db->insert("sht_isp_publicwifi_pricing", array('currency_id' => $public_currency, 'billing_frequency' => $public_billing_frequency, 'cost_per_location' => $public_license_fee, 'added_on' => date('Y-m-d H:i:s'), 'isp_uid' => $isp_uid));
	}
	$this->add_publicpricing_othercountry($isp_uid, $public_currency,$public_license_fee,$public_billing_frequency);
	echo json_encode(true);
    }
    
    public function publicpricing_details(){
	$data = array();
	$isp_uid = $this->input->post('isp_uid');
	
	$query = $this->db->query("SELECT * FROM sht_isp_publicwifi_pricing WHERE isp_uid='".$isp_uid."'");
	if($query->num_rows() > 0){
	    $pubrowdata = $query->row();
	    $public_currency = $pubrowdata->currency_id;
	    $public_license_fee = $pubrowdata->cost_per_location;
	    $public_billing_frequency = $pubrowdata->billing_frequency;
	
	    $data = array('currency_id' => $public_currency, 'billing_frequency' => $public_billing_frequency, 'cost_per_location' => $public_license_fee);
	}
	
	echo json_encode($data);
    }
    
    public function copystates(){
	$countryQ = $this->db->query("SELECT * FROM sht_states WHERE country_id='101'");
	if($countryQ->num_rows() > 0){
	    foreach($countryQ->result() as $cobj){
		$cityQ = $this->db->query("SELECT * FROM sht_cities WHERE city_state_id='".$cobj->id."'");
		if($cityQ->num_rows() > 0){
		    $ccdQ = $this->db->query("SELECT id FROM sht_states WHERE country_id='247' AND state='".$cobj->state."'");
		    if($ccdQ->num_rows() > 0){
			$cstateis = $ccdQ->row()->id;
			foreach($cityQ->result() as $ciobj){
			    //echo $cstateis. '=>'. $cobj->state .'=>'. $ciobj->city_name; die;
			    //$this->db->insert('sht_cities', array('city_name' => $ciobj->city_name, 'city_state_id' => $cstateis, 'isp_uid' => '0'));
			}
		    }
		}
		
	    }
	}
    }
}
?>