<?php
class Records_model extends CI_Model{
    
	public function activeusers_permonth(){
		$filterby = $this->input->post('filterby');
		$filterval = $this->input->post('filterval');
		$secfilter = $this->input->post('secfilter');
		
		$query = $this->db->query("SELECT * FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0' AND is_franchise='0'");
		$gen = '';
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$isp_uid = $qobj->isp_uid;
				if(isset($filterby) && ($filterby == 'month')){
					$startdate = $secfilter.$filterval.'01';
					$enddate = $secfilter.$filterval.'31';
				}elseif(isset($filterby) && ($filterby == 'year')){
					$startdate = $filterval.$secfilter.'01';
					$enddate = $filterval.$secfilter.'31';
				}else{
					$startdate = date('Ym').'01';
					$enddate = date('Ymd');
				}
				
				$moduleArr = array(); $modulePrice = 0;
				$moduleQ = $this->db->query("SELECT tb2.module_name, tb2.price FROM sht_isp_admin_modules as tb1 INNER JOIN sht_modules as tb2 ON(tb1.module=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.status='1'");
				if($moduleQ->num_rows() > 0){
					foreach($moduleQ->result() as $mobj){
						$moduleArr[] = $mobj->module_name;
						$modulePrice += $mobj->price;
					}
					$moduleArr = implode(', <br/>', $moduleArr);
				}
				
				$radquery = $this->db->query("SELECT count(DISTINCT(username)) as activeusers FROM radacct WHERE username LIKE '".$isp_uid."%' AND DATE(acctstarttime) BETWEEN '".$startdate."' AND '".$enddate."'");
				//echo $this->db->last_query(); die;
				if($radquery->num_rows() > 0){
					$activeusers = $radquery->row()->activeusers;
					$gen .= "<tr><td><a href='".base_url()."manageISP/edit_isp/".$qobj->id."'>".$qobj->isp_name." (".$isp_uid.")</a></td><td>".$qobj->legal_name."</td><td>".$qobj->portal_url."</td><td>".$qobj->email."</td><td>".$qobj->phone."</td><td>".$moduleArr."</td><td>".$modulePrice."</td><td style='color:#EC6E20'>".$activeusers."</td><td>&#8377; ".$modulePrice*$activeusers."</td>";
				}
			}
		}
		
		echo $gen;
	}
    
	
	public function ispbilling(){
		$query = $this->db->query("SELECT * FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0' AND is_franchise='0'");
		$gen = '';
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$isp_uid = $qobj->isp_uid;
				$startdate = date('Ym').'01';
				$enddate = date('Ymd');
				
				$moduleArr = array(); $modulePrice = 0;
				$moduleQ = $this->db->query("SELECT tb2.module_name, tb2.price FROM sht_isp_admin_modules as tb1 INNER JOIN sht_modules as tb2 ON(tb1.module=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.status='1'");
				if($moduleQ->num_rows() > 0){
					foreach($moduleQ->result() as $mobj){
						$moduleArr[] = $mobj->module_name;
						$modulePrice += $mobj->price;
					}
					$moduleArr = implode(', <br/>', $moduleArr);
				}
				
				$radquery = $this->db->query("SELECT count(DISTINCT(username)) as activeusers FROM radacct WHERE username LIKE '".$isp_uid."%' AND DATE(acctstarttime) BETWEEN '".$startdate."' AND '".$enddate."'");
				//echo $this->db->last_query(); die;
				if($radquery->num_rows() > 0){
					$activeusers = $radquery->row()->activeusers;
					$total_price = $modulePrice * $activeusers;
					$gen .= "<tr><td><a href='".base_url()."manageISP/edit_isp/".$qobj->id."'>".$qobj->isp_name." (".$isp_uid.")</a></td><td>".$qobj->legal_name."</td><td>".$qobj->portal_url."</td><td>".$qobj->email."</td><td>".$qobj->phone."</td><td>".$moduleArr."</td><td>".$modulePrice."</td><td style='color:#EC6E20'>".$activeusers."</td><td>&#8377; ".$modulePrice*$activeusers."</td>";
				}
			}
		}
	}
}


?>