<?php
class Settings_model extends CI_Model{
  
    public function getcurrencylists(){
        $gen = '';
        $currencyQ = $this->db->query("SELECT currency_id, currency_title, currency_symbol FROM sht_currency WHERE status='1'");
        if($currencyQ->num_rows() > 0){
            foreach($currencyQ->result() as $currobj){
                $gen .= '<option value="'.$currobj->currency_id.'">'.$currobj->currency_title.' ('.$currobj->currency_symbol.') </option>';
            }
        }
        return $gen;
    }
    
    public function configure_country(){
        $post = $this->input->post();
        $countryid = $post['countryid'];
        
        $countryArr = array(
            'shortname' => $post['country_shortname'],
            'name' => $post['country_name'],
            'phonecode' => $post['country_phonecode'],
            'currency_id' => $post['country_currency'],
            'cost_per_user' => $post['country_cost_user'],
            'demo_cost' => $post['country_democost'],
            'cost_per_location' => $post['country_cost_location']
        );
        
        if($countryid == ''){
            $this->db->insert('sht_countries', $countryArr);
        }else{
            $this->db->update('sht_countries', $countryArr, array('id' => $countryid));
        }
        
        echo json_encode(true);
    }
    
    public function country_listing(){
        $data = array();
        $gen = '';
        $countryQ = $this->db->order_by('name', 'ASC')->get('sht_countries');
        if($countryQ->num_rows() > 0){
            $i = 1;
            foreach($countryQ->result() as $countryobj){
                $currid = $countryobj->currency_id;
                $currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
                if($currencyQ->num_rows() > 0){
                    $currobj = $currencyQ->row();
                    $currsymbol = $currobj->currency_symbol;
                    $country_currency = $currobj->currency_title.' ('.$currobj->currency_symbol.')';
                }else{
                    $currsymbol = '';
                    $country_currency = '-';
                }
                
                $gen .= '<tr><td>'.$i.'</td><td>'.$countryobj->name.' ('.$countryobj->id.')</td><td>'.$countryobj->shortname.'</td><td>'.$country_currency.'</td><td>'.$currsymbol.$countryobj->demo_cost.'</td><td>'.$currsymbol.$countryobj->cost_per_user.'</td><td>'.$currsymbol.$countryobj->cost_per_location.'</td><td><a href="javascript:void(0)" onclick="edit_countrydetails('.$countryobj->id.')" title="Edit Country">Edit</a></td></tr>';
                $i++;
            }
        }else{
            $gen .= '<tr><td colspan="7">No Records Found</td></tr>';
        }
        
        $data['countrylisting'] = $gen;
        echo json_encode($data);
    }
    
    public function countrydetails(){
        $data = array();
        $post = $this->input->post();
        $countryid = $post['countryid'];
        $countryQ = $this->db->get_where('sht_countries', array('id' => $countryid));
        if($countryQ->num_rows() > 0){
            $rowdata = $countryQ->row();
            $data['country_name'] = $rowdata->name;
            $data['country_shortname'] = $rowdata->shortname;
            $data['country_currencyid'] = $rowdata->currency_id;
            $data['country_democost'] = $rowdata->demo_cost;
            $data['country_cost_user'] = $rowdata->cost_per_user;
            $data['country_cost_location'] = $rowdata->cost_per_location;
            $data['country_phonecode'] = $rowdata->phonecode;
        }
        
        echo json_encode($data);
    }
    
}

?>