<?php
class ExceltoDB_model extends CI_Model{
    
	   public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2', TRUE);
		//$this->mongo_conn = new MongoClient('mongodb://103.20.213.152:27017');
    }
    
	
        public function ispcodeslisting(){
		$gen = '';
		$query = $this->db->query("SELECT isp_name, isp_uid FROM sht_isp_admin WHERE is_activated='1' AND is_deleted='0'");
		if($query->num_rows() > 0){
		    foreach($query->result() as $qobj){
			$ispname = $qobj->isp_name;
			$isp_uid = $qobj->isp_uid;
			$gen .= '<option value="'.$isp_uid.'">'.$ispname.'</option>';
		    }
		}
		echo $gen;
	}
	
	function getlastuserid_inserted(){
		$query = $this->db->query("SELECT MAX(id) as last_id FROM sht_users");
		$lastid = $query->row()->last_id;
		if($lastid == NULL){
			return 0;
		}else{
			return $lastid;
		}
	}
	
	function addUsertoDB($header,$arr_data){
		$isp_uid = $this->input->post('isp_uid');
		//$isp_uid = 100;
		$headerArr = $header[1];
		$i=2;
		foreach($arr_data as $valueArr){
			$lastid = $this->getlastuserid_inserted();
			$newid = $lastid + 1;
			$lencount = 8 - (strlen($isp_uid) + strlen($newid));
			$newavailableid = $isp_uid.str_repeat('0',$lencount).$newid;
			
			$username = ''; $baseplanid = '0'; $firstname=''; $lastname=''; $middlename='';
			$mobile=''; $address=''; $email='';
			$state = ''; $city = ''; $zone= ''; $flat_number = ''; $address2 = ''; $user_plan_type='prepaid';
			foreach($valueArr as $vcolname => $vcolval){
				if(array_key_exists($vcolname, $headerArr)){
					
					if($headerArr["$vcolname"] == 'usertype'){
						$user_plan_type = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'baseplanid'){
						$planname = trim($vcolval);
						$planQ = $this->db->query("SELECT srvid FROM sht_services WHERE srvname='".$planname."' AND enableplan='1' AND isp_uid='".$isp_uid."' AND is_deleted='0'");
						if($planQ->num_rows() > 0){
							$baseplanid = $planQ->row()->srvid;
						}
					}
					if($headerArr["$vcolname"] == 'uid'){
						$newavailableid = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'username'){
						$username = trim($vcolval);
					}
					
					if($headerArr["$vcolname"] == 'firstname'){
						$firstname = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'lastname'){
						$lastname = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'mobile'){
						$mobile = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'flat_number'){
						$flat_number = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'address'){
						$address = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'address2'){
						$address2 = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'email'){
						$email = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'state'){
						$state = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'city'){
						$city = trim($vcolval);
					}
					if($headerArr["$vcolname"] == 'zone'){
						$zone = trim($vcolval);
					}
					
				}
			}
			
			$chkuserQ = $this->db->query("SELECT uid FROM sht_users WHERE (uid='".$newavailableid."' OR username='".$username."')");
			if($chkuserQ->num_rows() == 0){
				if($username == ''){ $username = $newavailableid; }
				$stateid = $this->get_state_id($state);
				$radiusUserArr = array(
					'username' => $username,
					'isp_uid' => $isp_uid,
					'uid' => $newavailableid,
					'user_plan_type' => "$user_plan_type",
					'password' => '',
					'enableuser' => '0',
					'firstname' => ucwords($firstname),
					'middlename' => '',
					'lastname' => ucwords($lastname),
					'mobile' => $mobile,
					'dob'	  => '',
					'flat_number' => $flat_number,
					'address' => $address,
					'address2' => $address2,
					'state'	  => $this->get_state_id($state),
					'city'	  => $this->getplan_city_id($city, $stateid),
					'zone'	  => $this->get_zone_id($zone),
					'usuage_locality' => '',
					'priority_account' => '0',
					'baseplanid' => $baseplanid,
					'createdon' => date('Y-m-d'),
					'createdby' => $isp_uid,
					'owner' => 'admin',
					'email' => $email,
					'expired' => '0'
				);
				
				//$this->db->insert('sht_users', $radiusUserArr);
				//$lastid = $this->db->insert_id();
				//$lastid = '121212';
				$installarr = array(
					'isp_uid' => $isp_uid,
					'subscriber_id' => $lastid,
					'subscriber_uuid' => $newavailableid,
					'connection_type' => '',
					'bill_added_on' => date('Y-m-d H:i:s'),
					'bill_number' => date('jnyHis'),
					'receipt_number' => rand(1111111111,9999999999),
					'bill_type' => 'installation',
					'payment_mode' => 'cash',
					'actual_amount' => '0',
					'discount' => '0',
					'total_amount' => '0',
					'receipt_received' => '1',
					'alert_user' => '1',
					'bill_generate_by' => $isp_uid,
					'bill_paid_on' => date('Y-m-d H:i:s')
				);
				//$this->db->insert('sht_subscriber_billing', $installarr);
				
				$securityarr = array(
					'isp_uid' => $isp_uid,
					'subscriber_id' => $lastid,
					'subscriber_uuid' => $newavailableid,
					'connection_type' => '',
					'bill_added_on' => date('Y-m-d H:i:s'),
					'receipt_number' => rand(1111111111,9999999999),
					'bill_type' => 'security',
					'payment_mode' => 'cash',
					'actual_amount' => '0',
					'discount' => '0',
					'total_amount' => '0',
					'receipt_received' => '1',
					'alert_user' => '1',
					'bill_generate_by' => $isp_uid,
					'bill_paid_on' => date('Y-m-d H:i:s')
				);
				//$this->db->insert('sht_subscriber_billing', $securityarr);
				
				//$this->db->insert('sht_subscriber_activation_panel', array('installation_details' => '1', 'security_details' => '1', 'personal_details' => '1', 'plan_details' => '1', 'subscriber_id' => $lastid));
				
				$user_creditlimit = $this->getuser_creditlimit($baseplanid);
				//$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit), array('uid' => $newavailableid));
			}
			echo $user_creditlimit;
			echo '<pre>'; print_r($radiusUserArr); echo '</pre>';
			echo '<pre>'; print_r($installarr);  echo '</pre>';
			echo '<pre>'; print_r($securityarr);  echo '</pre>';
			echo '******************************************************************';
			echo '<br/><br/>';
		}
	}
	
	public function getuser_creditlimit($srvid){
		$user_creditlimit = 0;
		$query = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.plan_duration FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$tax = 18;
			$plan_duration = $rowdata->plan_duration;
			$gross_amt = $rowdata->gross_amt;
			$gross_amt = round($gross_amt + (($gross_amt * $tax)/100));
			$planprice = ($gross_amt * $plan_duration);
			$user_creditlimit = $planprice + round($gross_amt / 2);
		}
		return $user_creditlimit;
	}
	
	public function getplan_pricing($srvid){
		$data = array();
		$query = $this->db->query("SELECT tb2.net_total, tb2.gross_amt, tb1.plan_duration FROM sht_services as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.srvid=tb2.srvid) WHERE tb2.srvid='".$srvid."'");
		if($query->num_rows() > 0){
			$rowdata = $query->row();
			$tax = 18;
			$plan_duration = $rowdata->plan_duration;
			$gross_amt = ($rowdata->gross_amt * $plan_duration);
			$planprice = round($gross_amt + (($gross_amt * $tax)/100));
			
			$plan_tilldays = ($plan_duration * 30);
			$data['plan_price'] = $planprice;
			$data['plan_tilldays'] = $plan_tilldays;
		}
		return $data;
		
	}
	
	public function add_plan_data($header, $arr_data) {
		//$isp_uid = $this->input->post('isp_uid');
		 $isp_uid=194;
		$id='';
		$headerArr = $header[1];
		$i = 2;
	
		foreach ($arr_data as $valueArr) {
		    $enableburst = $valueArr['L'];
		    $dlburstlimit = $this->convertToBytes((($valueArr['L'] == 1) ? $valueArr['M'] : 0) . "KB");
		    $ulburstlimit = $this->convertToBytes((($valueArr['L'] == 1) ? $valueArr['N'] : 0) . "KB");
		    $dlburstthreshold = $this->convertToBytes((($valueArr['L'] == 1) ? $valueArr['O'] : 0) . "KB");
		    $ulburstthreshold = $this->convertToBytes((($valueArr['L'] == 1) ? $valueArr['P'] : 0) . "KB");
		    $bursttime = $this->timeToSeconds(($valueArr['L'] == 1) ? $valueArr['Q'] : 0);
		    $priority = $this->convertToBytes(($valueArr['L'] == 1) ? $valueArr['R'] : 0);
		    $dwnrate = $this->convertToBytes($valueArr['F'] . "KB");
		    $uprate = $this->convertToBytes($valueArr['G'] . "KB");
	
		    if ($valueArr['E'] == "fup") {
			$datalimit = $this->convertToBytes($valueArr['H'] . "GB");
			$fupdwnrate = $this->convertToBytes($valueArr['J'] . "KB");
			$fupupldrate = $this->convertToBytes($valueArr['K'] . "KB");
			$tabledata = array('srvname' => $valueArr['C'], 'descr' => (isset($valueArr['D']))?$valueArr['D']:'', 'plantype' => 3, 'topuptype' => 0
			    , 'downrate' => $dwnrate, 'bursttime' => $bursttime,
			    'uprate' => $uprate, 'enableburst' => $enableburst, 'dlburstlimit' => $dlburstlimit, 'ulburstlimit' => $ulburstlimit,
			    'dlburstthreshold' => $dlburstthreshold, 'ulburstthreshold' => $ulburstthreshold,
			    'priority' => $priority, 'datacalc' => $valueArr['I'], 'datalimit' => $datalimit, 'fupuprate' => $fupupldrate
			    , 'fupdownrate' => $fupdwnrate, "enableplan" => 1,"isp_uid"=>$isp_uid);
		    } else if ($valueArr['E'] == "data") {
			$datalimit = $this->convertToBytes($valueArr['H'] . "GB");
			$tabledata = array('srvname' => $valueArr['C'], 'descr' => (isset($valueArr['D']))?$valueArr['D']:'', 'plantype' => 4, 'topuptype' => 0
			    , 'downrate' => $dwnrate, 'bursttime' => $bursttime,
			    'uprate' => $uprate, 'enableburst' => $enableburst, 'dlburstlimit' => $dlburstlimit, 'ulburstlimit' => $ulburstlimit,
			    'dlburstthreshold' => $dlburstthreshold, 'ulburstthreshold' => $ulburstthreshold,
			    'priority' => $priority, 'datacalc' => $valueArr['I'], 'datalimit' => $datalimit, 'fupuprate' => 0
			    , 'fupdownrate' => 0, "enableplan" => 1,"isp_uid"=>$isp_uid);
		    } else if ($valueArr['E'] == "unlimited") {
		      $tabledata = array('srvname' => $valueArr['C'], 'descr' => (isset($valueArr['D']))?$valueArr['D']:'', 'plantype' => 1, 'topuptype' => 0
			    , 'downrate' => $dwnrate, 'bursttime' => $bursttime,
			    'uprate' => $uprate, 'enableburst' => $enableburst, 'dlburstlimit' => $dlburstlimit, 'ulburstlimit' => $ulburstlimit,
			    'dlburstthreshold' => $dlburstthreshold, 'ulburstthreshold' => $ulburstthreshold,
			    'priority' => $priority, 'datacalc' => 0, 'datalimit' => 0, 'fupuprate' => 0
			    , 'fupdownrate' => 0, "enableplan" => 1,"isp_uid"=>$isp_uid);
		    }
		    
		
		    $query=$this->db->query("select srvid from sht_services where srvname='".$valueArr['C']."' and is_deleted=0");
		    if($query->num_rows()==0)
		    {
				
		  $this->db->insert('sht_services', $tabledata);
		    $id = $this->db->insert_id();
		    $this->add_pricing($id,$valueArr);
		     $this->add_plan_region($id,$valueArr,$isp_uid);
		      $this->activate_plan($id);
			   $this->add_nasplan_association($id,$isp_uid);
		    }
			else{
				
				$rowarr=$query->row_array();
				$id= $rowarr['srvid'];
				$tabledata=array("plan_duration"=>$valueArr['X']);
				$this->db->update('sht_services', $tabledata,array("srvid"=>$id));
		    $this->add_pricing($id,$valueArr);
		     $this->add_plan_region($id,$valueArr,$isp_uid);
		      $this->activate_plan($id);
			  $this->add_nasplan_association($id,$isp_uid);
			}
		     // die;
		     
		    
		}
		if($id!='')
		{
		    echo "updated Plan";
		}
		else
		{
		    echo "something went wrong";
		}
        
	}
	
	public function add_nasplan_association($srvid,$isp_uid)
	{
		$query=$this->db->query("select id from nas where isp_uid='".$isp_uid."'");
		
		foreach($query->result() as $val)
		{
			$query1=$this->db->query("select id from sht_plannasassoc where srvid='".$srvid."' and nasid='".$val->id."'");
			if($query1->num_rows()==0)
			{
				$tabledata=array("srvid"=>$srvid,"nasid"=>$val->id);
				$this->db->insert('sht_plannasassoc',$tabledata);
			}
			
		}
		
	}
    
	 public function add_pricing($id,$valueArr)
    {
		
		$nettotal=$valueArr['S']+(str_replace("%","",$valueArr['T'])/100)*$valueArr['S'];
	
        $tabledata=array("srvid"=>$id,"gross_amt"=>$valueArr['S'],"tax"=>str_replace("%","",$valueArr['T']),"net_total"=>$nettotal,
            "created_on"=>date("Y-m-d H:i:s"),"region_type"=>"region");
			 $query=$this->db->query("select id from sht_plan_pricing where srvid='".$id."'");
			if($query->num_rows()>0)
			{
				$rowarr=$query->row_array();
				$id=$rowarr['id'];
				echo "update sht_plan_pricing<br/>";
				echo "<pre>"; print_R($tabledata);
				 $this->db->update('sht_plan_pricing', $tabledata,array("id"=>$id));
			}else{
				echo "insert sht_plan_pricing<br/>";
				 $this->db->insert('sht_plan_pricing', $tabledata);
			}
		
       // $this->db->insert('sht_plan_pricing', $tabledata);
    }
    
        public function add_plan_region($id,$valueArr,$isp_uid)
    {
            $state=(strtoupper($valueArr['U'])!='ALL')?$this->get_state_id($valueArr['U']):'all';
            $city=(strtoupper($valueArr['V'])!='ALL')?$this->getplan_city_id($valueArr['V'],$state):'all';
            $zone=(strtoupper($valueArr['W'])!='ALL')?$this->get_zone_id($valueArr['W']):'all';
        $tabledata=array("plan_id"=>$id,"state_id"=>$state,"city_id"=>$city,"zone_id"=>$zone,
            "created_on"=>date("Y-m-d H:i:s"),"status"=>1,"isp_uid"=>$isp_uid);
			 $query=$this->db->query("select id from sht_plan_region where plan_id='".$id."'");
			if($query->num_rows()>0)
			{
				$rowarr=$query->row_array();
				$id=$rowarr['id'];
				echo "update sht_plan_region<br/>";
				echo "<pre>"; print_R($tabledata);
				 $this->db->update('sht_plan_region', $tabledata,array("id"=>$id));
			}else{
				echo "insert sht_plan_region<br/>";
				 $this->db->insert('sht_plan_region', $tabledata);
			}
		//	echo "<pre>"; print_R($tabledata); die;
       
    }
    
	public function activate_plan($id)
	{
	    $tabledata=array("plan_id"=>$id,"plan_detail"=>1,"plan_setting"=>1,"plan_pricing"=>1,"plan_region"=>1,"forced_disable"=>0);
		$query=$this->db->query("select id from sht_plan_activation_panel where plan_id='".$id."'");
		if($query->num_rows()>0)
			{
				$rowarr=$query->row_array();
				$id=$rowarr['id'];
				echo "update sht_plan_activation_panel<br/>";
				echo "<pre>"; print_R($tabledata);
				 $this->db->update('sht_plan_activation_panel', $tabledata,array("id"=>$id));
			}else{
				echo "update sht_plan_activation_panel<br/>";
				 $this->db->insert('sht_plan_activation_panel', $tabledata);
			}
	    
	}
    
	public function get_state_id($statename)
	{
	    $query=$this->db->query("SELECT id FROM `sht_states` WHERE state='".$statename."'");
	    if($query->num_rows()>0)
		{
		    $rowarr=$query->row_array();
		    return $rowarr['id'];
		}
		else
		{
		   return 0; 
		}
        
	}
	
	 public function getplan_city_id($cityname,$stateid)
    {
       // echo $cityname;
         $query=$this->db->query("SELECT city_id FROM `sht_cities` WHERE `city_name`='".$cityname."' and city_state_id='".$stateid."'");
        // echo "<pre>"; print_R($query->result());
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            return $rowarr['city_id'];
        }
        else
        {
           return 0; 
        }
        
    }
	
	
	public function get_city_id($cityname)
	{
	   // echo $cityname;
		$query=$this->db->query("SELECT city_id FROM `sht_cities` WHERE `city_name`='".$cityname."'");
	       // echo "<pre>"; print_R($query->result());
	       if($query->num_rows()>0)
	       {
		   $rowarr=$query->row_array();
		   return $rowarr['city_id'];
	       }
	       else
	       {
		  return 0; 
	       }		
        
	}
	public function get_zone_id($zonename)
	{
	      $query=$this->db->query("SELECT id FROM `sht_zones` WHERE `zone_name`='".$zonename."'");
	    if($query->num_rows()>0)
	    {
		$rowarr=$query->row_array();
		return $rowarr['id'];
	    }
	    else
	    {
	       return 0; 
	    }
	}
    
	public function convertToBytes($from) {
	    $number = substr($from, 0, -2);
	    switch (strtoupper(substr($from, -2))) {
		case "KB":
		    return $number * 1024;
		case "MB":
		    return $number * 1024 * 1024;
		case "GB":
		    return $number * 1024 * 1024 * 1024;
		case "TB":
		    return $number * 1024 * 1024 * 1024 * 1024;
		case "PB":
		    return $number * 1024 * 1024 * 1024 * 1024 * 1024;
		default:
		    return $from;
	    }
	}
    
	function timeToSeconds($time) {
	    if($time==0)
	    {
	      return 0;  
	    }
	    else
	    {
	       /* $timeExploded = explode(':', $time);
	    if (isset($timeExploded[2])) {
		return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
	    } 
	    return $timeExploded[0] * 3600 + $timeExploded[1] * 60;*/
		return $time * 3600;
	    }
	   
	    
	}
	
	
	public function update_creditlimit(){
		$query = $this->db->query("SELECT tb1.id, tb1.uid, tb1.isp_uid, tb1.baseplanid, tb2.net_total FROM sht_users as tb1 INNER JOIN sht_plan_pricing as tb2 ON(tb1.baseplanid=tb2.srvid) WHERE tb1.isp_uid='100' ORDER BY tb1.id ASC");
		if($query->num_rows() > 0){
			foreach($query->result() as $obj){
				$actual_price = $obj->net_total;
				$subscriber_uuid = $obj->uid;
				$user_creditlimit = $actual_price + ceil($actual_price / 2);
				$this->db->update('sht_users', array('user_credit_limit' => $user_creditlimit), array('uid' => $subscriber_uuid));
			}
		}
		
	}
	
	public function update_hotspot($isp_uid='',$cptype='',$routertype='')
	{
		$cond='';
		if($isp_uid!='')
		{
			$cond.="and wl.isp_uid='".$isp_uid."'";
		}
		
		$query=$this->DB2->query("select wl.location_name,wlc.cp_type,wlc.isp_uid,wlc.location_id,na.nastype as router_type from wifi_location wl 
		inner join wifi_location_cptype wlc on (wl.id=wlc.location_id)
INNER JOIN nas na on (wl.nasid=na.id) where wl.status=1 and wl.is_deleted=0 {$cond}");

$isparr=array();
$x=0;
foreach($query->result() as $val)
{
	
	if($cptype!='')
	{
		if($cptype==$val->cp_type)
		{
			if($routertype!='')
			{
				if($routertype==$val->router_type)
				{
					$isparr[$val->isp_uid][$val->router_type][$val->cp_type]=$val;
				}
			}
			else{
				$isparr[$val->isp_uid][$val->router_type][$val->cp_type]=$val;
			}
		}
	}
	else{
			if($routertype!='')
			{
				if($routertype==$val->router_type)
				{
					$isparr[$val->isp_uid][$val->router_type][$val->cp_type]=$val;
				}
			}
			else{
				$isparr[$val->isp_uid][$val->router_type][$val->cp_type]=$val;
			}
	}
	$x++;
}
//echo "<pre>"; print_R($isparr); die;

$notcptype=array("cp_custom","cp_offline","cp_visitor","cp_contestify","cp_retail_audit","cp4","cp_event_module","cp_lms");
foreach($isparr as $key=>$valisparr)
{
	
	foreach($valisparr as $valr)
	{
		foreach($valr as $vald)
		{
		//	echo $vald->cp_type."::::".$vald->location_id.":::".$vald->router_type.":::".$vald->isp_uid."<br/>";
		
		if(!in_array($vald->cp_type,$notcptype))
		{
			
			$this->createuserpanelfolder($vald->cp_type,$vald->isp_uid,$vald->location_id,$vald->router_type);
		}
			//
		
		}
	
		
		//
		
	}
}

//echo "<pre>"; print_R($isparr);die;

		
		
	}
	

	public function createuserpanelfolder($cptype,$isp_uid,$location_id,$router_type = 1){
   
         $query = $this->db->query("select portal_url,isp_domain_name,domain_exists,isp_domain_protocol  from sht_isp_admin where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $portal_url = $row['portal_url'];
            $portal_url = strtolower($portal_url);
            $check_subdomain_created = $this->getisp_accounttype($isp_uid);
	    $isp_domain_name = $row['isp_domain_name'];
	    $isp_domain_protocol = $row['isp_domain_protocol'];
	    $domain_exists = $row['domain_exists'];
	    if($domain_exists == '1'){
	        if($router_type == '5'){//cambium
			  $ispurl = $isp_domain_protocol.$isp_domain_name.'/cambium/';//for live  
		    }else if($router_type == '6'){//onehop
			 $ispurl = $isp_domain_protocol.$isp_domain_name.'/onehop/';//for live  
		    }else{
			  $ispurl = $isp_domain_protocol.$isp_domain_name.'/mikrotik_kt/';//for live
		    }
	    }
	    else{
	       if(strtolower($check_subdomain_created) == 'paid'){
		    /*if($router_type == '5'){//cambium
			  $ispurl = 'https://'.$portal_url.'.shouut.com/cambium/';//for live  
		    }else if($router_type == '6'){//onehop
			 $ispurl = 'https://'.$portal_url.'.shouut.com/onehop/';//for live  
		    }else{
			  $ispurl = 'https://'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
		    }*/
		    
		    if($router_type == '5'){//cambium
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/cambium/';//for live
		    }else if($router_type == '6'){//onehop
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/onehop/';//for live
		    }else{
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
		    }
		    
		 }else{
		    if($router_type == '5'){//cambium
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/cambium/';//for live
		    }else if($router_type == '6'){//onehop
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/onehop/';//for live
		    }else{
			  $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
		    }
		    
		 }
	    }
	    
	    
            $hotspot_folder_type = '';
            if($cptype == 'cp1'){
                $hotspot_folder_type = 'hotspot';
            }elseif($cptype == 'cp2'){
                $hotspot_folder_type = 'hotspot_v2';
            }
            elseif($cptype == 'cp3'){
		$hotspot_folder_type = 'hotspot_v3';
            }elseif($cptype == 'cp_hotel'){
                    $hotspot_folder_type = 'hotspot_htv2';
                
            }elseif($cptype == 'cp_hospital'){
                    $hotspot_folder_type = 'hotspot_hsptlv2';
                
            }elseif($cptype == 'cp_institutional'){
                    $hotspot_folder_type = 'hotspot_institutev2';
                
            }elseif($cptype == 'cp_enterprise'){
                    $hotspot_folder_type = 'hotspot_enterprisev2';
                
            }elseif($cptype == 'cp_cafe'){
                    $hotspot_folder_type = 'hotspot_cafe';
                
            }elseif($cptype == 'cp_retail'){
                    $hotspot_folder_type = 'hotspot_retail';
                
            }
			else if($cptype == 'cp_purple')
			{
				 $hotspot_folder_type = 'hotspot_purplebox';
			}else if($cptype == 'cp_club')
			{
				 $hotspot_folder_type = 'hotspot_club';
			}
			
			if($row['domain_exists']==1)
			{
				
				$isp_domain_name=$row['isp_domain_name'];
			
					 if($router_type == '1'){
				$dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt";
						if (!file_exists($dst_microtik)) {
							mkdir($dst_microtik, 0777, true);
						}
						$dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt/".$hotspot_folder_type;
				if (!file_exists($dst)) {
							mkdir($dst, 0777, true);
						}
						$editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt/".$hotspot_folder_type;
						$src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/mikrotik_kt/".$hotspot_folder_type;
			   
			   }else if($router_type == '5'){
				$dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium";
						if (!file_exists($dst_microtik)) {
							mkdir($dst_microtik, 0777, true);
						}
						$dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium/".$hotspot_folder_type;
				if (!file_exists($dst)) {
							mkdir($dst, 0777, true);
						}
						$editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium/".$hotspot_folder_type;
						$src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/cambium/".$hotspot_folder_type;
			   }else if($router_type == '6'){
				$dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop";
						if (!file_exists($dst_microtik)) {
							mkdir($dst_microtik, 0777, true);
						}
						$dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop/".$hotspot_folder_type;
				if (!file_exists($dst)) {
							mkdir($dst, 0777, true);
						}
						//echo "dest===>>".$dst."<br/>";
						$editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop/".$hotspot_folder_type;
						$src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/onehop/".$hotspot_folder_type;
			   }
			}
			else{
				
				 if($router_type == '1'){
		    $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt";
                    if (!file_exists($dst_microtik)) {
                        mkdir($dst_microtik, 0777, true);
                    }
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt/".$hotspot_folder_type;
		    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt/".$hotspot_folder_type;
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/mikrotik_kt/".$hotspot_folder_type;
		   
	       }else if($router_type == '5'){
		    $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium";
                    if (!file_exists($dst_microtik)) {
                        mkdir($dst_microtik, 0777, true);
                    }
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium/".$hotspot_folder_type;
		    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium/".$hotspot_folder_type;
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/cambium/".$hotspot_folder_type;
	       }else if($router_type == '6'){
		    $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop";
                    if (!file_exists($dst_microtik)) {
                        mkdir($dst_microtik, 0777, true);
                    }
                    $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop/".$hotspot_folder_type;
		    if (!file_exists($dst)) {
                        mkdir($dst, 0777, true);
                    }
                    $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop/".$hotspot_folder_type;
                    $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/onehop/".$hotspot_folder_type;
	       }
			}
			
	      
	          
		   // echo $portal_url."--".$ispurl."--".$editorpath."--".$isp_uid.'--'.$hotspot_folder_type.'--'.$location_id;die;
		    if($router_type != '2' && $router_type != '3' && $router_type != '4'){
			 $this->recurse_copy($src,$dst);
			 $this->edit_configuration($portal_url,$ispurl,$editorpath,$isp_uid,$hotspot_folder_type,$location_id);
			 echo $editorpath." ".$portal_url." ".$isp_uid." copied Successfully<br/>";
		    }
		    /*if($router_type == '6' && $cptype == 'cp2'){// for onehop cp2 only
			 
			 $this->recurse_copy($src,$dst);
			 $this->edit_configuration($portal_url,$ispurl,$editorpath,$isp_uid,$hotspot_folder_type,$location_id);
		    }*/
		    
        }
    }
	
	

    public function recurse_copy($src,$dst) {
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) {
                    if(($file == 'source') || ($file == 'user_guide')){
                        continue;
                    }elseif($file == 'ispmedia'){
			if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
		    }elseif($file == 'documents'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }elseif($file == 'marketing_promotion'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }elseif($file == 'slider_promotion'){
                        if (!file_exists($dst.'/'.$file)) {
                            @mkdir($dst.'/'.$file, 0777, true);
                        }
                        continue;
                    }else{
                        $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                    }
                } 
                else { 
                    copy($src . '/' . $file,$dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir);
        return 1;
    }
    
     public function edit_configuration($portal,$ispurl='',$editorpath='',$isp_uid='',$type='', $location_id = ''){
         
	//echo $editorpath;die;
            $chreading = fopen($editorpath."/libraries/config.php", 'r');
            $chwriting = fopen($editorpath."/libraries/config.tmp", 'w');
            $chreplaced = false;
            while (!feof($chreading)) {
                $line = fgets($chreading);
                if(preg_match("/\b(BASE_URL)\b/", $line)){
                    $line = 'define("BASE_URL","'.$ispurl.$type.'/"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(ASSETS_URL)\b/", $line)){
                    $line = 'define("ASSETS_URL","'.$ispurl.$type.'/"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(ISP_UID)\b/", $line)){
                    $line = 'define("ISP_UID","'.$isp_uid.'"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(LOCATIONID)\b/", $line)){
                    $line = 'define("LOCATIONID","'.$location_id.'"); ';
                    $chreplaced = true;
                }
                fputs($chwriting, $line);
            }
            fclose($chreading); fclose($chwriting);
            if ($chreplaced) {
                rename($editorpath."/libraries/config.tmp", $editorpath."/libraries/config.php");
            } else {
                unlink($editorpath."/libraries/config.tmp");
            }
       
	/*****************************************************************************************/

        return 1;
    }
	
	public function getisp_accounttype($isp_uid){
        $query = $this->db->query("SELECT decibel_account FROM sht_isp_admin WHERE isp_uid='".$isp_uid."' AND is_activated='1'");
        if($query->num_rows() > 0){
            $rowdata = $query->row();
            return $rowdata->decibel_account;
        }
    }
	
	public function update_planpricing(){
		$query=$this->db->query("SELECT spp.* FROM sht_services ss 
		INNER JOIN `sht_plan_pricing` spp ON (spp.srvid=ss.srvid) WHERE ss.isp_uid='194'");
		if($query->num_rows()>0){
			foreach($query->result() as $val){
				$perc=18;
				$grossamt=$val->net_total-($perc/100*$val->net_total);
				$tabledata=array("gross_amt"=>$val->net_total,"tax"=>$perc);
				$this->db->update('sht_plan_pricing',$tabledata,array("id"=>$val->id));
				
				
				$grossamt=($val->net_total*100)/118;
				$tabledata=array("gross_amt"=>round($grossamt),"tax"=>$perc);
				$this->db->update('sht_plan_pricing',$tabledata,array("id"=>$val->id));
				
			}
		}
	}
	
	
	
}


?>