function addmorestate_toisp() {
    var countryid = $('select[name="isp_country"] option:selected').val();
    $.ajax({
        url: base_url+'manageISP/getstatelist',
        type: 'POST',
	data: 'countryid='+countryid,
        success: function(data){
            $('.morestate').append('<div class="row"><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-left:0px;"><div class="mui-select"><select name="isp_state[]" required><option value="">Select State *</option>'+data+'</select><label>Select STATE</label></div></div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><span class="mui-btn mui-btn--small mui-btn--accent" onclick="remove_stateoption(this)"><i class="fa fa-times" aria-hidden="true"></i></span></div></div>');
        }
    });
    
}

function remove_stateoption(obj) {
    $(obj).parent().parent().remove();
}

function add_ispuser() {
    $('.loading').removeClass('hide');
    var countryid = $('select[name="isp_country"] option:selected').val();
    var formdata = $('#isp_userform').serialize();
    $.ajax({
        url: base_url+'manageISP/addtodb_ispuser',
        type: 'POST',
        dataType: 'json',
        data: formdata+'&isp_country='+countryid,
        async: false,
        success: function(data){
            window.scrollTo(0,0);
            if (data.portal_exists != 1) {
                $('.portal_err').addClass('hide');
                $('input[name="ispadmin_uid"]').val(data.ispuid);
                $('.ispadmin_uid').val(data.ispuid);
                $('.ispuuid').removeClass('hide');
                $('.active_addc').removeClass('hide');
                moduletab_click();
                mui.tabs.activate('modules');
            }else{
                $('.portal_err').removeClass('hide');
            }
            $('.loading').addClass('hide');
        }
    });
    
}

function infotab_click() {
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    if(ispadmin_uid != ''){
        $('.loading').removeClass('hide');
        $.ajax({
            url: base_url+'manageISP/ispuser_details',
            type: 'POST',
            data: 'ispadmin_uid='+ispadmin_uid,
            dataType: 'json',
            async: false,
            success: function(data){
                window.scrollTo(0,0);
                
                $('input[name="ispadmin_uid"]').val(data.isp_uid);
                $('input[name="isp_name"]').val(data.isp_name);
                $('input[name="isp_legal_name"]').val(data.legal_name);
                $('input[name="isp_email"]').val(data.email);
                $('input[name="isp_portal_url"]').val(data.portal_url);
                $('input[name="isp_mobile"]').val(data.phone);
                $('select[name="isp_state"]').val(data.state);
                $('select[name="isp_city"]').val(data.city);
                $('select[name="isp_country"]').val(data.country_id);
                $('input[name="isp_gstin_number"]').val(data.gst_no);
                $('.loading').addClass('hide');
            }
        });
    }
}

$('body').on('change', '.homewifi', function(){
    var chkArray = [];
    $(".homewifi:checked").each(function() {
        chkArray.push($(this).val());
    });
    
    if (chkArray.length > 0) {
        $('.defhomewifi').prop("checked", true);
        $('.defhomewifi').attr("disabled", true);
        var decibel_module = $('.defhomewifi').val();
        chkArray.push(decibel_module);
    }else{
        $('.defhomewifi').attr("disabled", false);
        $('.defhomewifi').prop("checked", false);
        chkArray = [];
    }
    //alert(chkArray);
    if ((chkArray.indexOf('5') != -1) || chkArray.indexOf('6') != -1) {
        $('input[name="website_module"]').prop("checked", true);
    }
    
});

function add_ispmodules() {
    var addchkArray = [];
    var publicwifi = $(".publicwifi:checked").val();
    var channel = $(".channel:checked").val();
    if (typeof publicwifi != 'undefined') {
        addchkArray.push(publicwifi);
    }
    if (typeof channel != 'undefined') {
        addchkArray.push(channel);
    }

    var decibel_module = $('.defhomewifi:checked').val();
    if (typeof decibel_module != 'undefined') {
        addchkArray.push(decibel_module);
    }
    $(".homewifi:checked").each(function() {
        addchkArray.push($(this).val());
    });
    var selected_modules;
    selected_modules = addchkArray.join(',');  
    
    if(selected_modules.length > 0){
        //alert("You have selected " + selected_modules);
        $('.loading').removeClass('hide');
        var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
        var formdata = 'ispadmin_uid='+ispadmin_uid+'&isp_modules='+selected_modules;
        $.ajax({
            url: base_url+'manageISP/addtodb_ispmodules',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                window.scrollTo(0,0);
                $('.loading').addClass('hide');
                mui.tabs.activate('billing');
            }
        });
    }else{
        alert("Please select atleast one module");	
    }
}

$('body').on('change', '#userstatus_changealert', function(){
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    $.ajax({
	url: base_url+'manageISP/portalurl_exists',
	type: 'POST',
	data: 'ispadmin_uid='+ispadmin_uid,
	async: false,
	success: function(data){
	    if (data > 0) {
		var isp_portal_url = $('input[name="isp_portal_url"]').val();
		if ((ispadmin_uid != '') && (isp_portal_url != '')) {
		    $.ajax({
			url: base_url+'manageISP/checkuserstaus',
			type: 'POST',
			dataType: 'json',
			data: 'ispadmin_uid='+ispadmin_uid,
			async: false,
			success: function(data){
			    $('#userstatus_changealert').parent().removeClass('btn-success');
			    $('#userstatus_changealert').parent().addClass('btn-danger off');
			    if (data.tab_incomplete == 0) {
				if (data.infotab_details == 0) {
				    $('#infoactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');
				}else{
				    $('#infoactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289'); 
				}
				
				if (data.moduletab_details == 0) {
				    $('#modactive').html('<i class="fa fa-times" aria-hidden="true"></i>').parent().css('color', '#f00');
				}else{
				    $('#modactive').html('<i class="fa fa-check" aria-hidden="true"></i>').parent().css('color', '#3CC289'); 
				}
				$('#pendingAlert').modal('show');
				$('#user_activatedModal').modal('hide');
			    }else{
				/*$('#pendingAlert').modal('hide');
				$('#otp_to_activate_user').val(data.useract_otp);
				$('#activate_username').html(data.ispname);
				var custphone = data.mobile;
				$('#phone_to_activate_user').val(custphone);
				for (idx=2; idx < 7 ; idx++) {
				    custphone = custphone.substring(0, idx) + '*' + custphone.substring(idx+1);
				}
				$('#activate_userphone').html(custphone);
				
				$('#user_activatedModal').modal('show');*/
				
				$('.loading').removeClass('hide');
				$.ajax({
				    url: base_url+'manageISP/activate_ispuser',
				    type: 'POST',
				    dataType: 'json',
				    async:false,
				    data: 'ispadmin_uid='+ispadmin_uid,
				    success: function(data){
					var ispid = data.ispid;
					window.location = base_url+'manageISP/edit_isp/'+ispid;
				    }
				});
			    }
			}
		    });    
		}
	    }else{
		$('#userstatus_changealert').val(0);
		$('#userstatus_changealert').parent().removeClass('btn-success');
		$('#userstatus_changealert').parent().addClass('btn-danger off');
		alert('Please check all fields properly.');
	    }
	}
    });
    
});

function active_user_confirmation() {
    $('.loading').removeClass('hide');
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    var otp_to_activate_user = $('#otp_to_activate_user').val();
    var enter_otp_to_activate_user = $('#enter_otp_to_activate_user').val();
    var phone = $('#phone_to_activate_user').val();
    
    //alert(otp_to_activate_user +'@@@'+ enter_otp_to_activate_user);
    
    if (otp_to_activate_user != enter_otp_to_activate_user) {
        $('#activate_otp_err').html('Otp not matching. Please Try Again.');
        $('.loading').addClass('hide');
    }else{
        $('#user_activatedModal').modal('hide');
        $.ajax({
            url: base_url+'manageISP/activate_ispuser',
            type: 'POST',
            dataType: 'json',
            data: 'ispadmin_uid='+ispadmin_uid,
            success: function(data){
                var ispid = data.ispid;
                window.location = base_url+'manageISP/edit_isp/'+ispid;
            }
        });
    }
}

/*
function moduletab_click() {
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    if(ispadmin_uid == ''){
        $('#savemodulesbtn').prop('disabled', true);
    }
    $('.loading').removeClass('hide');
    $('.defhomewifi').prop('checked',false);
    $('.homewifi').prop('checked',false);
    $('.publicwifi').prop('checked',false);
    $('.channel').prop('checked',false);
    $.ajax({
        url: base_url+'manageISP/ispmodules_details',
        type: 'POST',
        data: 'ispadmin_uid='+ispadmin_uid,
        dataType: 'json',
        async: false,
        success: function(data){
            window.scrollTo(0,0);
            $('.left_module').html('');
            $('.right_module').html('');
            $('.pwifi_module').html('');
            $('.shchannel_module').html('');
	    $('.reseller_module').html('');
            var modulejsonArr = data;
            
            var decsec = '';
            var decid = modulejsonArr.modules.decibel_module.id;
            var deciname = modulejsonArr.modules.decibel_module.name;
            var deciprice = modulejsonArr.modules.decibel_module.price;
            var decisections = modulejsonArr.modules.decibel_module.sections;
            var sectionArr = decisections.toString().split(',');
            for (var s=0; s < sectionArr.length; s++) {
                decsec += '<p style="margin: 5px 25px 0px"><strong>'+sectionArr[s]+'</strong></p>';
            }
            $('.left_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="decibel_module" value="'+decid+'" class="defhomewifi"><strong style="font-weight:700">'+deciname+' - Rs. '+deciprice+'</strong></label></div>'+decsec+'<div>');
            
            var appid = modulejsonArr.modules.apps_module.id;
            var appname = modulejsonArr.modules.apps_module.name;
            var appprice = modulejsonArr.modules.apps_module.price;
            $('.left_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="apps_module" value="'+appid+'" class="homewifi"><strong style="font-weight:700">'+appname+' - Rs. '+appprice+'</strong></label><div>');
            
            var custsec = '';
            var custid = modulejsonArr.modules.customer_module.id;
            var custname = modulejsonArr.modules.customer_module.name;
            var custprice = modulejsonArr.modules.customer_module.price;
            var custsections = modulejsonArr.modules.customer_module.sections;
            var custsectionArr = custsections.toString().split(',');
            for (var s=0; s < custsectionArr.length; s++) {
                custsec += '<p style="margin: 5px 25px 0px"><strong>'+custsectionArr[s]+'</strong></p>';
            }
            $('.left_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="customer_module" value="'+custid+'" class="homewifi"><strong style="font-weight:700">'+custname+' - Rs. '+custprice+'</strong></label></div>'+custsec+'<div>');
            
            /*******************************************************************************************/
            
	    /*
            var websec = '';
            var webid = modulejsonArr.modules.website_module.id;
            var webname = modulejsonArr.modules.website_module.name;
            var webprice = modulejsonArr.modules.website_module.price;
            var websections = modulejsonArr.modules.website_module.sections;
            var websectionArr = websections.toString().split(',');
            for (var s=0; s < websectionArr.length; s++) {
                websec += '<p style="margin: 5px 25px 0px"><strong>'+websectionArr[s]+'</strong></p>';
            }
            $('.right_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="website_module" value="'+webid+'" class="homewifi"><strong style="font-weight:700">'+webname+' - Rs. '+webprice+'</strong></label></div>'+websec+'<div>');
            
            var ebid = modulejsonArr.modules.earnburn_module.id;
            var ebname = modulejsonArr.modules.earnburn_module.name;
            var ebprice = modulejsonArr.modules.earnburn_module.price;
            $('.right_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="earnburn_module" value="'+ebid+'" class="homewifi"><strong style="font-weight:700">'+ebname+' - Rs. '+ebprice+'</strong></label><div>');
            
            var iotid = modulejsonArr.modules.ecomm_iot_module.id;
            var iotname = modulejsonArr.modules.ecomm_iot_module.name;
            var iotprice = modulejsonArr.modules.ecomm_iot_module.price;
            $('.right_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="ecomm_iot_module" value="'+iotid+'" class="homewifi"><strong style="font-weight:700">'+iotname+' - Rs. '+iotprice+'</strong></label><div>');
            
            var pwifiid = modulejsonArr.modules.publicwifi_module.id;
            var pwifiname = modulejsonArr.modules.publicwifi_module.name;
            var pwifiprice = modulejsonArr.modules.publicwifi_module.price;
            $('.pwifi_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="publicwifi_module" value="'+pwifiid+'" class="publicwifi"><strong>'+pwifiname+' - Rs. '+pwifiprice+'</strong></label></div></div>');
            
            var channelid = modulejsonArr.modules.channel_module.id;
            var channelname = modulejsonArr.modules.channel_module.name;
            var channelprice = modulejsonArr.modules.channel_module.price;
            $('.shchannel_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="channel_module" value="'+channelid+'" class="channel"><strong>'+channelname+' - Rs. '+channelprice+'</strong></label></div></div>');
	    
	    var resellerid = modulejsonArr.modules.reseller.id;
            var resellername = modulejsonArr.modules.reseller.name;
            var resellerprice = modulejsonArr.modules.reseller.price;
            $('.reseller_module').append('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="reseller" value="'+resellerid+'" class="reseller"><strong>'+resellername+' - Rs. '+resellerprice+'</strong></label></div></div>');
            
            //data = data.toString();
            var seldatArr = modulejsonArr.modules.selected_module;
            if (typeof seldatArr != 'undefined') {
                for(var i = 0; i < seldatArr.length; i++) {
                    if (seldatArr[i] == '1') {
                        $('input[name="decibel_module"]').prop('checked',true);
                        $('.defhomewifi').attr("disabled", true);
                    }
                    if (seldatArr[i] == '2') {
                        $('input[name="apps_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '3') {
                        $('input[name="customer_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '4') {
                        $('input[name="website_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '5') {
                        $('input[name="earnburn_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '6') {
                        $('input[name="ecomm_iot_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '7') {
                        $('input[name="publicwifi_module"]').prop('checked',true);
                    }
                    if (seldatArr[i] == '8') {
                        $('input[name="channel_module"]').prop('checked',true);
                    }
		    if (seldatArr[i] == '9') {
                        $('input[name="reseller"]').prop('checked',true);
                    }
                }
            }
            
            $('.loading').addClass('hide');
        }
    });
}
*/
/*
function edit_ispmodules() {
    var addchkArray = [];
    var publicwifi = $(".publicwifi:checked").val();
    var channel = $(".channel:checked").val();
    var reseller = $(".reseller:checked").val();
    if (typeof publicwifi != 'undefined') {
        addchkArray.push(publicwifi);
    }
    if (typeof channel != 'undefined') {
        addchkArray.push(channel);
    }
    if (typeof reseller != 'undefined') {
        addchkArray.push(reseller);
    }

    var decibel_module = $('.defhomewifi:checked').val();
    if (typeof decibel_module != 'undefined') {
        addchkArray.push(decibel_module);
    }
    $(".homewifi:checked").each(function() {
        addchkArray.push($(this).val());
    });
    var selected_modules;
    selected_modules = addchkArray.join(',');  
    
    if(selected_modules.length > 0){
        //alert("You have selected " + selected_modules);
        $('.loading').removeClass('hide');
        var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
        var formdata = 'ispadmin_uid='+ispadmin_uid+'&isp_modules='+selected_modules;
        $.ajax({
            url: base_url+'manageISP/edittodb_ispmodules',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                window.scrollTo(0,0);
                $('.loading').addClass('hide');
                location.reload();
            }
        });
    }else{
        alert("Please select atleast one module");	
    }
}
*/

function moduletab_click() {
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    if(ispadmin_uid == ''){
        $('#savemodulesbtn').prop('disabled', true);
    }
    $('.loading').removeClass('hide');
    $("input[name='homewifi_module']").prop('checked',false);
    $("input[name='publicwifi_module']").prop('checked',false);
    $.ajax({
        url: base_url+'manageISP/ispmodules_details',
        type: 'POST',
        data: 'ispadmin_uid='+ispadmin_uid,
        dataType: 'json',
        async: false,
        success: function(data){
	    var modulejsonArr = data;
	    var seldatArr = modulejsonArr.modules.selected_module;
            if (typeof seldatArr != 'undefined') {
                for(var i = 0; i < seldatArr.length; i++) {
                    if ((seldatArr[i] == '1') || (seldatArr[i] == '2') || (seldatArr[i] == '3') || (seldatArr[i] == '4') || (seldatArr[i] == '5') || (seldatArr[i] == '6')) {
                        $('input[name="homewifi_module"]').prop('checked',true);
                    }
                    if ((seldatArr[i] == '7') || (seldatArr[i] == '8') || (seldatArr[i] == '9')) {
                        $('input[name="publicwifi_module"]').prop('checked',true);
                    }
                }
            }
	    
	    $('.loading').addClass('hide');
	}
    });
}

function edit_ispmodules() {
    var addchkArray = [];
    var publicwifi = $("input[name='publicwifi_module']:checked").val();
    if (typeof publicwifi != 'undefined') {
        addchkArray.push(publicwifi);
    }
    var homewifi = $("input[name='homewifi_module']:checked").val();
    if (typeof homewifi != 'undefined') {
        addchkArray.push(homewifi);
    }
    
    var selected_modules;
    selected_modules = addchkArray.join(',');  
    
    
    if(selected_modules.length > 0){
        //alert("You have selected " + selected_modules);
        $('.loading').removeClass('hide');
        var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
        var formdata = 'ispadmin_uid='+ispadmin_uid+'&isp_modules='+selected_modules;
        $.ajax({
            url: base_url+'manageISP/edittodb_ispmodules',
            type: 'POST',
            dataType: 'json',
            data: formdata,
            async: false,
            success: function(data){
                window.scrollTo(0,0);
                $('.loading').addClass('hide');
                location.reload();
            }
        });
    }else{
        alert("Please select atleast one module");	
    }
}

function billingtab_click() {
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    $('.loading').removeClass('hide');
    $.ajax({
        url: base_url+'manageISP/ispbilling_details',
        type: 'POST',
        data: 'ispadmin_uid='+ispadmin_uid,
        dataType: 'json',
        async: false,
        success: function(data){
            //{"wallet_amt":"0.00","balance_amt":0,"total_license_used":"0.00","home_license_used":"0"}
            $('#acctype').html(data.decibel_account.toUpperCase());
            $('#license_balance').html(data.balance_amt);
            $('#home_license_used').html(data.home_license_used);
            $('#public_license_used').html(data.public_license_used);
            $('#total_license_used').html(data.total_license_used);
            if (data.usuage_logs != '') {
                $('#usuage_logs').html(data.usuage_logs);
            }
            $('.loading').addClass('hide');
        }
    });
}

function add_planbudget() {
    var ispadmin_uid = $('input[name="ispadmin_uid"]').val();
    var selectedslab = $('input[type=radio][name=select_module]:checked').val();
    var err = 0;
    if (selectedslab == 'custom') {
        var selectedslab = $('input[name=custom_license_fee]').val(); 
        $('.customlicense').each(function(i, obj) {
            if (this.value == '') {
               $('.customlicense').attr('required', 'required');
               err++;
            }
        });
    }
    
    var homewifi_budget = $('input[type=radio][name=select_module]:checked').data('homewifi');
    var publicwifi_budget = $('input[type=radio][name=select_module]:checked').data('publicwifi');
    
    if ((typeof homewifi_budget == 'undefined') && (typeof publicwifi_budget == 'undefined')) {
        var homewifi_budget = $('input[name=custom_license_homewifi]').val();
        var publicwifi_budget = $('input[name=custom_license_publicwifi]').val();
    }
    
    if (err == 0) {
        //var formdata = $('#isp_planform').serialize();
        var license_cheque_dd_neft = $('input[name="license_cheque_dd_neft"]').val();
        var license_discount = $('select[name="license_discount"] option:selected').val();
        $('.paybtn').addClass('hide');
        $.ajax({
            url: base_url+'manageISP/addlicense_toisp',
            type: 'POST',
            data: 'slabamt='+selectedslab+'&hbudget='+homewifi_budget+'&pbudget='+publicwifi_budget+'&cheque_dd_neft='+license_cheque_dd_neft+'&discount='+license_discount+'&ispadmin_uid='+ispadmin_uid,
            dataType: 'json',
            async: false,
            success: function(data){
                $('#planModal').modal('hide');
                billingtab_click();
            }
        });
    }
    
}

